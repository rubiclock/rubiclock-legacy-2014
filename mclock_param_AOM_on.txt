


rtts_server = 192.168.1.96:3000


#
# Display gnuplot
#
gplot_detection = udp:localhost:7000

#
# Acquisition avec le NIDAQ
#
phd_offset=4.3
use_raw_detection_acquisition = true
acquisition_freq = 50e3
acquisition_interchannel_gap = 5e-6
display_fine_phd = true
display_coarse_phd = true


#
# Acquisition avec l'USBDUX SIGMA
#
use_duxadc = true
duxadc_server = 192.168.1.96:3001
duxadc_filerecord = usbduxsigma_record.bin
duxadc_nchannels = 8
duxadc_freq = 1000
duxadc_shotperiod = 10e-3



#
# DDS
#
ad9959_freq = 500e6
add9959_pllmult = 5
ad9959_stepduration=1e-6

ad9912_freq = 875e6


#
# Carac des shutters
#
cooling_shutter_opening_delay = 2e-3
cooling_shutter_closing_delay = 2e-3

vertical_shutter_opening_delay = 2e-3
vertical_shutter_closing_delay = 2e-3



#
# parametres definissant les temps dans un cycle
#

# Coolings
Cooling_length = 100e-3
Cooling_AOM_amp = 5
Cooling_Repumper_amp = 0
Cooling_Repumper_DDS_frequency = 209.934e6
Cooling_Cavity_DDS_frequency = 165.317389096e6
Cooling_Cooling_DDS_frequency = 613e6


# Cooling_to_SDC1_transition
SDC1_trans_length = 0.5e-3

# SDC1
SDC1_length = 0.5e-3
SDC1_AOM_amp = 5
SDC1_Repumper_DDS_frequency = 185.67e6
SDC1_Cooling_DDS_frequency = 643.33e6


# SDC1_to_SDC2_transition
SDC2_trans_length = 0.5e-3


# SDC2
SDC2_length = 0.5e-3
SDC2_Repumper_amp = 0.5
SDC2_Repumper_DDS_frequency = 179.604e6
SDC2_Cooling_DDS_frequency = 649.396e6


# SDC2_to_Blue
Blue_trans_length = 0.5e-3

# Blue
Blue_length = 0.5e-3
Blue_AOM_amp = 5
Blue_Repumper_amp = 1
Blue_Repumper_DDS_frequency = 222.066e6
Blue_Cooling_DDS_frequency = 606.934e6


# Blue_to_Depumping
Dep_trans_length = 0.5e-3


# Depumping
Dep_length = 0.5e-3
Dep_Repumper_amp = 0
Dep_Repumper_DDS_frequency = 22.015e6
Dep_Cooling_DDS_frequency = 746.325e6


# Expansion
expansion_length = 0.5e-3
expansion_Repumper_DDS_frequency = 125.01e6
expansion_Cooling_DDS_frequency = 643.33e6

# Pi2_1
pi2_1_length = 0.5e-3

# Ramsey
T_R = 0.5e-3

# Pi2_2
pi2_2_length = 0.5e-3

# Detection_wait
det_wait_length = 0.5e-3
det_wait_AOM_amp = 5
det_wait_Repumper_DDS_frequency = 155.34e6
det_wait_Cooling_DDS_frequency = 500e6

# Detection
det_length = 100e-3

# Push_Depump
push_depump_length = 2e-3
push_depump_Repumper_DDS_frequency = 22.015e6
push_depump_Cooling_DDS_frequency = 500e6


# Detection_thermal_zero
det_th_zero_length = 1e-3
det_th_zero_Repumper_DDS_frequency = 155.34e6


# Detection_thermal_wait
det_th_wait_length = 0.5e-3

# Detection_thermal
det_th_length = 2e-3

# Dead_time
dead_time = 1e-3

