
Cooling = true
Cooling_to_SDC1_transition = false
SDC1 = true
SDC1_to_SDC2_transition = false
SDC2 = false
SDC2_to_Blue = false
Blue = false
Blue_to_Depumping = false
Depumping = false
Expansion = true
Pi2_1 = false
Ramsey = false
Pi2_2 = false
Detection_wait = true
Detection = true
Push_Depump = true
Detection_thermal_zero = true
Detection_thermal_wait = true
Detection_thermal = true
Dead_time = true