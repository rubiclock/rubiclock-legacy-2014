#!/bin/sh

#adds frequency offset to main sequence

MainPath=/home/mclock
MainSeq=mclock_paramshort_cooling.txt
BackupPrefix=old_

if [ $# -ge 1 ]; then
	newf=$(echo $(awk '/Cooling_Cavity_DDS/ {print $3}' $MainPath/$MainSeq) '+ (' $1 ')' | bc)
fi

cp $MainPath/$MainSeq $MainPath/$BackupPrefix$MainSeq

echo Changing center frequency to $newf
awk -v nf=$newf '/Cooling_Cavity_DDS/ { $3 = nf }; 1' $MainPath/$MainSeq > $MainPath/temp.txt

cp $MainPath/temp.txt $MainPath/$MainSeq

echo New frequency $(awk '/Cooling_Cavity_DDS/ {print $3}' $MainPath/$MainSeq)

rm $MainPath/temp.txt
