#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_loading.txt
_cdef=mclock_cdef_loading.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param

if [ $# -gt 0 ]; then
	echo Loading time scan, custom duration 
	./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef scan Cooling_length 10e-3 200e-3 10e-3 1 $1
else
	echo Loading time scan, default duration 
	./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef scan Cooling_length 10e-3 200e-3 10e-3 1 3
fi

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

