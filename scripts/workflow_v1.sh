#!/bin/sh

#pathtoscans_rel=../scans
pathtoscans=/home/mclock/scans
scan_prefix=scan.
phd_prefix=phd
sleep_time=0.5
scan_avg_duration=.2

#range for averaging photodide signal
first_pulse_begin=10
first_pulse_end=110
sec_pulse_begin=320
sec_pulse_end=420

#param_name=Ramsey_Cavity_DDS_frequency
param_name=Cooling_Cavity_DDS_frequency

ABSPATHTOSCAN=$(pwd)$pathtoscans_rel/

echo Scripting test

########################################################
#phase 1: launch data **DUMMY**
#we will get the number of the scan that is launched
WHICHSCAN=121

#and the total number of scans
HOWMANYSCANS=201
########################################################

########################################################
#phase 2: data process

#max_iter = $(echo "$scan_avg_duration*$HOWMANYSCANS/$sleep_time+1" | bc)
max_iter=100

#we find the right directory
summary_name=$scan_prefix$(printf "%06d" $WHICHSCAN)
#irpath=$pathtoscans_rel/$summary_name
dirpath=$pathtoscans/$summary_name
#echo 'Now in ' $(pwd)

#we wait for all the scans to be done
scan_aborted=1
for ((i=1;i<=$max_iter;i++))
do
    echo Waiting for scan to complete $i/$max_iter
    #we could start processing scans here
    if [ $(ls -1 $dirpath | wc -l) -ge $[$HOWMANYSCANS+1] ]; then 
	scan_aborted=0
	break
    fi
    sleep $sleep_time
done

if [ $scan_aborted -gt 0 ]; then
    echo Timed out while waiting for scan to complete
    exit 10 #we could instead update $HOWMANYSCANS
fi

#we look for the column in which data reside
column_id=$(head -n1 $dirpath/$summary_name | awk -v var=$param_name \
    'BEGIN {RS="\t"} $1 ~ var {printf "%d", NR}')

#we extract relevant columns from file
awk -v c=$column_id 'NR==1 {print "#",$4,$c} NR!=1 {print $3,$(c-1)}' \
    $dirpath/$summary_name > changescan.dat

#loop on all file and process
printf "%s" "Processing scans... "
echo "#scan phd1_fine_avg phd1_fine_std phd2_fine_avg phd2_fine_std phd1_coarse_avg phd1_coarse_std phd2_coarse_avg phd2_coarse_std" >> photodiodes_1.dat
echo "#scan phd1_fine_avg phd1_fine_std phd2_fine_avg phd2_fine_std phd1_coarse_avg phd1_coarse_std phd2_coarse_avg phd2_coarse_std" >> photodiodes_2.dat
for f in $(ls $dirpath/$phd_prefix*)
do
    temp=$(echo $f | awk 'BEGIN {RS="."} NR==3 {print $1," "}')
    temp+=$(/cygdrive/c/Users/mclock/Dropbox/rubiclock/codes/bash/worktest/process_phd.sh $first_pulse_begin $first_pulse_end $f)
    echo $temp >> photodiodes_1.dat
    temp=$(echo $f | awk 'BEGIN {RS="."} NR==3 {print $1," "}')
    temp+=$(/cygdrive/c/Users/mclock/Dropbox/rubiclock/codes/bash/worktest/process_phd.sh $sec_pulse_begin $sec_pulse_end $f)
    echo $temp >> photodiodes_2.dat
done
echo "done!"

#produce final result
final_name=results_$summary_name.dat
paste changescan.dat photodiodes_1.dat photodiodes_2.dat > $final_name
rm changescan.dat photodiodes_1.dat photodiodes_2.dat

gnuplot -persist -e "f='$final_name'; c=165.327e6" /cygdrive/c/Users/mclock/Dropbox/rubiclock/codes/bash/worktest/plot_results.gp

echo End of script

