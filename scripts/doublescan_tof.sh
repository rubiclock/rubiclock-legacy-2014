#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_tof.txt
_cdef=mclock_cdef_loading.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_loading.txt | \
	awk '/dead_time/ {$3 = 0.04}; 1' > ../$_param

#verifié 20s pour loading 100ms
./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef scan expansion_length 4e-3 150e-3 5e-3 1 3 | \
	awk '{print "Scan",$4, "Iterations",$6}'

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

