#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_snr.txt
_cdef=mclock_cdef_rabi.txt 

#on se met en pulse pi
cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
	awk '/pi2_1_length/ {temp=$3; $3=2*temp}; 1' > ../$_param

echo Loading Rabi micro scan 

#on calcule les fréquences de start et stop
startf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) - 0.1 | bc)
stopf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) + 0.1 | bc)

echo Scanning from $startf Hz to $stopf Hz

#on lance la séquence
./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef \
	scan Cooling_Cavity_DDS_frequency $startf $stopf 0.1 1 70

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

