#!/bin/sh

pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_rabi.txt
_cdef=mclock_cdef_rabi.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
	awk '/pi2_1_length/ {temp=$3; $3=2*temp}; 1' > ../$_param

echo Loading Rabi frequency scan 

startf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) - 24000 | bc)
stopf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) + 24000 | bc)

echo Scanning from $startf Hz to $stopf Hz

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef \
	scan Cooling_Cavity_DDS_frequency $startf $stopf 120 1 1

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

