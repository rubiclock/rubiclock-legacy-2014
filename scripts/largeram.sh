#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_ramsey.txt
_cdef=mclock_cdef_ramsey.txt 

echo Loading Ramsey frequency scan 

_halfspan=2000
	
if [ $# -eq 1 ]; then
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
		awk -v newtr=$1 '/T_R/ { $3 = newtr }; 1'  > ../$_param
	_npoints=$(echo "scale=0; 9.7 / ( $1 + 0.11)" | bc)
else
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param
	_npoints=80
fi

fstep=$(echo "scale=3; $_halfspan / $_npoints" | bc)

startf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) "- ( $_halfspan ) " | bc)
stopf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) "+ ( $_halfspan ) " | bc)

echo Scanning from $startf Hz to $stopf Hz with step $fstep Hz

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef \
	scan Cooling_Cavity_DDS_frequency $startf $stopf $fstep 1 1

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

