#!/bin/sh

pathtoscans_rel=../scans
pathtoscans=/home/mclock/scans
pathtoscr=/home/mclock/scripts
scan_prefix=scan.
phd_prefix=phd

#range for averaging photodide signal
first_pulse_begin=10
first_pulse_end=110
sec_pulse_begin=320
sec_pulse_end=420

WHICHSCAN=270

summary_name=$scan_prefix$(printf "%06d" $WHICHSCAN)
dirpath=$pathtoscans/$summary_name

# cat $(ls $dirpath/$phd_prefix*) | awk \
	# -v _col=1 -v _start=$first_pulse_begin -v _stop=$first_pulse_end\
	# '$1 ~ "#" {_ma=0; _mb=0; _mc=0; _md=0; n=0; _off=NR; _beg = _start+_off; _end= _stop+_off; print NR,_beg,_end }; \
	# NR==_beg, NR==_end \
	# { n++; _m+=($_col-_m)/n;};
	# NR==_end+1 {print NR, _m}'

cat $(ls $dirpath/$phd_prefix*) | awk -f $pathtoscr/data_crunch.awk
