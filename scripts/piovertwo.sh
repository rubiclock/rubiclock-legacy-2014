#!/bin/sh

#adds frequency offset to main sequence

MainPath=/home/mclock
MainSeq=mclock_paramshort_cooling.txt
BackupPrefix=old_

if [ $# -ge 1 ]; then
	cp $MainPath/$MainSeq $MainPath/$BackupPrefix$MainSeq
	echo Changing pi/2 time to $1
	awk -v nt=$1 '/pi2_1_length/ { $3 = nt }; 1' $MainPath/$MainSeq > $MainPath/temp.txt
	cp $MainPath/temp.txt $MainPath/$MainSeq	
	echo New pi/2 time $(awk '/pi2_1_length/ {print $3}' $MainPath/$MainSeq)
	rm $MainPath/temp.txt
else
	echo Nothing to do, leaving pi/2 at $(awk '/pi2_1_length/ {print $3}' $MainPath/$MainSeq)
fi
	