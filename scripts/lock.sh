#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_lock.txt
_cdef=mclock_cdef_lock.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param

echo Running settings for lock

./bin/mclock.exe param ../$_param cdef ../$_cdef override use_duxadc false force_dds_cooling_freq 613e6

if [ $# -ge 1 ]; then
	if [ $1 = 'duxoff' ]; then
		echo Overriding: DUXADC disabled
		_override="use_duxadc false" 
	else
		_override=""
	fi
else
		_override=""
fi

./bin/mclock.exe param ../$_param cdef ../$_cdef $_override

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

