#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_loading.txt
_cdef=mclock_cdef_rabi.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param

echo Loading time scan 

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef scan pi2_1_length 0.1e-3 5.7e-3 0.1e-3 1 3

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

