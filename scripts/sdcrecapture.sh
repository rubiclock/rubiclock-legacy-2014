#!/bin/sh

pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_cooling.txt
_cdef=mclock_cdef_sdc_loading.txt 


echo Scan with extralong dead_time

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
	awk '/dead_time/ {$3 = 0.3}; 1' > ../$_param

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef 
sleep 0.5

echo Loading time scan 

if [ $# -gt 0 ]; then
	echo Changing dead_time to $1
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
		awk -v dtime=$1 '/dead_time/ {$3 = dtime}; 1' > ../$_param
else
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param
fi

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef scan Cooling_length 10e-3 200e-3 10e-3 1 9 | \
	awk '{print "Scan",$4, "Iterations",$6}'

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

