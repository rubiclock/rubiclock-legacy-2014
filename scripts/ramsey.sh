#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_ramsey.txt
_cdef=mclock_cdef_ramsey.txt 

cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param

echo Loading Ramsey frequency scan 

_halfspan=150
startf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) + $1 - $_halfspan | bc)
stopf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) + $_halfspan +$1 | bc)

echo Scanning from $startf Hz to $stopf Hz

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef \
	scan Cooling_Cavity_DDS_frequency $startf $stopf 2 1 1

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

