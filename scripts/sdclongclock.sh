#!/bin/sh

#pathtoscans_rel=../scans
pathtoprod=/home/mclock/devpack-mclock-prod
pathtoscans=/home/mclock/scans

_pwd=$(pwd)

cd $pathtoprod

_param=mclock_paramtemp_ramsey.txt
_cdef=mclock_cdef_sdc_ramsey.txt 

echo Loading Ramsey frequency scan 

if [ $# -eq 1 ]; then
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt | \
		awk -v newtr=$1 '/T_R/ { $3 = newtr }; 1'  > ../$_param
	_halfspan=$(echo "scale=2; 0.25 / $1" | bc)
	_npoints=$(echo "scale=0; 72 / ( $1 + 0.11)" | bc)
	else
	cat ../mclock_param_preambule.txt ../mclock_paramshort_cooling.txt > ../$_param
	_halfspan=6.25
	_npoints=120
fi

startf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) "- ( $_halfspan ) " | bc)
stopf=$(echo $(awk '/Cooling_Cavity/ {print $3}' ../$_param) "+ ( $_halfspan ) " | bc)

echo Scanning from $startf Hz to $stopf Hz with step $_halfspan Hz for $_npoints

./bin/mclock_cmd.exe param ../$_param cdef ../$_cdef \
	scan Cooling_Cavity_DDS_frequency $startf $stopf $_halfspan 1 $_npoints

####ATTENTION!!!!!!!!!!!!!!
rm ../$_param
###########################

cd $_pwd

