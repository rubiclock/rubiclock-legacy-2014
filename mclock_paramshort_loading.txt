#
# parametres definissant les temps dans un cycle
#

# Coolings
Cooling_length = 100e-3
Cooling_AOM_amp = 5
Cooling_Repumper_amp = 1
Cooling_Repumper_DDS_frequency = 210e6
Cooling_Cavity_DDS_frequency = 165314407
Cooling_Cooling_DDS_frequency = 620e6
Cooling_VCO_setpoint = 4.26
# 1.4 MHz/10mV at 780 nm, resonance at 4.14V


# Cooling_to_SDC1_transition
SDC1_trans_length = 0.5e-3

# SDC1
SDC1_length = 0.5e-3
SDC1_AOM_amp = 3
SDC1_Repumper_amp = 1
SDC1_Repumper_DDS_frequency = 210e6
SDC1_Cooling_DDS_frequency = 643e6
SDC1_VCO_setpoint = 4.595
# 1.4 MHz/10mV at 780 nm


# SDC1_to_SDC2_transition
SDC2_trans_length = 0.5e-3


# SDC2
SDC2_length = 0.5e-3
SDC2_AOM_amp = 5
SDC2_Repumper_amp = 0
SDC2_Repumper_DDS_frequency = 179.604e6
SDC2_Cooling_DDS_frequency = 649.396e6
SDC2_VCO_setpoint = 4.686

# SDC2_to_Blue
Blue_trans_length = 0.5e-3

# Blue
Blue_length = 0.5e-3
Blue_AOM_amp = 5
Blue_Repumper_amp = 0
Blue_Repumper_DDS_frequency = 222.066e6
Blue_Cooling_DDS_frequency = 606.934e6
Blue_VCO_setpoint = 4.05


# Blue_to_Depumping
Dep_trans_length = 0.5e-3


# Depumping
Dep_length = 2e-3
Dep_AOM_amp = 5
Dep_Repumper_amp = 1
Dep_Repumper_DDS_frequency = 22.015e6
Dep_Cooling_DDS_frequency = 746.325e6 
Dep_VCO_setpoint = 6.127


# Expansion
expansion_length = 4e-3
expansion_AOM_amp = 0
expansion_Repumper_amp = 0
expansion_Repumper_DDS_frequency = 125.01e6
expansion_Cooling_DDS_frequency = 643.33e6
expansion_VCO_setpoint = 4.595

# Pi2_1
pi2_1_length = 0.7e-3

# Ramsey
T_R = 41e-3
Ramsey_delay_sw = 10e-6
Ramsey_delay_cavity_frequency = 0
#Ramsey_Cavity_DDS_frequency = 165.3244


# Pi2_2: ce param n'est plus utilisé, voir code c
pi2_2_length = 3e-3

# Detection_wait
det_wait_length = 0.2e-3
det_wait_AOM_amp = 1.6
det_wait_Repumper_amp = 0
det_wait_Repumper_DDS_frequency = 155.34e6
det_wait_Cooling_DDS_frequency = 613e6
det_wait_VCO_setpoint =4.14


# Detection
det_length = 2e-3

# Push_Depump
push_depump_length = 1e-3
push_depump_AOM_amp = 3
push_depump_Repumper_amp = 0
push_depump_Repumper_DDS_frequency = 22.015e6
push_depump_Cooling_DDS_frequency = 746.325e6
push_depump_VCO_setpoint = 6.127


# Detection_thermal_zero
det_th_zero_length = 3e-3
det_th_zero_AOM_amp = 1.6
#det_th_zero_Repumper_amp = 0
#det_th_zero_Repumper_DDS_frequency = 155.34e6
#det_th_zero_Cooling_DDS_frequency = 613e6


# Detection_thermal_wait
det_th_wait_length = 0.2e-3
#det_th_wait_AOM_amp = 1.6
#det_th_wait_Repumper_amp = 0
#det_th_wait_Cooling_DDS_frequency = 613e6

# Detection_thermal
det_th_length = 2e-3
#det_th_AOM_amp = 1.6
#det_th_Repumper_amp = 0
#det_th_Cooling_DDS_frequency = 613e6

# Dead_time
dead_time = 200e-3
Dead_time_AOM_amp = 0
Dead_time_Repumper_amp = 0

