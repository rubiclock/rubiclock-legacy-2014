# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.6

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake.exe

# The command to remove a file.
RM = /usr/bin/cmake.exe -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/mclock/devpack-mclock

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/mclock/devpack-mclock-prod

# Include any dependencies generated for this target.
include src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/depend.make

# Include the progress variables for this target.
include src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/progress.make

# Include the compile flags for this target's objects.
include src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/flags.make

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/flags.make
src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o: /home/mclock/devpack-mclock/src/mclock_cycle_def/mclock_cycle_def.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/mclock/devpack-mclock-prod/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o"
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && /usr/bin/gcc.exe  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o   -c /home/mclock/devpack-mclock/src/mclock_cycle_def/mclock_cycle_def.c

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.i"
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && /usr/bin/gcc.exe  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/mclock/devpack-mclock/src/mclock_cycle_def/mclock_cycle_def.c > CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.i

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.s"
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && /usr/bin/gcc.exe  $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/mclock/devpack-mclock/src/mclock_cycle_def/mclock_cycle_def.c -o CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.s

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.requires:

.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.requires

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.provides: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.requires
	$(MAKE) -f src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/build.make src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.provides.build
.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.provides

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.provides.build: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o


# Object files for target mclock_cycle_def
mclock_cycle_def_OBJECTS = \
"CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o"

# External object files for target mclock_cycle_def
mclock_cycle_def_EXTERNAL_OBJECTS =

lib/libmclock_cycle_def.a: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o
lib/libmclock_cycle_def.a: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/build.make
lib/libmclock_cycle_def.a: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/mclock/devpack-mclock-prod/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C static library ../../lib/libmclock_cycle_def.a"
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && $(CMAKE_COMMAND) -P CMakeFiles/mclock_cycle_def.dir/cmake_clean_target.cmake
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/mclock_cycle_def.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/build: lib/libmclock_cycle_def.a

.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/build

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/requires: src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o.requires

.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/requires

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/clean:
	cd /home/mclock/devpack-mclock-prod/src/mclock_cycle_def && $(CMAKE_COMMAND) -P CMakeFiles/mclock_cycle_def.dir/cmake_clean.cmake
.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/clean

src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/depend:
	cd /home/mclock/devpack-mclock-prod && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/mclock/devpack-mclock /home/mclock/devpack-mclock/src/mclock_cycle_def /home/mclock/devpack-mclock-prod /home/mclock/devpack-mclock-prod/src/mclock_cycle_def /home/mclock/devpack-mclock-prod/src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/depend

