# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/mclock/devpack-mclock/src/mclock_cycle_def/mclock_cycle_def.c" "/home/mclock/devpack-mclock-prod/src/mclock_cycle_def/CMakeFiles/mclock_cycle_def.dir/mclock_cycle_def.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/mclock/devpack-mclock/include"
  "/home/mclock/devpack-mclock/src/mclock_conf_hw"
  "/home/mclock/devpack-mclock/src/mclock_cycle_def"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
