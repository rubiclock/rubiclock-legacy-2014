


add_definitions( -O5 )

#
# Conf HW de la manip
#

include_directories( mclock_conf_hw )
add_subdirectory( mclock_conf_hw )

#
# Cycle Definition MCLOCK
#
include_directories( mclock_cycle_def )
add_subdirectory( mclock_cycle_def )

#
# Parametres MCLOCK
#
include_directories( mclock_param )
add_subdirectory( mclock_param )

include_directories( mclock )

include_directories( mclock_dds )
add_subdirectory( mclock_dds )

#
# MCLOCK
#
include_directories( mclock )
add_subdirectory( mclock )




#
# Module de commande MCLOCK
#
add_subdirectory( mclock_cmd )
