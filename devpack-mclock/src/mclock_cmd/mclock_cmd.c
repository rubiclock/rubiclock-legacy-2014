

#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#include "xalloc.h"
#include "xstring.h"
#include "xstrtonum.h"
#include "xfdio.h"

#include "sock_inet.h"


#include "conf_param.h"
#include "param_scan.h"

#include "mclock_param.h"
#include "mclock_cycle_def.h"

#include "mclock_cmd.h"




static char *mclock_scan_get_seqnum(void)
{
  int _fd;
  unsigned long _seqnum;
  unsigned long _newseqnum;
  char *_buf;

  _fd = xcreat_notrunc( xgetenv( "MCLOCK_SCAN_SEQNUM_PATH" ) );

  xftruncate( _fd, sizeof(_seqnum) );

  xread_exact( _fd, &_seqnum, sizeof(_seqnum) );
  _newseqnum = _seqnum + 1;
  xlseek( _fd, 0, SEEK_SET );
  xwrite( _fd, &_newseqnum, sizeof(_newseqnum) );

  xasprintf( &_buf, "%lu", _seqnum );

  return _buf;
}



static void mclock_send_dgram( struct iovec *p_iov, int p_iovcnt )
{
  char *_env;
  int _fd;
  struct sockaddr_in *_peer;

  _fd = xsocket_inet_dgram();

  _peer = ( _env = getenv( "MCLOCK_HOST" ) ) ?
              xsockaddr_in_initbystring( _env ) :
                xsockaddr_in_init( "localhost", MCLOCK_CMD_MSG_PORT );

  xsock_inet_dgram_sendv( _fd, p_iov, p_iovcnt, 0, _peer );
}


static void usage(void)
{
  fprintf( stderr, "usage : mclock_cmd <cmd>\n"
            "\tstop : stop mclock\n"
            "\tparam <paramfile> : update mclock param file\n"
            "\tcdef <cdeffile> : update mclock cycle def file\n"
            "\tscan <param_name> <start> <end> <step> <nsameshot> <npass>\n"
            "\tcancel_scan\n"
            "\n" );
  exit(1);
}





static void mclock_cmd_hdr_iovec_copy(enum mclock_cmd p_cmd,
                                      unsigned long p_len,
                                      struct iovec *p_iov )
{
  struct mclock_cmd_hdr *_hdr;

  _hdr = xzmalloc( sizeof(struct mclock_cmd_hdr) );

  _hdr->cmd = p_cmd;
  _hdr->len = p_len;

  p_iov->iov_base = _hdr;
  p_iov->iov_len = sizeof(struct mclock_cmd_hdr);
}

void param_scan_serialize(char *p_tag,
                          char *p_param,
                          double p_start,
                          double p_end,
                          double p_step,
                          unsigned long p_nsameshot,
                          unsigned long p_npass,
                          char **p_argz,
                          size_t *p_argzlen );

int main( int argc, char *argv[] )
{
  struct iovec _iov[16];
  int _iovcnt = 0;

  char *_argz = NULL;
  size_t _argzlen = 0;

  struct conf_params *_confparams;
  char *_seqnum;

  if ( argc < 2 )
    usage();

  int _i;

  for( _i=1; _i<argc; _i++ )
  {
    if ( strcmp( argv[_i], "stop" ) == 0 )
    {
      mclock_cmd_hdr_iovec_copy( mclock_cmd_stop, 0, &_iov[_iovcnt] );
      _iovcnt++;
    }
    else if ( strcmp( argv[_i], "param" ) == 0 )
    {
      mclock_conf_params_instantiate( &_confparams );
      conf_params_load_file( _confparams, argv[_i+1] );
      _argz = NULL;
      _argzlen = 0;
      conf_params_serialize_argz( _confparams, &_argz, &_argzlen );

      mclock_cmd_hdr_iovec_copy(mclock_cmd_set_param,
                                _argzlen,
                                &_iov[_iovcnt] );
      _iovcnt++;

      _iov[_iovcnt].iov_base = _argz;
      _iov[_iovcnt].iov_len = _argzlen;
      _iovcnt++;

      _i++;
    }
    else if ( strcmp( argv[_i], "cdef" ) == 0 )
    {
      mclock_cycle_def_conf_params_instantiate( &_confparams );
      conf_params_load_file( _confparams, argv[_i+1] );
      _argz = NULL;
      _argzlen = 0;
      conf_params_serialize_argz( _confparams, &_argz, &_argzlen );

      mclock_cmd_hdr_iovec_copy(mclock_cmd_set_cdef,
                                _argzlen,
                                &_iov[_iovcnt] );
      _iovcnt++;

      _iov[_iovcnt].iov_base = _argz;
      _iov[_iovcnt].iov_len = _argzlen;
      _iovcnt++;

      _i++;
    }
    else if ( strcmp( argv[_i], "scan" ) == 0 )
    {
      double _start = xstrtod( argv[_i+2] );
      double _end = xstrtod( argv[_i+3] );
      double _step = xstrtod( argv[_i+4] );
      unsigned long _nsameshot = xstrtoul( argv[_i+5] );
      unsigned long _npass = xstrtoul( argv[_i+6] );

      _seqnum = mclock_scan_get_seqnum();

      printf("scan number : %s for %lu cycles\n",
              _seqnum,
              _nsameshot *
                _npass *
                  param_scan_get_range_nstep( _start, _end, _step, true )  );

      _argz = NULL;
      _argzlen = 0;
      param_scan_serialize( _seqnum,
                            argv[_i+1],
                            _start,
                            _end,
                            _step,
                            _nsameshot,
                            _npass,
                            &_argz,
                            &_argzlen );

      mclock_cmd_hdr_iovec_copy(mclock_cmd_set_scan,
                                _argzlen,
                                &_iov[_iovcnt] );
      _iovcnt++;

      _iov[_iovcnt].iov_base = _argz;
      _iov[_iovcnt].iov_len = _argzlen;
      _iovcnt++;

      _i += 6;
    }
    else if ( strcmp( argv[_i], "cancel_scan" ) == 0 )
    {
      mclock_cmd_hdr_iovec_copy( mclock_cmd_cancel_scan, 0, &_iov[_iovcnt] );
      _iovcnt++;
    }
    else
    {
      fprintf( stderr, "unknown command\n" );
      usage();
    }
  }

  // printf("_iovcnt = %d\n", _iovcnt );
  mclock_send_dgram( _iov, _iovcnt );

  return 0;
}
