
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

#include "list.h"
#include "xalloc.h"

#include "mclock_param.h"
#include "mclock_cycle_def.h"

#include "mclock_internal.h"


struct mclock_cycle *mclock_cycle_instantiate(struct mclock *p_mclock,
                                              double p_time )
{
  struct mclock_cycle *_cycle;

  _cycle = xzmalloc( sizeof(struct mclock_cycle) );

  _cycle->cycle = p_mclock->cycle;
  _cycle->time = p_time;
  _cycle->tseq = p_mclock->tseq;

  pthread_mutex_init( &_cycle->lock, NULL );

  _cycle->cyclepr = p_mclock->cyclepr;

  _cycle->ccdef = p_mclock->ccdef_cycle;
  _cycle->cdef = mclock_cycle_def_duplicate( p_mclock->cdef );

  _cycle->cparam = p_mclock->cparam_cycle;
  _cycle->param = mclock_param_duplicate( p_mclock->param );

  return _cycle;
}


void mclock_cycle_free( struct mclock_cycle *p_cycle )
{

  if ( p_cycle->cdef )
    mclock_cycle_def_free( p_cycle->cdef );

  if ( p_cycle->param )
    xfree( p_cycle->param );

  if ( p_cycle->phdacq )
    mclock_phd_acq_free( p_cycle->phdacq );

  if ( p_cycle->scan )
    xfree( p_cycle->scan );

  xfree( p_cycle );
}





///////////////


struct mclock_cycle_processing
{
  struct mclock *mclock;

  pthread_mutex_t lock;
  pthread_cond_t cond;

  unsigned long lastcycleprocessed;
  unsigned long nrcycle;
  struct list_head cyclehead;
};





struct mclock_cycle_processing *mclock_cycle_processing_instantiate(
                                                      struct mclock *p_mclock )
{
  struct mclock_cycle_processing *_cyclepr;

  _cyclepr = xzmalloc( sizeof(struct mclock_cycle_processing) );

  _cyclepr->mclock = p_mclock;

  pthread_mutex_init( &_cyclepr->lock, NULL );
  pthread_cond_init( &_cyclepr->cond, NULL );

  INIT_LIST_HEAD( &_cyclepr->cyclehead );

  return _cyclepr;
}





void mclock_cycle_post( struct mclock_cycle *p_cycle )
{
  struct mclock_cycle_processing *_cyclepr = p_cycle->cyclepr;

  //
  // La le cycle est pret pour la phase de post processing
  //

  // printf("mclock_cycle_post : posting cycle %lu\n", p_cycle->seqcnt );

  INIT_LIST_HEAD( &p_cycle->list );

  pthread_mutex_lock( &_cyclepr->lock );

  list_add_tail( &p_cycle->list, &_cyclepr->cyclehead );

  _cyclepr->nrcycle++;
  if ( _cyclepr->nrcycle == 1 )
  {
    pthread_cond_signal( &_cyclepr->cond );
  }

  pthread_mutex_unlock( &_cyclepr->lock );
}




static void mclock_cycle_process_one_cycle( struct mclock *p_mclock,
                                            struct mclock_cycle *p_cycle )
{
  char *_recordpath;

  // printf("mclock_cycle_process_one_cycle : cycle %lu\n", p_cycle->cycle );

#if 0
  if ( p_cycle->seqcnt == 0 )
  {
    mclock_cycle_greetings( p_cycle );
  }
#endif

  // Display des photodiodes
  if ( p_mclock->phdgplot && p_cycle->phdacq )
    mclock_phd_gplot_display( p_mclock->phdgplot, p_cycle->phdacq );

  // Enregistrement d'un scan en cours
  if ( p_cycle->scan )
  {
    // Enregistrement des parametres du cycle
    mclock_scan_cycle_process(p_cycle->scan,
                              p_cycle->cycle,
                              p_cycle->time,
                              p_cycle->tseq,
                              p_cycle->ccdef,
                              p_cycle->cdef,
                              p_cycle->cparam,
                              p_cycle->param,
                              p_mclock->scan,
                              &_recordpath );
                              
    //printf("_recordpath: %s\n", _recordpath);
    
    // Enregistrement des photodiodes
    mclock_phd_record(_recordpath,
                      p_cycle->cycle,
                      p_cycle->phdacq );
		
    // Calcul des variables calculees et enregistrement
    mclock_phd_compute_calculated_variables(_recordpath,
                                            p_cycle->cycle,
                                            p_cycle->phdacq,
                                            p_cycle->calcvars);
    
    mclock_scan_cycle_process2(p_cycle->scan,
                               p_cycle->cycle,
                               p_cycle->time,
                               p_cycle->tseq,
                               p_cycle->calcvars,
                               p_mclock->scan,
                               &_recordpath );
  }


}





static void mclock_cycle_processing_manage_cycles(
                                    struct mclock_cycle_processing *p_cyclepr )
{
  struct mclock_cycle *_cycle, *_tmp;

  list_for_each_entry_safe( _cycle, _tmp, &p_cyclepr->cyclehead, list )
  {
    mclock_cycle_process_one_cycle( p_cyclepr->mclock, _cycle );
    p_cyclepr->lastcycleprocessed = _cycle->cycle;

    list_del( &_cycle->list );
    mclock_cycle_free( _cycle );
    p_cyclepr->nrcycle--;
  }
}

static void *mclock_cycle_processing_thread( void *p_arg )
{
  struct mclock_cycle_processing *_cyclepr = p_arg;

  while(1)
  {
    pthread_mutex_lock( &_cyclepr->lock );

    while( _cyclepr->nrcycle == 0 )
    {
      pthread_cond_wait( &_cyclepr->cond, &_cyclepr->lock );
    }

    mclock_cycle_processing_manage_cycles( _cyclepr );


    pthread_mutex_unlock( &_cyclepr->lock );
  }

  return NULL;
}

void mclock_cycle_processing_run( struct mclock_cycle_processing *p_cyclepr )
{
  pthread_t _tid;

  if ( pthread_create(&_tid,
                      NULL,
                      mclock_cycle_processing_thread,
                      p_cyclepr ) != 0 )
  {
    fprintf( stderr, "mclock_cycle_processing_run : pthread_create failed\n" );
    exit(1);
  }
}



