
#ifndef MCLOCK_CYCLE_INTERNAL_H
#define MCLOCK_CYCLE_INTERNAL_H

#include "mclock_internal.h"

struct mclock_cycle *mclock_cycle_instantiate(struct mclock *p_mclock,
                                              double p_time );

void mclock_cycle_free( struct mclock_cycle *p_cycle );

void mclock_cycle_post( struct mclock_cycle *p_cycle );


//
// cycle processing
//
struct mclock_cycle_processing *mclock_cycle_processing_instantiate(
                                                      struct mclock *p_mclock );

void mclock_cycle_processing_run( struct mclock_cycle_processing *p_cyclepr );



#endif // MCLOCK_CYCLE_INTERNAL_H
