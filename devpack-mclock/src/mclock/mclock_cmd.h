
#ifndef MCLOCK_CMD_H
#define MCLOCK_CMD_H

#define MCLOCK_CMD_MSG_PORT 1882

enum mclock_cmd
{
  mclock_cmd_unk,
  mclock_cmd_stop,
  mclock_cmd_set_param,
  mclock_cmd_set_cdef,
  mclock_cmd_set_scan,
  mclock_cmd_cancel_scan
};


struct mclock_cmd_hdr
{
  unsigned long len;
  enum mclock_cmd cmd;
};

#endif // MCLOCK_CMD_H
