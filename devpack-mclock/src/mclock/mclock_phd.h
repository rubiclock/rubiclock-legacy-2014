
#ifndef MCLOCK_PHD_H
#define MCLOCK_PHD_H

struct mclock_phd_acq;

struct mclock_phd_trace;

struct mclock_calc_vars;

struct mclock_phd_acq *mclock_phd_acq_instantiate( unsigned long p_nsamples );

struct mclock_phd_trace *mclock_phd_trace_instantiate( unsigned long p_nsamples );

struct mclock_calc_vars *mclock_calc_vars_instantiate( unsigned long p_nsamples );

void mclock_phd_acq_free( struct mclock_phd_acq *p_phdacq );

void mclock_phd_acq_copy_samples( struct mclock_phd_acq *p_phdacq,
                                  double *p_samples );






struct mclock_phd_gplot;


struct mclock_phd_gplot *mclock_phd_gplot_instantiate(bool p_dispfine,
                                                      bool p_dispcoarse,
                                                      char *p_gdisplay );


void mclock_phd_gplot_display(struct mclock_phd_gplot *p_phdgplot,
                              struct mclock_phd_acq *p_phdacq );



void mclock_phd_record( char *p_path,
                        unsigned long p_cycle,
                        struct mclock_phd_acq *p_phdacq );
						
void mclock_phd_compute_calculated_variables( char *p_path,
                                              unsigned long p_cycle,
                                              struct mclock_phd_acq *p_phdacq,
                                              struct mclock_calc_vars *p_calcvars );

#endif // MCLOCK_PHD_H
