
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <errno.h>

#include "ptrarith.h"

#include "xalloc.h"
#include "xstring.h"

#include "sock_inet.h"

#include "conf_param.h"
#include "param_scan.h"


#include "mclock_internal.h"
#include "mclock_cmd.h"
#include "mclock_scan.h"



struct mclock_cmd_server
{
  int fd;
  void *buf;
  size_t bufsize;

  struct mclock *mclock;
};


struct mclock_cmd_server *mclock_cmd_server_instantiate(
                                                      struct mclock *p_mclock )
{
  struct mclock_cmd_server *_cmdsrv;
  char *_env;
  int _port;

  _cmdsrv = xzmalloc( sizeof(struct mclock_cmd_server) );

  _port = ( _env = getenv( "MCLOCK_CMD_MSG_PORT" ) ) ?
            xstrtol( _env ) : MCLOCK_CMD_MSG_PORT;

  _cmdsrv->fd = xsock_udp_setup_binded( _port );
  xsocket_set_non_blocking( _cmdsrv->fd );

  _cmdsrv->bufsize = 65000;
  _cmdsrv->buf = xzmalloc(_cmdsrv->bufsize);

  _cmdsrv->mclock = p_mclock;

  return _cmdsrv;
}




static void mclock_cmd_server_process_cmd(struct mclock_cmd_server *p_cmdsrv,
                                          enum mclock_cmd p_cmd,
                                          void *p_buf,
                                          size_t p_len )
{
  void *_buf;
  size_t _len;

  struct mclock *_mclock = p_cmdsrv->mclock;
  struct param_scan *_scan;

  printf("mclock_cmd_server_process_cmd : cmd %lu, len = %zu\n", p_cmd, p_len );

  switch( p_cmd )
  {
    case mclock_cmd_stop:
      _mclock->stopping = true;
      break;

    case mclock_cmd_set_param:
      conf_param_deserialize_argz( _mclock->cparam, p_buf, p_len );
      break;

    case mclock_cmd_set_cdef:
      conf_param_deserialize_argz( _mclock->ccdef, p_buf, p_len );
      break;

    case mclock_cmd_set_scan:
      _scan = param_scan_deserialize( _mclock->cparam, p_buf, p_len );
      mclock_scan_add( _mclock->scan, _scan );
      break;

    case mclock_cmd_cancel_scan:
      mclock_scan_cancel( _mclock->scan );
      break;

    default:
      break;
  }
}



static void mclock_cmd_server_process_dgram(struct mclock_cmd_server *p_cmdsrv,
                                            void *p_buf,
                                            size_t p_len )
{
  struct mclock_cmd_hdr _hdr;
  unsigned long _offset = 0;

  while( _offset < p_len )
  {
    memcpy( &_hdr, PTR_ADD( p_buf, _offset ), sizeof(_hdr) );
    _offset += sizeof(_hdr);

    mclock_cmd_server_process_cmd(p_cmdsrv,
                                  _hdr.cmd,
                                  PTR_ADD( p_buf, _offset ),
                                  _hdr.len );
    _offset += _hdr.len;
  }
}



void mclock_cmd_server_poll( struct mclock_cmd_server *p_cmdsrv )
{
  ssize_t _ret;
  struct sockaddr_in _peer;

  _ret = xsock_inet_recvfrom(p_cmdsrv->fd,
                            p_cmdsrv->buf,
                            p_cmdsrv->bufsize,
                            0,
                            &_peer );
  if ( _ret > 0 )
    mclock_cmd_server_process_dgram( p_cmdsrv, p_cmdsrv->buf, (size_t)_ret );
}
