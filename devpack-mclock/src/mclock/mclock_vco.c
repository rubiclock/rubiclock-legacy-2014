

#include <stdbool.h>

#include "xalloc.h"

#include "mclock_internal.h"


struct mclock_vco
{
  bool setv0;
  double v0;

  bool setvramp;
  double vramp0;
  double vramp1;
};


struct mclock_vco *mclock_vco_instantiate(void)
{
  struct mclock_vco *_vco;

  _vco = xzmalloc( sizeof(struct mclock_vco) );

  return _vco;
}




int mclock_vco_get( struct mclock_vco *p_vco, double *p_v0 )
{
  if ( p_vco->setv0 == true )
  {
    *p_v0 = p_vco->v0;
    p_vco->setv0 = false;
    return 0;
  }

  return -1;
}




int mclock_vco_get_ramp(struct mclock_vco *p_vco,
                        double *p_vramp0,
                        double *p_vramp1 )
{
  if ( p_vco->setvramp == true )
  {
    *p_vramp0 = p_vco->vramp0;
    *p_vramp1 = p_vco->vramp1;
    p_vco->setvramp = false;
    return 0;
  }

  return -1;
}



static void _mclock_vco_set( struct mclock_vco *p_vco, double p_v0 )
{
  if ( p_vco != NULL )
  {
    p_vco->setv0 = true;
    p_vco->v0 = p_v0;
  }
}

static void _mclock_vco_set_ramp( struct mclock_vco *p_vco,
                                  double p_vramp0,
                                  double p_vramp1 )
{
  if ( p_vco != NULL )
  {
    p_vco->setvramp = true;
    p_vco->vramp0 = p_vramp0;
    p_vco->vramp1 = p_vramp1;
  }
}

void mclock_vco_set( struct mclock *p_mclock, double p_v0 )
{
  _mclock_vco_set( p_mclock->vco, p_v0 );
}


void mclock_vco_set_ramp( struct mclock *p_mclock,
                          double p_vramp0,
                          double p_vramp1 )
{
  _mclock_vco_set_ramp( p_mclock->vco, p_vramp0, p_vramp1 );
}


void mclock_vco2_set( struct mclock *p_mclock, double p_v0 )
{
  _mclock_vco_set( p_mclock->vco2, p_v0 );
}


void mclock_vco2_set_ramp(struct mclock *p_mclock,
                          double p_vramp0,
                          double p_vramp1 )
{
  _mclock_vco_set_ramp( p_mclock->vco2, p_vramp0, p_vramp1 );
}


