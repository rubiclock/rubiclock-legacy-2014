
#ifndef MCLOCK_CALIBRATION_H
#define MCLOCK_CALIBRATION_H

#include "param_calibration.h"

struct param_calibrations *mclock_param_calibrations_instantiate(void);

void mclock_amp_set(struct mclock *p_mclock,
                    char *p_timeref,
                    double p_timeoff,
                    bool p_AOM,
                    double p_AOMval,
                    bool p_Repumper,
                    double p_Repumperval );

void mclock_amp_set_ramp( struct mclock *p_mclock,
                          char *p_timeref,
                          double p_timeoff,
                          double p_duration,
                          unsigned long p_nsamples,
                          bool p_AOM,
                          double p_AOMval0,
                          double p_AOMval1,
                          bool p_Repumper,
                          double p_Repumperval0,
                          double p_Repumperval1 );
						  
//extern const CAL_CURVE AOM_amp_cal[];

#endif // MCLOCK_CALIBRATION_H
