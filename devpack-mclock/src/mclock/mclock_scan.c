
#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <pthread.h>

#include "list.h"

#include "xalloc.h"
#include "xstring.h"
#include "xstrtonum.h"
#include "xstdio.h"

#include "conf_param.h"
#include "param_scan.h"

#include "mclock_cycle_def.h"
#include "mclock_param.h"

struct mclock_calc_vars
{
  struct mclock_phd_trace *phd_ratio_coarse;
  struct mclock_phd_trace *phd_ratio_fine;
  double atom_number_c;
};

struct mclock_scan_file
{
  unsigned long nscan;
  // path ou vont etre stockes les logs du scan
  // et l'enregistrement des photodiodes
  char *path;
  // FILE utlise pour stocker les logs du scan
  FILE *f;

  struct list_head list;
};


struct mclock_scan
{
  char *recordpath;

  pthread_mutex_t lock;

  struct param_scan *scan;
  // numero du scan en cours
  unsigned long nscan;

  // cancel du scan en cours
  bool cancel;

  // head de la liste chainee des fichiers de log pour les scans
  struct list_head fscans;
  
  // head de la liste chainee des fichiers de log pour les scans
  struct list_head fscans2;
};




struct mclock_scan *mclock_scan_instantiate(void)
{
  struct mclock_scan *_scan;

  _scan = xzmalloc( sizeof(struct mclock_scan) );

  _scan->recordpath = xgetenv( "MCLOCK_SCAN_RECORD_PATH" );

  pthread_mutex_init( &_scan->lock, NULL );

  INIT_LIST_HEAD( &_scan->fscans );
  
  INIT_LIST_HEAD( &_scan->fscans2 );

  return _scan;
}



void mclock_scan_add( struct mclock_scan *p_mscan, struct param_scan *p_scan )
{
  unsigned long _nscan;
  struct mclock_scan_file *_sfile;
  struct mclock_scan_file *_sfile2;

  // le tag est le numero du scan en cours
  _nscan = xstrtoul( param_scan_get_tag( p_scan ) );

  printf("mclock_scan_add : begin scan %lu\n", _nscan );

  _sfile = xzmalloc( sizeof(struct mclock_scan_file) );
  _sfile->nscan = _nscan;
  // _sfile->path sera renseigne plus tard
  // _sfile->f sera ouvert plus tard
  INIT_LIST_HEAD( &_sfile->list );
  
  _sfile2 = xzmalloc( sizeof(struct mclock_scan_file) );
  _sfile2->nscan = _nscan;
  // _sfile->path sera renseigne plus tard
  // _sfile->f sera ouvert plus tard
  INIT_LIST_HEAD( &_sfile2->list );

  pthread_mutex_lock( &p_mscan->lock );

  p_mscan->cancel = false;
  p_mscan->scan = p_scan;
  p_mscan->nscan = _nscan;

  list_add( &_sfile->list, &p_mscan->fscans );
  
  list_add( &_sfile2->list, &p_mscan->fscans2 );

  pthread_mutex_unlock( &p_mscan->lock );
}


void mclock_scan_cancel( struct mclock_scan *p_mscan )
{
  pthread_mutex_lock( &p_mscan->lock );

  p_mscan->cancel = true;

  pthread_mutex_unlock( &p_mscan->lock );
}


struct mclock_scan_cycle
{
  // numero de scan
  unsigned long nscan;

  // shot en cours
  unsigned long nshot;

  // passe en cours
  unsigned long pass;

  // scan fini
  bool done;
};


struct mclock_scan_cycle *mclock_scan_iterate( struct mclock_scan *p_mscan )
{
  struct mclock_scan_cycle *_scancycle = NULL;

  pthread_mutex_lock( &p_mscan->lock );

  if ( p_mscan->scan != NULL )
  {
    _scancycle = xzmalloc( sizeof(struct mclock_scan_cycle) );

    _scancycle->nscan = p_mscan->nscan;

    if ( ( p_mscan->cancel == true )
      || ( param_scan_iterate(p_mscan->scan,
                              NULL,
                              &_scancycle->nshot,
                              &_scancycle->pass ) != 0 ) )
    {
      _scancycle->done = true;
      printf("mclock_scan_iterate : scan %lu done\n", p_mscan->nscan );
      param_scan_free( p_mscan->scan );
      p_mscan->scan = NULL;
    }
  }

  pthread_mutex_unlock( &p_mscan->lock );

  return _scancycle;
}




static void print_mclock_cdef_header( void *p_priv, struct conf_param *p_param )
{
  FILE *_f = p_priv;

  fprintf( _f, "\t%s", conf_param_get_name( p_param ) );
}

static void print_mclock_param_header( void *p_priv, struct conf_param *p_param )
{
  FILE *_f = p_priv;

  enum conf_param_type _type;

  _type = conf_param_get_type( p_param );

  if ( ( _type == conf_param_type_int )
    || ( _type == conf_param_type_uint )
    || ( _type == conf_param_type_double ) )
    fprintf( _f, "\t%s", conf_param_get_name( p_param ) );
}

static void mclock_scan_cycle_print_header( FILE *p_file,
                                            struct conf_params *p_ccdef,
                                            struct conf_params *p_cparam )
{
  fprintf( p_file, "#\ttimeUTC\ttseq\tcycle\tscan\tnshot\tpass" );

  conf_params_iterate( p_ccdef, print_mclock_cdef_header, p_file );
  conf_params_iterate( p_cparam, print_mclock_param_header, p_file );

  fputc( '\n', p_file );
}


static void print_mclock_cdef( void *p_priv, struct conf_param *p_param )
{
  FILE *_f = p_priv;
  bool _val;

  if ( conf_param_get_type( p_param ) == conf_param_type_bool )
  {
    _val = *(bool *)conf_param_get_ptr(p_param);
    fprintf( _f, "\t%lu", _val );
  }
}

static void print_mclock_param( void *p_priv, struct conf_param *p_param )
{
  FILE *_f = p_priv;

  enum conf_param_type _type;

  _type = conf_param_get_type( p_param );

  if ( ( _type == conf_param_type_int )
    || ( _type == conf_param_type_uint )
    || ( _type == conf_param_type_double ) )
  {
    fputc( '\t', _f );
    conf_param_fprintf_val( _f, p_param );
  }
}


void mclock_scan_cycle_process( struct mclock_scan_cycle *p_scancycle,
                                unsigned long p_cycle,
                                double p_time,
                                double p_tseq,
                                struct conf_params *p_ccdef,
                                struct mclock_cycle_def *p_cdef,
                                struct conf_params *p_cparam,
                                struct mclock_param *p_param,
                                struct mclock_scan *p_mscan,
                                char **p_recordpath )
{
  struct mclock_scan_file *_sfile = NULL;

  // on commence par recuperer le mclock_scan_file associe a ce scan

  pthread_mutex_lock( &p_mscan->lock );

  list_for_each_entry( _sfile, &p_mscan->fscans, list )
  {
    if ( p_scancycle->nscan == _sfile->nscan )
      break;
  }

  pthread_mutex_unlock( &p_mscan->lock );

  if ( _sfile == NULL )
  {
    fprintf( stderr, "mclock_scan_cycle_process : unknown scan %lu\n",
              p_scancycle->nscan );
    exit(1);
  }


  // on commence par creer le dossier d'enregistrement du scan
  if ( _sfile->path == NULL )
  {
    xasprintf(&_sfile->path,
              "%s/scan.%06lu",
              p_mscan->recordpath,
              p_scancycle->nscan );
    mkdir( _sfile->path, 0755 );
    if ( p_recordpath != NULL )
      *p_recordpath = _sfile->path;
  }

  // On commence par ouvrir le stream si ce n'est pas deja fait
  if ( _sfile->f == NULL )
  {
    char *_fname;
    xasprintf( &_fname, "%s/scan.%06lu", _sfile->path, p_scancycle->nscan );
    _sfile->f = xfopen( _fname, "w" );
    xfree( _fname );

    // on ecrit le header a ce moment la
    mclock_scan_cycle_print_header( _sfile->f, p_ccdef, p_cparam );
  }

  // ecriture des parametres du shot du scan
  fprintf( _sfile->f, "\t%f\t%f\t%lu\t%lu\t%lu\t%lu",
            p_time,
            p_tseq,
            p_cycle,
            p_scancycle->nscan,
            p_scancycle->nshot,
            p_scancycle->pass );

  conf_params_change_baseptr( p_ccdef, p_cdef );
  conf_params_iterate( p_ccdef, print_mclock_cdef, _sfile->f );

  conf_params_change_baseptr( p_cparam, p_param );
  conf_params_iterate( p_cparam, print_mclock_param, _sfile->f );

  fputc( '\n', _sfile->f );

  if ( p_scancycle->done == true )
  {
    fclose( _sfile->f );

    pthread_mutex_lock( &p_mscan->lock );
    list_del( &_sfile->list );
    pthread_mutex_unlock( &p_mscan->lock );
  }
  else
    fflush( _sfile->f );

}

void mclock_scan_cycle_process2( struct mclock_scan_cycle *p_scancycle,
                                 unsigned long p_cycle,
                                 double p_time,
                                 double p_tseq,
                                 struct mclock_calc_vars *p_calcvars,
                                 struct mclock_scan *p_mscan,
                                 char **p_recordpath )
{
  struct mclock_scan_file *_sfile2 = NULL;

  // on commence par recuperer le mclock_scan_file associe a ce scan

  pthread_mutex_lock( &p_mscan->lock );

  list_for_each_entry( _sfile2, &p_mscan->fscans2, list )
  {
    if ( p_scancycle->nscan == _sfile2->nscan )
      break;
  }

  pthread_mutex_unlock( &p_mscan->lock );

  if ( _sfile2 == NULL )
  {
    fprintf( stderr, "mclock_scan_cycle_process : unknown scan %lu\n",
              p_scancycle->nscan );
    exit(1);
  }

  // on commence par creer le dossier d'enregistrement du scan
  if ( _sfile2->path == NULL )
  {
    xasprintf(&_sfile2->path,
              "%s/scan.%06lu",
              p_mscan->recordpath,
              p_scancycle->nscan );
    mkdir( _sfile2->path, 0755 );
    if ( p_recordpath != NULL )
      *p_recordpath = _sfile2->path;
  }

  // On commence par ouvrir le stream si ce n'est pas deja fait
  if ( _sfile2->f == NULL )
  {
    char *_fname;
    xasprintf( &_fname, "%s/scan_rec.%06lu.txt", _sfile2->path, p_scancycle->nscan );
    _sfile2->f = xfopen( _fname, "w" );
    xfree( _fname );

    // on ecrit le header a ce moment la
    //mclock_scan_cycle_print_header( _sfile->f, p_ccdef, p_cparam );
  }

  // ecriture des parametres du shot du scan
  fprintf( _sfile2->f, "\t%lu\t%f\t%f\t%f\n", p_cycle, p_time, p_tseq, p_calcvars->atom_number_c);
  
  if ( p_scancycle->done == true )
  {
    fclose( _sfile2->f );

    pthread_mutex_lock( &p_mscan->lock );
    list_del( &_sfile2->list );
    pthread_mutex_unlock( &p_mscan->lock );
  }
  else
    fflush( _sfile2->f );
}



