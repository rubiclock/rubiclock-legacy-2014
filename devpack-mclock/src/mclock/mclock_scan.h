
#ifndef MCLOCK_SCAN_H
#define MCLOCK_SCAN_H

#include "conf_param.h"
#include "param_scan.h"
#include "mclock_phd.h"

#include "mclock_cycle_def.h"
#include "mclock_param.h"

struct mclock_scan;

struct mclock_scan *mclock_scan_instantiate(void);

void mclock_scan_add( struct mclock_scan *p_mscan, struct param_scan *p_scan );

void mclock_scan_cancel( struct mclock_scan *p_mscan );

struct mclock_scan_cycle;

struct mclock_scan_cycle *mclock_scan_iterate( struct mclock_scan *p_mscan );


void mclock_scan_cycle_process( struct mclock_scan_cycle *p_scancycle,
                                unsigned long p_cycle,
                                double p_time,
                                double p_tseq,
                                struct conf_params *p_ccdef,
                                struct mclock_cycle_def *p_cdef,
                                struct conf_params *p_cparam,
                                struct mclock_param *p_param,
                                struct mclock_scan *p_mscan,
                                char **p_recordpath );

void mclock_scan_cycle_process2( struct mclock_scan_cycle *p_scancycle,
                                 unsigned long p_cycle,
                                 double p_time,
                                 double p_tseq,
                                 struct mclock_calc_vars *p_calcvars,
                                 struct mclock_scan *p_mscan,
                                 char **p_recordpath );
                                 
#endif // MCLOCK_SCAN_H
