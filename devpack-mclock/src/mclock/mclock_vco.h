
#ifndef MCLOCK_VCO_H
#define MCLOCK_VCO_H

#include <stdbool.h>

#include "mclock_internal.h"

struct mclock_vco;

struct mclock_vco *mclock_vco_instantiate(void);



int mclock_vco_get( struct mclock_vco *p_vco, double *p_v0 );

int mclock_vco_get_ramp(struct mclock_vco *p_vco,
                        double *p_vramp0,
                        double *p_vramp1 );





void mclock_vco_set( struct mclock *p_mclock, double p_v0 );

void mclock_vco_set_ramp( struct mclock *p_mclock,
                          double p_vramp0,
                          double p_vramp1 );



void mclock_vco2_set( struct mclock *p_mclock, double p_v0 );

void mclock_vco2_set_ramp(struct mclock *p_mclock,
                          double p_vramp0,
                          double p_vramp1 );
#endif // MCLOCK_VCO_H
