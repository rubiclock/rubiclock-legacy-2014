
#ifndef MCLOCK_INTERNAL_H
#define MCLOCK_INTERNAL_H

#include "list.h"

#include "conf_param.h"
#include "param_scan.h"
#include "apts.h"
#include "rtts_session.h"
#include "rtts_dux_adc_session.h"
#include "param_calibration.h"

#include "mclock_cycle_def.h"
#include "mclock_phd.h"
#include "mclock_scan.h"



struct mclock_param;
struct mclock_dds;
struct mclock_vco;
struct mclock_cycle_processing;
struct mclock_cmd_server;


struct mclock_cycle
{
  // numero du cycle
  unsigned long cycle;

  // temps UTC du cycle
  double time;
  // temps sequenceur du cycle
  double tseq;

  // mutex du cycle
  pthread_mutex_t lock;

  // cycle processing
  struct mclock_cycle_processing *cyclepr;

  // les parametres correspondant a ce cycle
  struct conf_params *cparam;
  struct mclock_param *param;
  // la definition du cycle
  struct conf_params *ccdef;
  struct mclock_cycle_def *cdef;


  // donnees brutes issues de l'acquisition des photodiodes
  struct mclock_phd_acq *phdacq;

  // donnees calcualees a partir des donnees brutes
  struct mclock_calc_vars *calcvars;
  
  // scan
  struct mclock_scan_cycle *scan;


  // liste chainee de cycles
  struct list_head list;
};


struct mclock
{
  struct conf_params *cparam;
  struct conf_params *cparam_cycle;
  struct mclock_param *param;

  struct conf_params *ccdef;
  struct conf_params *ccdef_cycle;
  struct mclock_cycle_def *cdef;

  struct param_calibrations *calibrations;

  struct apts *apts;

  struct rtts_session *rtts;

  struct mclock_phd_gplot *phdgplot;

  struct rtts_dux_adc_session *duxadc;

  struct mclock_dds *dds;

  struct mclock_vco *vco;
  struct mclock_vco *vco2;

  //
  // scan
  //
  struct mclock_scan *scan;

  //
  // Gestion des cycles
  //

  struct mclock_cycle_processing *cyclepr;

  // serveur pour la reception des commandes
  struct mclock_cmd_server *cmdsrv;


  // temps sequenceur
  double tseq;

  // numero de cycle
  unsigned long cycle;

  bool stopping;
  bool stoppded;
};

#endif // MCLOCK_INTERNAL_H
