
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rtts_session.h"
#include "rtts.h"

#include "mclock_conf_hw.h"
#include "mclock_internal.h"



void mclock_rtts_start_cycle( struct mclock *p_mclock,
                              double p_time,
                              double p_tseq,
                              unsigned long p_cycle )
{
  struct rtts_cycle_time _cycletime;

  _cycletime.time = p_time;
  _cycletime.tseq = p_tseq;

  rtts_session_add_event_timestamp( p_mclock->rtts,
                                    RTTS_TIMESTAMP_ID_START_CYCLE,
                                    p_cycle,
                                    sizeof(_cycletime),
                                    &_cycletime );

  rtts_session_add_group( p_mclock->rtts );

  rtts_session_flush( p_mclock->rtts );


  // RTTS trig qui va marquer le debut de la transaction spi
  apts_set_single_pulse(p_mclock->apts,
                        NULL,
                        p_tseq,
                        MCLOCK_RTTS_TRIG_NIDAQ_DLINE,
                        1,
                        MCLOCK_RTTS_TRIG_PULSE_WIDTH );
}