
add_executable( mclock
                  mclock.c
                  mclock_rtts.c
                  mclock_vco.c
                  mclock_calibration.c
                  mclock_phd.c
                  mclock_cycle.c
                  mclock_scan.c
                  mclock_cmd_server.c )

target_link_libraries( mclock
                        conf_param
                        param_scan
                        param_calibration
                        mclock_cycle_def
                        mclock_param
                        mclock_conf_hw
                        mclock_dds
                        rtts_session
                        rtts_dux_adc_session
                        apts_nidaq
                        xalloc
                        xstring
                        xstrtonum
                        xstdio
                        xfdio
                        nidaqmx_tools
                        nidaq_dline
                        nidaq_counters_clocking
                        ${NIDAQMX}
                        apts
                        seqevent
                        seqbitstream
                        ddsbitstream
                        dds_iface
                        dds_iface_null
                        ad9959
                        ad9956
                        ad9852
                        ad9912
                        ad995x
                        runningstats
                        gsl
                        gslcblas
                        gplot
                        sockutil )
