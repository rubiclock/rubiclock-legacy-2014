
#ifndef MCLOCK_RTTS_INTERNAL_H
#define MCLOCK_RTTS_INTERNAL_H

#include "mclock_internal.h"


void mclock_rtts_start_cycle( struct mclock *p_mclock,
                              double p_time,
                              double p_tseq,
                              unsigned long p_cycle );

#endif // MCLOCK_RTTS_INTERNAL_H
