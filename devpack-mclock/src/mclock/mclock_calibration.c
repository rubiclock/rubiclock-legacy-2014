
#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "intarith.h"

#include "param_calibration.h"
#include "apts.h"

#include "mclock_internal.h"

const CALIBRATION_CURVE AOM_amp_cal[28] = 
{
  {0, 0},
  {0.5, 6e-5},
  {0.8, 8.23e-4},
  {0.9, 3.8e-3},
  {1., 9.62e-3},
  {1.1, 4.56e-2},
  {1.27, 0.108},
  {1.41, 0.334},
  {1.5, 0.621},
  {1.6, 1.18},
  {1.7, 1.96},
  {1.8, 3.04},
  {1.9, 4.94},
  {2., 8.35},
  {2.2, 14.2},
  {2.4, 22.5},
  {2.6, 32.9},
  {2.8, 42.7},
  {3., 54.9},
  {3.2, 62.0},
  {3.4, 72.8},
  {3.6, 83.5},
  {3.8, 86.1},
  {4., 92.4},
  {4.2, 96.8},
  {4.4, 98.7},
  {4.6, 99.4},
  {5., 100.},
/*  {0.0, 0.0},
  {5, 5}*/
};

static double AOM_amp_power_to_volts( double p_power )
{
  return interp_lin(AOM_amp_cal, 28, p_power);
}

static double Repumper_amp_power_to_volts( double p_power )
{
  double volt;
  
  volt = -4. + 4.*p_power;
  if ( volt < -4)
  {
    volt = -4;
  }
  else if (volt > 0)
  {
    volt = 0;
  }
  
  return volt;
}

struct param_calibrations *mclock_param_calibrations_instantiate(void)
{
  struct param_calibrations *_calibrations;
  struct param_calibration *_param;


  _calibrations = param_calibrations_instantiate();


  _param =  param_calibration_instantiate( _calibrations, "AOM_amp" );

  param_calibration_set_phys_to_analog( _param, AOM_amp_power_to_volts );

#if 0
  param_calibration_set_interp( _param,
                                "linear",
                                ARRAY_SIZE(diode_current_V),
                                diode_current_V,
                                diode_current_mA );
#endif

  _param =  param_calibration_instantiate( _calibrations, "Repumper_amp" );
  
  param_calibration_set_phys_to_analog( _param, Repumper_amp_power_to_volts );

  return _calibrations;
}



void mclock_amp_set(struct mclock *p_mclock,
                    char *p_timeref,
                    double p_timeoff,
                    bool p_AOM,
                    double p_AOMval,
                    bool p_Repumper,
                    double p_Repumperval )
{
  char *_channels[4];
  double _voltages[4];
  unsigned long _n = 0;
  double _v0;

  if ( p_AOM == true )
  {
    _channels[_n] = "AOM_amp";
    _voltages[_n] = param_calibrations_phys_to_analog_eval(
                                                        p_mclock->calibrations,
                                                        "AOM_amp",
                                                        p_AOMval );
    _n++;
  }

  if ( p_Repumper == true )
  {
    _channels[_n] = "Repumper_amp";
    _voltages[_n] = param_calibrations_phys_to_analog_eval(
                                                        p_mclock->calibrations,
                                                        "Repumper_amp",
                                                        p_Repumperval );
    _n++;
  }

  if ( ( p_mclock->vco != NULL )
    && ( mclock_vco_get( p_mclock->vco, &_v0 ) == 0 ) )
  {
    _channels[_n] = "vco";
    _voltages[_n] = max( 0.5, _v0 );
    // printf("mclock_amp_set <%s>: vco = %f V\n", p_timeref, _voltages[_n] );
    _n++;
  }

  /*
  if ( ( p_mclock->vco2 != NULL )
    && ( mclock_vco_get( p_mclock->vco2, &_v0 ) == 0 ) )
  {
    _channels[_n] = "vco2";
    _voltages[_n] = max( 0.5, _v0 );
    // printf("mclock_amp_set <%s>: vco2 = %f V\n", p_timeref, _voltages[_n] );
    _n++;
  }
  */

  if ( _n > 0 )
  {
    apts_set_ao(p_mclock->apts,
                p_timeref,
                p_timeoff,
                _n,
                _channels,
                _voltages );
  }
}




void mclock_amp_set_ramp( struct mclock *p_mclock,
                          char *p_timeref,
                          double p_timeoff,
                          double p_duration,
                          unsigned long p_nsamples,
                          bool p_AOM,
                          double p_AOMval0,
                          double p_AOMval1,
                          bool p_Repumper,
                          double p_Repumperval0,
                          double p_Repumperval1 )
{
  char *_channels[4];
  double _v0[4];
  double _v1[4];
  unsigned long _n = 0;
  double _vramp0;
  double _vramp1;

  if ( p_AOM == true )
  {
    _channels[_n] = "AOM_amp";
    _v0[_n] = param_calibrations_phys_to_analog_eval( p_mclock->calibrations,
                                                      "AOM_amp",
                                                      p_AOMval0 );
    _v1[_n] = param_calibrations_phys_to_analog_eval( p_mclock->calibrations,
                                                      "AOM_amp",
                                                      p_AOMval1 );
    _n++;
  }

  if ( p_Repumper == true )
  {
    _channels[_n] = "Repumper_amp";
    _v0[_n] = param_calibrations_phys_to_analog_eval( p_mclock->calibrations,
                                                      "Repumper_amp",
                                                      p_Repumperval0 );
    _v1[_n] = param_calibrations_phys_to_analog_eval( p_mclock->calibrations,
                                                      "Repumper_amp",
                                                      p_Repumperval0 );
    _n++;
  }

  if ( ( p_mclock->vco != NULL )
    && ( mclock_vco_get_ramp( p_mclock->vco, &_vramp0, &_vramp1 ) == 0 ) )
  {
    _channels[_n] = "vco";
    _v0[_n] = max( 0.5, _vramp0 );
    _v1[_n] = max( 0.5, _vramp1 );
    // printf("mclock_amp_set_ramp vco <%s>: vramp0 = %f V, vramp1 = %f V\n", p_timeref, _vramp0, _vramp1 );
    _n++;
  }

  /*
  if ( ( p_mclock->vco2 != NULL )
    && ( mclock_vco_get_ramp( p_mclock->vco2, &_vramp0, &_vramp1 ) == 0 ) )
  {
    _channels[_n] = "vco2";
    _v0[_n] = max( 0.5, _vramp0 );
    _v1[_n] = max( 0.5, _vramp1 );
    // printf("mclock_amp_set_ramp vco 2 <%s>: vramp0 = %f V, vramp1 = %f V\n", p_timeref, _vramp0, _vramp1 );
    _n++;
  }
  */

  if ( _n > 0 )
  {
    apts_set_ao_ramp( p_mclock->apts,
                      p_timeref,
                      p_timeoff,
                      p_duration,
                      p_nsamples,
                      _n,
                      _channels,
                      _v0,
                      _v1 );
  }
}

// Linear interpolation of a calibration curve
double interp_lin( const CALIBRATION_CURVE* curve, int n_points, double y)
{
  int i;
  
  for( i = 0; i < n_points-1; i++ )
  {
    if ( curve[i].y <= y && curve[i+1].y >= y )
    {
	  double diffy = y - curve[i].y;
      double diffn = curve[i+1].y - curve[i].y;

	  // DEBUG
	  //printf("X value found: %1.3f\n", curve[i].x + ( curve[i+1].x - curve[i].x ) * diffy / diffn);
	  
	  return curve[i].x + ( curve[i+1].x - curve[i].x ) * diffy / diffn;
    }
  }
  // If value out of range, return first element of calibration curve
  // DEBUG
  //printf("Returning default X value: %1.3f\n", curve[0].x);
  return curve[0].x;
}

