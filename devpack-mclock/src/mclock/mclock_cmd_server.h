
#ifndef MCLOCK_CMD_SERVER_H
#define MCLOCK_CMD_SERVER_H

#include "mclock_internal.h"

struct mclock_cmd_server;

struct mclock_cmd_server *mclock_cmd_server_instantiate(
                                                      struct mclock *p_mclock );

void mclock_cmd_server_poll( struct mclock_cmd_server *p_cmdsrv );

#endif // MCLOCK_CMD_SERVER_H
