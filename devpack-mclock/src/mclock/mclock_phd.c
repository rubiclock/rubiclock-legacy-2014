
#include <stdbool.h>
#include <math.h>

#include "xalloc.h"
#include "xstdio.h"

#include "gplot.h"


struct mclock_phd_acq
{
  unsigned long nsamples;
  double *phd1_fine;
  double *phd2_fine;
  double *phd1_coarse;
  double *phd2_coarse;
};

struct mclock_phd_trace
{
  unsigned long nsamples;
  double *trace;
};

struct mclock_calc_vars
{
  struct mclock_phd_trace *phd_ratio_coarse;
  struct mclock_phd_trace *phd_ratio_fine;
  double atom_number_c;
};

struct mclock_phd_acq *mclock_phd_acq_instantiate( unsigned long p_nsamples )
{
  struct mclock_phd_acq *_phdacq;

  _phdacq = xzmalloc( sizeof(struct mclock_phd_acq) );

  _phdacq->nsamples = p_nsamples;

  _phdacq->phd1_fine = xzmalloc( p_nsamples * sizeof(double) );
  _phdacq->phd2_fine = xzmalloc( p_nsamples * sizeof(double) );

  _phdacq->phd1_coarse = xzmalloc( p_nsamples * sizeof(double) );
  _phdacq->phd2_coarse = xzmalloc( p_nsamples * sizeof(double) );

  return _phdacq;
}

struct mclock_phd_trace *mclock_phd_trace_instantiate( unsigned long p_nsamples )
{
  struct mclock_phd_trace *_trace;

  _trace = xzmalloc( sizeof(struct mclock_phd_trace) );

  _trace->nsamples = p_nsamples;

  _trace->trace = xzmalloc( p_nsamples * sizeof(double) );

  return _trace;
}

struct mclock_calc_vars *mclock_calc_vars_instantiate( unsigned long p_nsamples )
{
  struct mclock_calc_vars *_calcvars;

  _calcvars = xzmalloc( sizeof(struct mclock_calc_vars) );

  _calcvars->phd_ratio_coarse = mclock_phd_trace_instantiate( p_nsamples );
  _calcvars->phd_ratio_fine = mclock_phd_trace_instantiate( p_nsamples );

  return _calcvars;
}

void mclock_phd_acq_free( struct mclock_phd_acq *p_phdacq )
{
  xfree( p_phdacq->phd1_fine );
  xfree( p_phdacq->phd2_fine );
  xfree( p_phdacq->phd1_coarse );
  xfree( p_phdacq->phd2_coarse );
}


void mclock_phd_acq_copy_samples( struct mclock_phd_acq *p_phdacq,
                                  double *p_samples )
{
  unsigned long _i;

  for( _i=0; _i<p_phdacq->nsamples; _i++ )
  {
    p_phdacq->phd1_fine[_i] = p_samples[4*_i];
    p_phdacq->phd2_fine[_i] = p_samples[4*_i + 1];
    p_phdacq->phd1_coarse[_i] = p_samples[4*_i + 2];
    p_phdacq->phd2_coarse[_i] = p_samples[4*_i + 3];
  }
}



struct mclock_phd_gplot_window
{
  struct gplot_window *gwin;

  struct gplot_plot *phd1;
  struct gplot_plot *phd2;

  struct gplot_plots *plots;
};



static struct mclock_phd_gplot_window *mclock_phd_gplot_window_instantiate(
                                                  struct gplot_window *p_gwin )
{
  struct mclock_phd_gplot_window *_win;

  _win = xzmalloc( sizeof(struct mclock_phd_gplot_window) );

  _win->gwin = p_gwin;

  _win->phd1 = gplot_plot_instantiate(gplot_binary_format_float64,
                                      gplot_binary_format_none,
                                      "Phd1" );

  _win->phd2 = gplot_plot_instantiate(gplot_binary_format_float64,
                                      gplot_binary_format_none,
                                      "Phd2" );



  _win->plots = gplot_plots_instantiate();

  gplot_plots_add_plot( _win->plots, _win->phd1 );
  gplot_plots_add_plot( _win->plots, _win->phd2 );

  return _win;
}


struct mclock_phd_gplot
{
  FILE *gplot;

  struct mclock_phd_gplot_window *gwfine;
  struct mclock_phd_gplot_window *gwcoarse;
};


struct mclock_phd_gplot *mclock_phd_gplot_instantiate(bool p_dispfine,
                                                      bool p_dispcoarse,
                                                      char *p_gdisplay )
{
  struct mclock_phd_gplot *_phdgplot;
  FILE *_gplot;
  struct gplot_window *_gwin;

  _phdgplot = xzmalloc( sizeof(struct mclock_phd_gplot) );

  _gplot = gplot_open_session( p_gdisplay );
  _phdgplot->gplot = _gplot;

  if ( p_dispfine == true )
  {
    _gwin = gplot_window_instantiate( _gplot, 0, "Phd fine" );
    _phdgplot->gwfine = mclock_phd_gplot_window_instantiate( _gwin );
  }

  if ( p_dispcoarse == true )
  {
    _gwin = gplot_window_instantiate( _gplot, 1, "Phd coarse" );
    _phdgplot->gwcoarse = mclock_phd_gplot_window_instantiate( _gwin );
  }

  return _phdgplot;
}



static void mclock_phd_gplot_window_display(
                                          FILE *p_gplot,
                                          struct mclock_phd_gplot_window *p_win,
                                          unsigned long p_nsamples,
                                          double *p_phd1,
                                          double *p_phd2 )
{
  gplot_window_set( p_win->gwin );

  gplot_plot_set_data( p_win->phd1, p_phd1, NULL, p_nsamples );
  gplot_plot_set_data( p_win->phd2, p_phd2, NULL, p_nsamples );

  gplot_plots_draw( p_win->plots, p_gplot );

  fflush( p_gplot );
}



void mclock_phd_gplot_display(struct mclock_phd_gplot *p_phdgplot,
                              struct mclock_phd_acq *p_phdacq )
{
  if ( p_phdgplot->gwfine )
  {
    mclock_phd_gplot_window_display(p_phdgplot->gplot,
                                    p_phdgplot->gwfine,
                                    p_phdacq->nsamples,
                                    p_phdacq->phd1_fine,
                                    p_phdacq->phd2_fine );
  }

  if ( p_phdgplot->gwcoarse )
  {
    mclock_phd_gplot_window_display(p_phdgplot->gplot,
                                    p_phdgplot->gwcoarse,
                                    p_phdacq->nsamples,
                                    p_phdacq->phd1_coarse,
                                    p_phdacq->phd2_coarse );
  }
}


void mclock_phd_record( char *p_path,
                        unsigned long p_cycle,
                        struct mclock_phd_acq *p_phdacq )
{
  char *_filepath;
  FILE *_f;
  unsigned long _i;

  xasprintf( &_filepath, "%s/phd.%06lu", p_path, p_cycle );

  _f = xfopen( _filepath, "w" );
  xfree( _filepath );

  fprintf( _f, "# phd1_fine phd2_fine phd1_coarse phd2_coarse\n" );
  for( _i=0; _i<p_phdacq->nsamples; _i++ )
  {
    fprintf( _f, "%f %f %f %f\n",
              p_phdacq->phd1_fine[_i],
              p_phdacq->phd2_fine[_i],
              p_phdacq->phd1_coarse[_i],
              p_phdacq->phd2_coarse[_i] );
  }

  fclose( _f );
}

void mclock_phd_compute_calculated_variables( char *p_path,
                                              unsigned long p_cycle,
                                              struct mclock_phd_acq *p_phdacq,
                                              struct mclock_calc_vars *p_calcvars )
{
  unsigned long _i;
  
  double _mean_1 = 0;
  double _mean_2 = 0;
  
  int _range_1_start = 9;
  int _range_1_stop = 104;
  int _range_2_start = 318;
  int _range_2_stop = 413;
  
  // printf("in:  mclock_phd_compute_calculated_variables\n" );
  
  for( _i = 0 ; _i < p_phdacq->nsamples ; _i++ )
  {
    p_calcvars->phd_ratio_coarse->trace[_i] = p_phdacq->phd1_coarse[_i] / p_phdacq->phd2_coarse[_i];
  }

  // mean of first pulse
  for( _i = _range_1_start ; _i <= _range_1_stop ; _i++ )
  {
    _mean_1 += p_calcvars->phd_ratio_coarse->trace[_i];
  }
  _mean_1 = _mean_1/(_range_1_stop - _range_1_start + 1);
  
  // mean of second pulse
  for( _i = _range_2_start ; _i <= _range_2_stop ; _i++ )
  {
    _mean_2 += p_calcvars->phd_ratio_coarse->trace[_i];
  }
  _mean_2 = _mean_2/(_range_2_stop - _range_2_start + 1);
  
  // atom number calculated from coarse photodiodes
  p_calcvars->atom_number_c = -log( _mean_1 / _mean_2 );
  
  // printf("atom_number_c: %f\n", p_calcvars->atom_number_c);
  
  // printf("out: mclock_phd_compute_calculated_variables\n" );
}

