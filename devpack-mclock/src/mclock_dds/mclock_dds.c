
#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xalloc.h"


#include "rtts_session.h"

#include "dds_iface_null.h"
#include "ad9959.h"
#include "ad9912.h"

#include "mclock_conf_hw.h"

#include "mclock_internal.h"

//
// Les chipselect physiques des DDS (conf chipselectlayout)
//

#define AD9959_CS 0
#define AD9912_1_CS 1
#define AD9912_2_CS 2

#define AD9959_SPI_FCLK 4000000
#define AD9912_SPI_FCLK 3000000


enum mclock_dds_id
{
  mclock_dds_unk,
  mclock_dds_ad9959,
  mclock_dds_ad9912_1,
  mclock_dds_ad9912_2,
};


struct mclock_dds
{
  // les DDS
  struct dds_iface *ddsiface;

  struct ad9959_dds *ad9959;
  struct ad9912_dds *ad9912_1;
  struct ad9912_dds *ad9912_2;
};


struct mclock_dds *mclock_dds_instantiate(double p_ad9959_freq,
                                          unsigned long p_add9959_pllmult,
                                          double p_ad9912_1_freq,
                                          double p_ad9912_2_freq )
{
  struct mclock_dds *_mdds;

  _mdds = xzmalloc( sizeof(struct mclock_dds) );


  _mdds->ddsiface = dds_iface_null_instantiate();

  // AD9959
  _mdds->ad9959 = ad9959_dds_instantiate( _mdds->ddsiface, p_ad9959_freq );
  if ( ( p_add9959_pllmult !=0 ) && ( p_add9959_pllmult !=1 ) )
    ad9959_dds_init_internal_pll( _mdds->ad9959, p_add9959_pllmult, true );

  // AD9912_1
  _mdds->ad9912_1 = ad9912_dds_instantiate( _mdds->ddsiface,
                                            p_ad9912_1_freq,
                                            false );

  // AD9912_2
  _mdds->ad9912_2 = ad9912_dds_instantiate( _mdds->ddsiface,
                                            p_ad9912_2_freq,
                                            false );


  return _mdds;

}



static void mclock_dds_update(struct apts *p_apts,
                              enum mclock_dds_id p_id,
                              double p_time )
{
  char *_updline;

  switch( p_id )
  {
    case mclock_dds_ad9959:
      _updline = MCLOCK_AD9959_IO_UPDATE_NIDAQ_DLINE;
      break;

    case mclock_dds_ad9912_1:
      _updline = MCLOCK_AD9912_1_UPDATE_NIDAQ_DLINE;
      break;

    case mclock_dds_ad9912_2:
      _updline = MCLOCK_AD9912_2_UPDATE_NIDAQ_DLINE;
      break;

    default:
      fprintf( stderr, "mclock_dds_update : unknown dds id\n" );
      exit(1);
      break;
  }

  // discret UPDATE
  apts_set_single_pulse(p_apts,
                        NULL,
                        p_time,
                        _updline,
                        1,
                        MCLOCK_DDS_UPDATE_PULSE_WIDTH );
}




static double mclock_dds_spi_write_duration(size_t p_spilen,
                                            unsigned long p_spiflck )
{
  double _duration;

  // on compte 10 bits par mot
  // et un mot de rab a la fin
  _duration = ( 10. * ( p_spilen + 1 ) ) / (double)p_spiflck;

  return _duration;
}


static void mclock_dds_spi_write_and_update(struct apts *p_apts,
                                            struct rtts_session *p_rtts,
                                            struct mclock_dds *p_mdds,
                                            char *p_callername,
                                            enum mclock_dds_id p_id,
                                            bool p_update,
                                            char *p_timeref,
                                            double p_timeoff )
{
  void *_spibuf;
  size_t _spilen;
  unsigned _cs;
  unsigned long _spifclk;

  double _updatetime;
  double _duration;
  double _spitime;


  switch( p_id )
  {
    case mclock_dds_ad9959:
      _cs = AD9959_CS;
      _spifclk = AD9959_SPI_FCLK;
      break;

    case mclock_dds_ad9912_1:
      _cs = AD9912_1_CS;
      _spifclk = AD9912_SPI_FCLK;
      break;

    case mclock_dds_ad9912_2:
      _cs = AD9912_2_CS;
      _spifclk = AD9912_SPI_FCLK;
      break;

    default:
      fprintf( stderr, "mclock_dds_spi_write_and_update : "
                "caller = %s; unknown dds id\n", p_callername );
      exit(1);
      break;
  }

  dds_iface_spibuf_get( p_mdds->ddsiface, &_spibuf, &_spilen );
#if 0
  printf("mclock_dds_spi_write_and_update : %s - cs = %lu, "
          "spifclk = %lu, spilen = %lu\n",
          p_callername, _cs, _spifclk , _spilen );
#endif

  // calcul de la duree de la transaction
  _duration = mclock_dds_spi_write_duration( _spilen, _spifclk );

  //
  // Ecriture de la transaction spi dans le buffer RTTS
  //
  rtts_session_add_event_spi_xfer(p_rtts,
                                  _cs,
                                  _spifclk,
                                  _spilen,
                                  _spibuf );
  rtts_session_add_group( p_rtts );
  rtts_session_flush( p_rtts );

  // RAZ du buffer spi
  dds_iface_spibuf_clear( p_mdds->ddsiface );


  // calcul du temps correspondant au debut de la transaction spi
  _updatetime = apts_timeref_get_time( p_apts, p_timeref, p_timeoff );
  _spitime = _updatetime - _duration;

  // RTTS trig qui va marquer le debut de la transaction spi
  apts_set_single_pulse(p_apts,
                        NULL,
                        _spitime,
                        MCLOCK_RTTS_TRIG_NIDAQ_DLINE,
                        1,
                        MCLOCK_RTTS_TRIG_PULSE_WIDTH );

  if ( p_update == true )
    mclock_dds_update( p_apts, p_id, _updatetime );
}




void mclock_dds_cooling_reset(struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff )
{
  double _treset;
  double _tpostreset;

  _treset = apts_timeref_get_time( p_mclock->apts, p_timeref, p_timeoff );

  //
  // reset physique via le gpio AD9959_RESET de la RPI
  //
  rtts_session_add_event_gpio_pulse(p_mclock->rtts,
                                    MCLOCK_AD9959_RESET_RPI_GPIO,
                                    1,
                                    MCLOCK_AD9959_RESET_PULSE_WIDTH );
  rtts_session_add_group( p_mclock->rtts );
  // le rtts_session_flush va etre fait par mclock_dds_spi_write_and_update

  // RTTS trig associe au reset
  apts_set_single_pulse(p_mclock->apts,
                        p_timeref,
                        p_timeoff,
                        MCLOCK_RTTS_TRIG_NIDAQ_DLINE,
                        1,
                        MCLOCK_RTTS_TRIG_PULSE_WIDTH );



  // on laisse 100 usec de rab apres le reset
  _tpostreset = _treset + MCLOCK_AD9959_RESET_PULSE_WIDTH + 100e-6;

  // Ce reset va permettre de remplir le buffer SPI pour la mise a jour de
  // la valeur du PLL multiplier
  ad9959_reset( p_mclock->dds->ad9959 );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9959_reset",
                                  mclock_dds_ad9959,
                                  true,
                                  NULL,
                                  _tpostreset );
}



static struct ad9912_dds *mclock_dds_ad9912_get(struct mclock_dds *p_mdds,
                                                enum mclock_dds_id p_id )
{
  switch( p_id )
  {
    case mclock_dds_ad9912_1:
      return p_mdds->ad9912_1;
      break;

    case mclock_dds_ad9912_2:
      return p_mdds->ad9912_2;
      break;

    default:
      fprintf( stderr, "mclock_dds_ad9912_get : unsupported DDS id\n" );
      exit(1);
      break;
  }

  return NULL;
}


static void mclock_dds_ad9912_reset(struct mclock *p_mclock,
                                    enum mclock_dds_id p_id,
                                    char *p_timeref,
                                    double p_timeoff )
{
  // le reset est effectue en soft sur ces DDS
  ad9912_soft_reset( mclock_dds_ad9912_get( p_mclock->dds, p_id ) );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9912_reset",
                                  p_id,
                                  false,
                                  p_timeref,
                                  p_timeoff );
}



void mclock_dds_repumper_reset( struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff )
{
  mclock_dds_ad9912_reset( p_mclock, mclock_dds_ad9912_1, p_timeref, p_timeoff );
}


void mclock_dds_cavity_reset( struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff )
{
  mclock_dds_ad9912_reset( p_mclock, mclock_dds_ad9912_2, p_timeref, p_timeoff );
}



static void mclock_dds_ad9959_set_freq( struct mclock *p_mclock,
                                        char *p_timeref,
                                        double p_timeoff,
                                        bool p_update,
                                        double p_freq )
{
  struct ad9959_dds *_dds = p_mclock->dds->ad9959;
  unsigned long _ftw;

  ad9959_compute_frequency_tuning_word( _dds,  p_freq, &_ftw, NULL );

  ad9959_set_single_tone_ftw_pow( _dds,
                                  MCLOCK_AD9959_PLL_COOLER_CHANNEL,
                                  true,
                                  _ftw,
                                  false,
                                  0 );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9959_set_freq",
                                  mclock_dds_ad9959,
                                  p_update,
                                  p_timeref,
                                  p_timeoff );
}

void mclock_dds_cooling_set_freq( struct mclock *p_mclock,
                                  char *p_timeref,
                                  double p_timeoff,
                                  bool p_update,
                                  double p_freq )
{
  mclock_dds_ad9959_set_freq( p_mclock,
                              p_timeref,
                              p_timeoff,
                              p_update,
                              p_freq / 8. );
}


static void mclock_dds_ad9959_set_linear_sweep( struct mclock *p_mclock,
                                                char *p_timeref,
                                                double p_timeoff,
                                                bool p_update,
                                                double p_fstart,
                                                double p_fend,
                                                double p_stepduration,
                                                double p_duration )
{
  ad9959_set_linear_sweep_ramp_rate_given(p_mclock->dds->ad9959,
                                          MCLOCK_AD9959_PLL_COOLER_CHANNEL,
                                          0, // les PPIN des cartes d'eval AD9959 sont pull downees par defaut
                                          p_fstart,
                                          p_fend,
                                          p_stepduration,
                                          p_duration );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_a_ad9959_set_linear_sweep",
                                  mclock_dds_ad9959,
                                  p_update,
                                  p_timeref,
                                  p_timeoff );
}

void mclock_dds_cooling_set_linear_sweep( struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_fstart,
                                          double p_fend,
                                          double p_stepduration,
                                          double p_duration )
{
  mclock_dds_ad9959_set_linear_sweep( p_mclock,
                                      p_timeref,
                                      p_timeoff,
                                      p_update,
                                      p_fstart / 8.,
                                      p_fend / 8.,
                                      p_stepduration,
                                      p_duration );
}


static void mclock_dds_ad9959_update( struct mclock *p_mclock,
                                      char *p_timeref,
                                      double p_timeoff )
{
  double _time = apts_timeref_get_time( p_mclock->apts, p_timeref, p_timeoff );

  mclock_dds_update( p_mclock->apts, mclock_dds_ad9959, _time );
}

void mclock_dds_cooling_update( struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff )
{
  mclock_dds_ad9959_update( p_mclock, p_timeref, p_timeoff );
}










static void mclock_dds_ad9912_set_freq( struct mclock *p_mclock,
                                        enum mclock_dds_id p_id,
                                        char *p_timeref,
                                        double p_timeoff,
                                        bool p_update,
                                        double p_freq )
{
  ad9912_set_frequency( mclock_dds_ad9912_get( p_mclock->dds, p_id ), p_freq );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9912_set_freq",
                                  p_id,
                                  p_update,
                                  p_timeref,
                                  p_timeoff );
}


static void mclock_dds_ad9912_1_set_freq( struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_freq )
{
  mclock_dds_ad9912_set_freq( p_mclock,
                              mclock_dds_ad9912_1,
                              p_timeref,
                              p_timeoff,
                              p_update,
                              p_freq );
}


void mclock_dds_repumper_set_freq(struct mclock *p_mclock,
                                  char *p_timeref,
                                  double p_timeoff,
                                  bool p_update,
                                  double p_freq )
{
  mclock_dds_ad9912_1_set_freq( p_mclock,
                                p_timeref,
                                p_timeoff,
                                p_update,
                                p_freq );
}





static void mclock_dds_ad9912_2_set_freq( struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_freq )
{
  mclock_dds_ad9912_set_freq( p_mclock,
                              mclock_dds_ad9912_2,
                              p_timeref,
                              p_timeoff,
                              p_update,
                              p_freq );
}


void mclock_dds_cavity_set_freq(struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff,
                                bool p_update,
                                double p_freq )
{
  mclock_dds_ad9912_2_set_freq( p_mclock,
                                p_timeref,
                                p_timeoff,
                                p_update,
                                p_freq );
}


static void mclock_dds_ad9912_set_phase(struct mclock *p_mclock,
                                        enum mclock_dds_id p_id,
                                        char *p_timeref,
                                        double p_timeoff,
                                        bool p_update,
                                        double p_phase )
{
  ad9912_set_phase( mclock_dds_ad9912_get( p_mclock->dds, p_id ), p_phase );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9912_set_phase",
                                  p_id,
                                  p_update,
                                  p_timeref,
                                  p_timeoff );
}

static void mclock_dds_ad9912_1_set_phase(struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_phase )
{
  mclock_dds_ad9912_set_phase(p_mclock,
                              mclock_dds_ad9912_1,
                              p_timeref,
                              p_timeoff,
                              p_update,
                              p_phase );
}

static void mclock_dds_ad9912_2_set_phase(struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_phase )
{
  mclock_dds_ad9912_set_phase(p_mclock,
                              mclock_dds_ad9912_2,
                              p_timeref,
                              p_timeoff,
                              p_update,
                              p_phase );
}


static void mclock_dds_ad9912_set_freq_phase( struct mclock *p_mclock,
                                              enum mclock_dds_id p_id,
                                              char *p_timeref,
                                              double p_timeoff,
                                              bool p_update,
                                              double p_freq,
                                              double p_phase )
{
  ad9912_set_freq_phase(mclock_dds_ad9912_get( p_mclock->dds, p_id ),
                        p_freq,
                        p_phase );

  mclock_dds_spi_write_and_update(p_mclock->apts,
                                  p_mclock->rtts,
                                  p_mclock->dds,
                                  "mclock_dds_ad9912_set_freq_phase",
                                  p_id,
                                  p_update,
                                  p_timeref,
                                  p_timeoff );
}


static void mclock_dds_ad9912_1_set_freq_phase( struct mclock *p_mclock,
                                                char *p_timeref,
                                                double p_timeoff,
                                                bool p_update,
                                                double p_freq,
                                                double p_phase )
{
  mclock_dds_ad9912_set_freq_phase( p_mclock,
                                    mclock_dds_ad9912_1,
                                    p_timeref,
                                    p_timeoff,
                                    p_update,
                                    p_freq,
                                    p_phase );
}


static void mclock_dds_ad9912_2_set_freq_phase( struct mclock *p_mclock,
                                                char *p_timeref,
                                                double p_timeoff,
                                                bool p_update,
                                                double p_freq,
                                                double p_phase )
{
  mclock_dds_ad9912_set_freq_phase( p_mclock,
                                    mclock_dds_ad9912_2,
                                    p_timeref,
                                    p_timeoff,
                                    p_update,
                                    p_freq,
                                    p_phase );
}



static void mclock_dds_ad9912_1_update( struct mclock *p_mclock,
                                        char *p_timeref,
                                        double p_timeoff )
{
  double _time = apts_timeref_get_time( p_mclock->apts, p_timeref, p_timeoff );

  mclock_dds_update( p_mclock->apts, mclock_dds_ad9912_1, _time );
}

void mclock_dds_repumper_update(struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff )
{
  mclock_dds_ad9912_1_update( p_mclock, p_timeref, p_timeoff );
}



static void mclock_dds_ad9912_2_update( struct mclock *p_mclock,
                                        char *p_timeref,
                                        double p_timeoff )
{
  double _time = apts_timeref_get_time( p_mclock->apts, p_timeref, p_timeoff );

  mclock_dds_update( p_mclock->apts, mclock_dds_ad9912_2, _time );
}


void mclock_dds_cavity_update(struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff )
{
  mclock_dds_ad9912_2_update( p_mclock, p_timeref, p_timeoff );
}



