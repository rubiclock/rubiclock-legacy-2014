
#ifndef MCLOCK_DDS_H
#define MCLOCK_DDS_H

#include "mclock_internal.h"

struct mclock_dds;

struct mclock_dds *mclock_dds_instantiate(double p_ad9959_freq,
                                          unsigned long p_add9959_pllmult,
                                          double p_ad9912_1_freq,
                                          double p_ad9912_2_freq );


void mclock_dds_cooling_reset(struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff );


void mclock_dds_repumper_reset( struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff );


void mclock_dds_cavity_reset( struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff );






void mclock_dds_cooling_set_freq( struct mclock *p_mclock,
                                  char *p_timeref,
                                  double p_timeoff,
                                  bool p_update,
                                  double p_freq );

void mclock_dds_cooling_set_linear_sweep( struct mclock *p_mclock,
                                          char *p_timeref,
                                          double p_timeoff,
                                          bool p_update,
                                          double p_fstart,
                                          double p_fend,
                                          double p_stepduration,
                                          double p_duration );


void mclock_dds_cooling_update( struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff );




void mclock_dds_repumper_set_freq(struct mclock *p_mclock,
                                  char *p_timeref,
                                  double p_timeoff,
                                  bool p_update,
                                  double p_freq );

void mclock_dds_cavity_set_freq(struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff,
                                bool p_update,
                                double p_freq );


void mclock_dds_repumper_update(struct mclock *p_mclock,
                                char *p_timeref,
                                double p_timeoff );

void mclock_dds_cavity_update(struct mclock *p_mclock,
                              char *p_timeref,
                              double p_timeoff );



#endif // MCLOCK_DDS_H
