
#ifndef MCLOCK_CONF_HW_H
#define MCLOCK_CONF_HW_H

#include "apts.h"
#include "apts_nidaq.h"


#define MCLOCK_RTTS_TRIG_NIDAQ_DLINE "rtts_trig"
#define MCLOCK_RTTS_TRIG_PULSE_WIDTH 2e-6


#define MCLOCK_AD9959_PLL_COOLER_CHANNEL 0

#define MCLOCK_AD9959_RESET_RPI_GPIO 24
#define MCLOCK_AD9959_RESET_PULSE_WIDTH 100e-6
#define MCLOCK_AD9959_IO_UPDATE_NIDAQ_DLINE "ad9959_io_update"

#define MCLOCK_AD9912_1_UPDATE_NIDAQ_DLINE "ad9912_1_update"
#define MCLOCK_AD9912_2_UPDATE_NIDAQ_DLINE "ad9912_2_update"

#define MCLOCK_DDS_UPDATE_PULSE_WIDTH 2e-6


struct apts *mclock_conf_hw_nidaqseq_register(void);


#endif // MCLOCK_CONF_HW_H
