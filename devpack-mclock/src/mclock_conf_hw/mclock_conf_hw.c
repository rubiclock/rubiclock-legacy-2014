
#include <stddef.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"

#include "apts.h"
#include "apts_nidaq.h"

#include "mclock_conf_hw.h"


static struct apts_nidaq_dline __apts_nidaq_dline[] =
{
  { "trigscope1", 3 },
  { "trigscope2", 4 },

  { MCLOCK_RTTS_TRIG_NIDAQ_DLINE, 5 },
  { MCLOCK_AD9959_IO_UPDATE_NIDAQ_DLINE, 7 },
  { MCLOCK_AD9912_1_UPDATE_NIDAQ_DLINE, 8 },
  { MCLOCK_AD9912_2_UPDATE_NIDAQ_DLINE, 9 },

  { "AOM_sw", 11 },
  { "Cooling_shutter", 12 },
  { "Vertical_shutter", 13 },
  { "Repumper_sw", 14 },
  { "Dispenser_TTL", 15 },
  { "Trig_datalogger", 16 },
  { "Cavity_sw", 17 }
};

static struct apts_nidaq_aochannel __apts_nidaq_aochannel[] =
{
  { "AOM_amp", "Dev1/ao0", -5, 5 },
  { "Repumper_amp", "Dev1/ao1", -5, 5 },
  { "phd_offset", "Dev1/ao2", -10, 10 },
  { "vco", "Dev1/ao3", -10, 10 }
};



static struct apts_nidaq_aichannel __apts_nidaq_aichannel[] =
{
  { "phd1_fine", "Dev1/ai0", -2, 2 },
  { "phd2_fine", "Dev1/ai1", -2, 2 },
  { "phd1_coarse", "Dev1/ai2", -10, 10 },
  { "phd2_coarse", "Dev1/ai3", -10, 10 },
};



static struct apts_nidaq_serialline __apts_nidaq_serialline[] = 
{
  {
    .name = "serial_rpi",
    .dline = 10,
    .ratedivisor = 1,
    .bitsperword = 8,
    .maxwords = 64,
    .idlebits = 0,
    .invpolarity = false
  }
};



static void mclock_apts_nidaq_conf_override(struct apts_nidaq_conf *p_conf )
{
  // on veut utiliser la clock interne du DAQ
  p_conf->extclocksrcterm = NULL;
}



struct apts *mclock_conf_hw_nidaqseq_register(void)
{

  struct apts_nidaqseq *_nidaqseq;
  struct apts *_apts;


  _nidaqseq = apts_nidaqseq_instantiate(mclock_apts_nidaq_conf_override,

                                        ARRAY_SIZE(__apts_nidaq_dline),
                                        __apts_nidaq_dline,

                                        ARRAY_SIZE(__apts_nidaq_aochannel),
                                        __apts_nidaq_aochannel,

                                        ARRAY_SIZE(__apts_nidaq_aichannel),
                                        __apts_nidaq_aichannel,

                                        0,
                                        NULL,

                                        0,
                                        NULL,

                                        0,
                                        NULL,

                                        ARRAY_SIZE(__apts_nidaq_serialline),
                                        __apts_nidaq_serialline );

  _apts = apts_instantiate();

  apts_register_daqseq( _apts,
                        _nidaqseq,
                        apts_nidaq_get_seqfreq,
                        apts_nidaq_get_dline_by_name,
                        apts_nidaq_get_ao_channel_by_name,
                        apts_nidaq_get_ai_channel_by_name,
                        NULL,
                        NULL,
                        apts_nidaq_get_serial_line_by_name,
                        apts_nidaq_set_ao_sampling_period_check,
                        apts_nidaq_get_ai_check,
                        apts_nidaqseq_register_loadseq_callback,
                        apts_nidaqseq_run_sequences,
                        apts_nidaqseq_run_sequences_reset );

  return _apts;
}



