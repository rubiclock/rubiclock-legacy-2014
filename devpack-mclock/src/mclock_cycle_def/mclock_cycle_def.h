
#ifndef MCLOCK_CYCLE_DEF_H
#define MCLOCK_CYCLE_DEF_H

#include <stdbool.h>

#include "conf_param.h"

struct mclock_cycle_def
{
  bool Cooling;
  bool Cooling_to_SDC1_transition;
  bool SDC1;
  bool SDC1_to_SDC2_transition;
  bool SDC2;
  bool SDC2_to_Blue;
  bool Blue;
  bool Blue_to_Depumping;
  bool Depumping;
  bool Expansion;
  bool Pi2_1;
  bool Ramsey;
  bool Pi2_2;
  bool Detection_wait;
  bool Detection;
  bool Push_Depump;
  bool Detection_thermal_zero;
  bool Detection_thermal_wait;
  bool Detection_thermal;
  bool Dead_time;
};


struct mclock_cycle_def *mclock_cycle_def_instantiate(void);


struct mclock_cycle_def *mclock_cycle_def_duplicate(
                                              struct mclock_cycle_def *p_cdef );

void mclock_cycle_def_free( struct mclock_cycle_def *p_cdef );



struct mclock_cycle_def *mclock_cycle_def_conf_params_instantiate(
                                            struct conf_params **p_confparams );

#endif // MCLOCK_CYCLE_DEF_H
