

#include <string.h>

#include "ptrarith.h"
#include "xalloc.h"

#include "conf_param.h"

#include "mclock_cycle_def.h"




struct mclock_cycle_def *mclock_cycle_def_instantiate(void)
{
  struct mclock_cycle_def *_cdef;
  unsigned long _i;

  _cdef = xzmalloc( sizeof(struct mclock_cycle_def) );

  for( _i=0; _i<sizeof(struct mclock_cycle_def)/sizeof(bool); _i++ )
  {
    PTR_ADD_DEREF( _cdef, sizeof(bool)*_i, bool ) = true;
  }
  return _cdef;
}

struct mclock_cycle_def *mclock_cycle_def_duplicate(
                                              struct mclock_cycle_def *p_cdef )
{
  struct mclock_cycle_def *_cdef;

  _cdef = xzmalloc( sizeof(struct mclock_cycle_def) );

  memcpy( _cdef, p_cdef, sizeof(struct mclock_cycle_def) );

  return _cdef;
}


void mclock_cycle_def_free( struct mclock_cycle_def *p_cdef )
{
  xfree( p_cdef );
}



struct mclock_cycle_def *mclock_cycle_def_conf_params_instantiate(
                                            struct conf_params **p_confparams )
{
  struct mclock_cycle_def *_cdef;
  struct conf_params *_confparams;

  _cdef = xzmalloc( sizeof(struct mclock_cycle_def) );

  _confparams = conf_params_instantiate( "MCLOCK_CYCLE_DEF" );

  conf_params_set_baseptr( _confparams, _cdef );

  conf_param_instantiate( _confparams, "Cooling", conf_param_type_bool, &_cdef->Cooling );
  conf_param_instantiate( _confparams, "Cooling_to_SDC1_transition", conf_param_type_bool, &_cdef->Cooling_to_SDC1_transition );
  conf_param_instantiate( _confparams, "SDC1", conf_param_type_bool, &_cdef->SDC1 );
  conf_param_instantiate( _confparams, "SDC1_to_SDC2_transition", conf_param_type_bool, &_cdef->SDC1_to_SDC2_transition );
  conf_param_instantiate( _confparams, "SDC2", conf_param_type_bool, &_cdef->SDC2 );
  conf_param_instantiate( _confparams, "SDC2_to_Blue", conf_param_type_bool, &_cdef->SDC2_to_Blue );
  conf_param_instantiate( _confparams, "Blue", conf_param_type_bool, &_cdef->Blue );
  conf_param_instantiate( _confparams, "Blue_to_Depumping", conf_param_type_bool, &_cdef->Blue_to_Depumping );
  conf_param_instantiate( _confparams, "Depumping", conf_param_type_bool, &_cdef->Depumping );
  conf_param_instantiate( _confparams, "Expansion", conf_param_type_bool, &_cdef->Expansion );
  conf_param_instantiate( _confparams, "Pi2_1", conf_param_type_bool, &_cdef->Pi2_1 );
  conf_param_instantiate( _confparams, "Ramsey", conf_param_type_bool, &_cdef->Ramsey );
  conf_param_instantiate( _confparams, "Pi2_2", conf_param_type_bool, &_cdef->Pi2_2 );
  conf_param_instantiate( _confparams, "Detection_wait", conf_param_type_bool, &_cdef->Detection_wait );
  conf_param_instantiate( _confparams, "Detection", conf_param_type_bool, &_cdef->Detection );
  conf_param_instantiate( _confparams, "Push_Depump", conf_param_type_bool, &_cdef->Push_Depump );
  conf_param_instantiate( _confparams, "Detection_thermal_zero", conf_param_type_bool, &_cdef->Detection_thermal_zero );
  conf_param_instantiate( _confparams, "Detection_thermal_wait", conf_param_type_bool, &_cdef->Detection_thermal_wait );
  conf_param_instantiate( _confparams, "Detection_thermal", conf_param_type_bool, &_cdef->Detection_thermal );
  conf_param_instantiate( _confparams, "Dead_time", conf_param_type_bool, &_cdef->Dead_time );

  if ( p_confparams != NULL )
    *p_confparams = _confparams;

  return _cdef;
}
