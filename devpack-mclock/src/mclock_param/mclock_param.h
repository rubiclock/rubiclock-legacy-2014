
#ifndef MCLOCK_PARAM_H
#define MCLOCK_PARAM_H

#include <stdbool.h>

#include "conf_param.h"

struct mclock_param
{
  char *rtts_server;

  //
  // Display gnuplot
  //
  char *gplot_detection;

  //
  // Acquisition
  //

  double phd_offset;

  bool use_raw_detection_acquisition;
  double acquisition_freq;
  double acquisition_interchannel_gap;
  bool display_fine_phd;
  bool display_coarse_phd;


  //
  // USBDUX ADC
  //
  bool use_duxadc;
  char *duxadc_server;
  char *duxadc_filerecord;
  unsigned long duxadc_nchannels;
  double duxadc_freq;
  double duxadc_shotperiod;



  //
  // DDS
  //
  double ad9959_freq;
  unsigned long add9959_pllmult;
  double ad9959_stepduration;

  double ad9912_freq;


  //
  // Caracteristiques physiques des Shutters
  //
  double cooling_shutter_opening_delay;
  double cooling_shutter_closing_delay;

  double vertical_shutter_opening_delay;
  double vertical_shutter_closing_delay;



  //
  // parametres definissant les temps dans un cycle
  //

  // Cooling
  double Cooling_length;
  double Cooling_AOM_amp;
  double Cooling_Repumper_amp;
  double Cooling_Repumper_DDS_frequency;
  double Cooling_Cavity_DDS_frequency;
  double Cooling_Cooling_DDS_frequency;
  double Cooling_VCO_setpoint;
  double Cooling_VCO_repumper;
  //TTLs
  unsigned long Cooling_TTL_trigscope1;
  unsigned long Cooling_TTL_trigscope2;
  unsigned long Cooling_TTL_AOM_sw;
  unsigned long Cooling_TTL_Cooling_shutter;
  unsigned long Cooling_TTL_Vertical_shutter;
  unsigned long Cooling_TTL_Repumper_sw;
  unsigned long Cooling_TTL_Dispenser_TTL;
  unsigned long Cooling_TTL_Trig_datalogger;
  unsigned long Cooling_TTL_Cavity_sw;
  /*
  trigscope1;
  trigscope2;
  AOM_sw;
  Cooling_shutter;
  Vertical_shutter;
  Repumper_sw;
  Dispenser_TTL;
  Trig_datalogger;
  Cavity_sw;
  */
  
  // Cooling_to_SDC1_transition
  double SDC1_trans_length;
  //TTLs
  unsigned long SDC1_trans_TTL_trigscope1;
  unsigned long SDC1_trans_TTL_trigscope2;
  unsigned long SDC1_trans_TTL_AOM_sw;
  unsigned long SDC1_trans_TTL_Cooling_shutter;
  unsigned long SDC1_trans_TTL_Vertical_shutter;
  unsigned long SDC1_trans_TTL_Repumper_sw;
  unsigned long SDC1_trans_TTL_Dispenser_TTL;
  unsigned long SDC1_trans_TTL_Trig_datalogger;
  unsigned long SDC1_trans_TTL_Cavity_sw;
  
  // SDC1
  double SDC1_length;
  double SDC1_AOM_amp;
  double SDC1_Repumper_amp;
  double SDC1_Repumper_DDS_frequency;
  double SDC1_Cooling_DDS_frequency;
  double SDC1_VCO_setpoint;
  double SDC1_VCO_repumper;
  //TTLs
  unsigned long SDC1_TTL_trigscope1;
  unsigned long SDC1_TTL_trigscope2;
  unsigned long SDC1_TTL_AOM_sw;
  unsigned long SDC1_TTL_Cooling_shutter;
  unsigned long SDC1_TTL_Vertical_shutter;
  unsigned long SDC1_TTL_Repumper_sw;
  unsigned long SDC1_TTL_Dispenser_TTL;
  unsigned long SDC1_TTL_Trig_datalogger;
  unsigned long SDC1_TTL_Cavity_sw;
  
  // SDC1_to_SDC2_transition
  double SDC2_trans_length;
  //TTLs
  unsigned long SDC2_trans_TTL_trigscope1;
  unsigned long SDC2_trans_TTL_trigscope2;
  unsigned long SDC2_trans_TTL_AOM_sw;
  unsigned long SDC2_trans_TTL_Cooling_shutter;
  unsigned long SDC2_trans_TTL_Vertical_shutter;
  unsigned long SDC2_trans_TTL_Repumper_sw;
  unsigned long SDC2_trans_TTL_Dispenser_TTL;
  unsigned long SDC2_trans_TTL_Trig_datalogger;
  unsigned long SDC2_trans_TTL_Cavity_sw;
  
  // SDC2
  double SDC2_length;
  double SDC2_AOM_amp;
  double SDC2_Repumper_amp;
  double SDC2_Repumper_DDS_frequency;
  double SDC2_Cooling_DDS_frequency;
  double SDC2_VCO_setpoint;
  double SDC2_VCO_repumper;
  //TTLs
  unsigned long SDC2_TTL_trigscope1;
  unsigned long SDC2_TTL_trigscope2;
  unsigned long SDC2_TTL_AOM_sw;
  unsigned long SDC2_TTL_Cooling_shutter;
  unsigned long SDC2_TTL_Vertical_shutter;
  unsigned long SDC2_TTL_Repumper_sw;
  unsigned long SDC2_TTL_Dispenser_TTL;
  unsigned long SDC2_TTL_Trig_datalogger;
  unsigned long SDC2_TTL_Cavity_sw;
  
  // SDC2_to_Blue
  double Blue_trans_length;
  //TTLs
  unsigned long Blue_trans_TTL_trigscope1;
  unsigned long Blue_trans_TTL_trigscope2;
  unsigned long Blue_trans_TTL_AOM_sw;
  unsigned long Blue_trans_TTL_Cooling_shutter;
  unsigned long Blue_trans_TTL_Vertical_shutter;
  unsigned long Blue_trans_TTL_Repumper_sw;
  unsigned long Blue_trans_TTL_Dispenser_TTL;
  unsigned long Blue_trans_TTL_Trig_datalogger;
  unsigned long Blue_trans_TTL_Cavity_sw;
  
  // Blue
  double Blue_length;
  double Blue_AOM_amp;
  double Blue_Repumper_amp;
  double Blue_Repumper_DDS_frequency;
  double Blue_Cooling_DDS_frequency;
  double Blue_VCO_setpoint;
  double Blue_VCO_repumper;
  //TTLs
  unsigned long Blue_TTL_trigscope1;
  unsigned long Blue_TTL_trigscope2;
  unsigned long Blue_TTL_AOM_sw;
  unsigned long Blue_TTL_Cooling_shutter;
  unsigned long Blue_TTL_Vertical_shutter;
  unsigned long Blue_TTL_Repumper_sw;
  unsigned long Blue_TTL_Dispenser_TTL;
  unsigned long Blue_TTL_Trig_datalogger;
  unsigned long Blue_TTL_Cavity_sw;
  
  // Blue_to_Depumping
  double Dep_trans_length;
  //TTLs
  unsigned long Dep_trans_TTL_trigscope1;
  unsigned long Dep_trans_TTL_trigscope2;
  unsigned long Dep_trans_TTL_AOM_sw;
  unsigned long Dep_trans_TTL_Cooling_shutter;
  unsigned long Dep_trans_TTL_Vertical_shutter;
  unsigned long Dep_trans_TTL_Repumper_sw;
  unsigned long Dep_trans_TTL_Dispenser_TTL;
  unsigned long Dep_trans_TTL_Trig_datalogger;
  unsigned long Dep_trans_TTL_Cavity_sw;
  
  // Depumping
  double Dep_length;
  double Dep_AOM_amp;
  double Dep_Repumper_amp;
  double Dep_Repumper_DDS_frequency;
  double Dep_Cooling_DDS_frequency;
  double Dep_VCO_setpoint;
  double Dep_VCO_repumper;
  //TTLs
  unsigned long Dep_TTL_trigscope1;
  unsigned long Dep_TTL_trigscope2;
  unsigned long Dep_TTL_AOM_sw;
  unsigned long Dep_TTL_Cooling_shutter;
  unsigned long Dep_TTL_Vertical_shutter;
  unsigned long Dep_TTL_Repumper_sw;
  unsigned long Dep_TTL_Dispenser_TTL;
  unsigned long Dep_TTL_Trig_datalogger;
  unsigned long Dep_TTL_Cavity_sw;
  
  // Expansion
  double expansion_length;
  double expansion_AOM_amp;
  double expansion_Repumper_amp;
  double expansion_Repumper_DDS_frequency;
  double expansion_Cooling_DDS_frequency;
  double expansion_VCO_setpoint;
  double expansion_VCO_repumper;
  //TTLs
  unsigned long expansion_TTL_trigscope1;
  unsigned long expansion_TTL_trigscope2;
  unsigned long expansion_TTL_AOM_sw;
  unsigned long expansion_TTL_Cooling_shutter;
  unsigned long expansion_TTL_Vertical_shutter;
  unsigned long expansion_TTL_Repumper_sw;
  unsigned long expansion_TTL_Dispenser_TTL;
  unsigned long expansion_TTL_Trig_datalogger;
  unsigned long expansion_TTL_Cavity_sw;

  // Pi2_1
  double pi2_1_length;
  //TTLs
  unsigned long pi2_1_TTL_trigscope1;
  unsigned long pi2_1_TTL_trigscope2;
  unsigned long pi2_1_TTL_AOM_sw;
  unsigned long pi2_1_TTL_Cooling_shutter;
  unsigned long pi2_1_TTL_Vertical_shutter;
  unsigned long pi2_1_TTL_Repumper_sw;
  unsigned long pi2_1_TTL_Dispenser_TTL;
  unsigned long pi2_1_TTL_Trig_datalogger;
  unsigned long pi2_1_TTL_Cavity_sw;
  
  // Ramsey
  double T_R;
  double Ramsey_delay_sw;
  double Ramsey_delay_cavity_frequency;
  double Ramsey_Cavity_DDS_frequency;
  //TTLs
  unsigned long Ramsey_TTL_trigscope1;
  unsigned long Ramsey_TTL_trigscope2;
  unsigned long Ramsey_TTL_AOM_sw;
  unsigned long Ramsey_TTL_Cooling_shutter;
  unsigned long Ramsey_TTL_Vertical_shutter;
  unsigned long Ramsey_TTL_Repumper_sw;
  unsigned long Ramsey_TTL_Dispenser_TTL;
  unsigned long Ramsey_TTL_Trig_datalogger;
  unsigned long Ramsey_TTL_Cavity_sw;
  
  // Pi2_2
  double pi2_2_length;
  //TTLs
  unsigned long pi2_2_TTL_trigscope1;
  unsigned long pi2_2_TTL_trigscope2;
  unsigned long pi2_2_TTL_AOM_sw;
  unsigned long pi2_2_TTL_Cooling_shutter;
  unsigned long pi2_2_TTL_Vertical_shutter;
  unsigned long pi2_2_TTL_Repumper_sw;
  unsigned long pi2_2_TTL_Dispenser_TTL;
  unsigned long pi2_2_TTL_Trig_datalogger;
  unsigned long pi2_2_TTL_Cavity_sw;
  
  // Detection_wait
  double det_wait_length;
  double det_wait_AOM_amp;
  double det_wait_Repumper_amp;
  double det_wait_Repumper_DDS_frequency;
  double det_wait_Cooling_DDS_frequency;
  double det_wait_VCO_setpoint;
  double det_wait_VCO_repumper;
  //TTLs
  unsigned long det_wait_TTL_trigscope1;
  unsigned long det_wait_TTL_trigscope2;
  unsigned long det_wait_TTL_AOM_sw;
  unsigned long det_wait_TTL_Cooling_shutter;
  unsigned long det_wait_TTL_Vertical_shutter;
  unsigned long det_wait_TTL_Repumper_sw;
  unsigned long det_wait_TTL_Dispenser_TTL;
  unsigned long det_wait_TTL_Trig_datalogger;
  unsigned long det_wait_TTL_Cavity_sw;
  
  // Detection
  double det_length;
  //TTLs
  unsigned long det_TTL_trigscope1;
  unsigned long det_TTL_trigscope2;
  unsigned long det_TTL_AOM_sw;
  unsigned long det_TTL_Cooling_shutter;
  unsigned long det_TTL_Vertical_shutter;
  unsigned long det_TTL_Repumper_sw;
  unsigned long det_TTL_Dispenser_TTL;
  unsigned long det_TTL_Trig_datalogger;
  unsigned long det_TTL_Cavity_sw;
  
  // Push_Depump
  double push_depump_length;
  double push_depump_AOM_amp;
  double push_depump_Repumper_amp;
  double push_depump_Repumper_DDS_frequency;
  double push_depump_Cooling_DDS_frequency;
  double push_depump_VCO_setpoint;
  double push_depump_VCO_repumper;
  //TTLs
  unsigned long push_depump_TTL_trigscope1;
  unsigned long push_depump_TTL_trigscope2;
  unsigned long push_depump_TTL_AOM_sw;
  unsigned long push_depump_TTL_Cooling_shutter;
  unsigned long push_depump_TTL_Vertical_shutter;
  unsigned long push_depump_TTL_Repumper_sw;
  unsigned long push_depump_TTL_Dispenser_TTL;
  unsigned long push_depump_TTL_Trig_datalogger;
  unsigned long push_depump_TTL_Cavity_sw;

  // Detection_thermal_zero
  double det_th_zero_length;
  double det_th_zero_AOM_amp;
  double det_th_zero_Repumper_amp;
  double det_th_zero_Repumper_DDS_frequency;
  double det_th_zero_Cooling_DDS_frequency;
  double det_th_zero_VCO_setpoint;
  double det_th_zero_VCO_repumper;
  //TTLs
  unsigned long det_th_zero_TTL_trigscope1;
  unsigned long det_th_zero_TTL_trigscope2;
  unsigned long det_th_zero_TTL_AOM_sw;
  unsigned long det_th_zero_TTL_Cooling_shutter;
  unsigned long det_th_zero_TTL_Vertical_shutter;
  unsigned long det_th_zero_TTL_Repumper_sw;
  unsigned long det_th_zero_TTL_Dispenser_TTL;
  unsigned long det_th_zero_TTL_Trig_datalogger;
  unsigned long det_th_zero_TTL_Cavity_sw;

  // Detection_thermal_wait
  double det_th_wait_length;
  double det_th_wait_AOM_amp;
  double det_th_wait_Repumper_amp;
  double det_th_wait_Cooling_DDS_frequency;
  double det_th_wait_VCO_setpoint;
  //TTLs
  unsigned long det_th_wait_TTL_trigscope1;
  unsigned long det_th_wait_TTL_trigscope2;
  unsigned long det_th_wait_TTL_AOM_sw;
  unsigned long det_th_wait_TTL_Cooling_shutter;
  unsigned long det_th_wait_TTL_Vertical_shutter;
  unsigned long det_th_wait_TTL_Repumper_sw;
  unsigned long det_th_wait_TTL_Dispenser_TTL;
  unsigned long det_th_wait_TTL_Trig_datalogger;
  unsigned long det_th_wait_TTL_Cavity_sw;
  
  // Detection_thermal
  double det_th_length;
  //double det_th_AOM_amp;
  //double det_th_Repumper_amp;
  //double det_th_Cooling_DDS_frequency;
  //TTLs
  unsigned long det_th_TTL_trigscope1;
  unsigned long det_th_TTL_trigscope2;
  unsigned long det_th_TTL_AOM_sw;
  unsigned long det_th_TTL_Cooling_shutter;
  unsigned long det_th_TTL_Vertical_shutter;
  unsigned long det_th_TTL_Repumper_sw;
  unsigned long det_th_TTL_Dispenser_TTL;
  unsigned long det_th_TTL_Trig_datalogger;
  unsigned long det_th_TTL_Cavity_sw;
  
  // Dead_time
  double dead_time;
  double Dead_time_AOM_amp;
  double Dead_time_Repumper_amp;
  //TTLs
  unsigned long Dead_time_TTL_trigscope1;
  unsigned long Dead_time_TTL_trigscope2;
  unsigned long Dead_time_TTL_AOM_sw;
  unsigned long Dead_time_TTL_Cooling_shutter;
  unsigned long Dead_time_TTL_Vertical_shutter;
  unsigned long Dead_time_TTL_Repumper_sw;
  unsigned long Dead_time_TTL_Dispenser_TTL;
  unsigned long Dead_time_TTL_Trig_datalogger;
  unsigned long Dead_time_TTL_Cavity_sw;
};


struct mclock_param *mclock_conf_params_instantiate(
                                            struct conf_params **p_confparams );

struct mclock_param *mclock_param_duplicate( struct mclock_param *p_param );


#endif // MCLOCK_PARAM_H

