
#include <string.h>

#include "xalloc.h"

#include "conf_param.h"
#include "mclock_param.h"

struct mclock_param *mclock_conf_params_instantiate(
                                            struct conf_params **p_confparams )
{
  struct mclock_param *_param;
  struct conf_params *_confparams;

  _param = xzmalloc( sizeof(struct mclock_param) );

  _confparams = conf_params_instantiate( "MCLOCK" );
  conf_params_set_baseptr( _confparams, _param );


  conf_param_instantiate( _confparams, "rtts_server", conf_param_type_string, &_param->rtts_server );

  conf_param_instantiate( _confparams, "gplot_detection", conf_param_type_string, &_param->gplot_detection );

  //
  // Acquisition
  //
  conf_param_instantiate( _confparams, "phd_offset", conf_param_type_double, &_param->phd_offset );

  conf_param_instantiate( _confparams, "use_raw_detection_acquisition", conf_param_type_bool, &_param->use_raw_detection_acquisition );
  conf_param_instantiate( _confparams, "acquisition_freq", conf_param_type_double, &_param->acquisition_freq );
  conf_param_instantiate( _confparams, "acquisition_interchannel_gap", conf_param_type_double, &_param->acquisition_interchannel_gap );
  conf_param_instantiate( _confparams, "display_fine_phd", conf_param_type_bool, &_param->display_fine_phd );
  conf_param_instantiate( _confparams, "display_coarse_phd", conf_param_type_bool, &_param->display_coarse_phd );


  //
  // USBDUX ADC
  //
  conf_param_instantiate( _confparams, "use_duxadc", conf_param_type_bool, &_param->use_duxadc );
  conf_param_instantiate( _confparams, "duxadc_server", conf_param_type_string, &_param->duxadc_server );
  conf_param_instantiate( _confparams, "duxadc_filerecord", conf_param_type_string, &_param->duxadc_filerecord );
  conf_param_instantiate( _confparams, "duxadc_nchannels", conf_param_type_uint, &_param->duxadc_nchannels );
  conf_param_instantiate( _confparams, "duxadc_freq", conf_param_type_double, &_param->duxadc_freq );
  conf_param_instantiate( _confparams, "duxadc_shotperiod", conf_param_type_double, &_param->duxadc_shotperiod );




  conf_param_instantiate( _confparams, "ad9959_freq", conf_param_type_double, &_param->ad9959_freq );
  conf_param_instantiate( _confparams, "add9959_pllmult", conf_param_type_uint, &_param->add9959_pllmult );
  conf_param_instantiate( _confparams, "ad9959_stepduration", conf_param_type_double, &_param->ad9959_stepduration );
  conf_param_instantiate( _confparams, "ad9912_freq", conf_param_type_double, &_param->ad9912_freq );



  //
  // Caracteristiques physiques des Shutters
  //

  conf_param_instantiate( _confparams, "cooling_shutter_opening_delay", conf_param_type_double, &_param->cooling_shutter_opening_delay );
  conf_param_instantiate( _confparams, "cooling_shutter_closing_delay", conf_param_type_double, &_param->cooling_shutter_closing_delay );

  conf_param_instantiate( _confparams, "vertical_shutter_opening_delay", conf_param_type_double, &_param->vertical_shutter_opening_delay );
  conf_param_instantiate( _confparams, "vertical_shutter_closing_delay", conf_param_type_double, &_param->vertical_shutter_closing_delay );


  //
  // parametres definissant les temps dans un cycle
  //

  // Cooling
  conf_param_instantiate( _confparams, "Cooling_length", conf_param_type_double, &_param->Cooling_length );
  conf_param_instantiate( _confparams, "Cooling_AOM_amp", conf_param_type_double, &_param->Cooling_AOM_amp );
  conf_param_instantiate( _confparams, "Cooling_Repumper_amp", conf_param_type_double, &_param->Cooling_Repumper_amp );
  conf_param_instantiate( _confparams, "Cooling_Repumper_DDS_frequency", conf_param_type_double, &_param->Cooling_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "Cooling_Cavity_DDS_frequency", conf_param_type_double, &_param->Cooling_Cavity_DDS_frequency );
  conf_param_instantiate( _confparams, "Cooling_Cooling_DDS_frequency", conf_param_type_double, &_param->Cooling_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "Cooling_VCO_setpoint", conf_param_type_double, &_param->Cooling_VCO_setpoint );
  conf_param_instantiate( _confparams, "Cooling_VCO_repumper", conf_param_type_double, &_param->Cooling_VCO_repumper );
  
  // Cooling_to_SDC1_transition;
  conf_param_instantiate( _confparams, "SDC1_trans_length", conf_param_type_double, &_param->SDC1_trans_length );

  // SDC1
  conf_param_instantiate( _confparams, "SDC1_length", conf_param_type_double, &_param->SDC1_length );
  conf_param_instantiate( _confparams, "SDC1_AOM_amp", conf_param_type_double, &_param->SDC1_AOM_amp );
  conf_param_instantiate( _confparams, "SDC1_Repumper_amp", conf_param_type_double, &_param->SDC1_Repumper_amp );
  conf_param_instantiate( _confparams, "SDC1_Repumper_DDS_frequency", conf_param_type_double, &_param->SDC1_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "SDC1_Cooling_DDS_frequency", conf_param_type_double, &_param->SDC1_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "SDC1_VCO_setpoint", conf_param_type_double, &_param->SDC1_VCO_setpoint );
  conf_param_instantiate( _confparams, "SDC1_VCO_repumper", conf_param_type_double, &_param->SDC1_VCO_repumper );

  // SDC1_to_SDC2_transition
  conf_param_instantiate( _confparams, "SDC2_trans_length", conf_param_type_double, &_param->SDC2_trans_length );

  // SDC2
  conf_param_instantiate( _confparams, "SDC2_length", conf_param_type_double, &_param->SDC2_length );
  conf_param_instantiate( _confparams, "SDC2_AOM_amp", conf_param_type_double, &_param->SDC2_AOM_amp );
  conf_param_instantiate( _confparams, "SDC2_Repumper_amp", conf_param_type_double, &_param->SDC2_Repumper_amp );
  conf_param_instantiate( _confparams, "SDC2_Repumper_DDS_frequency", conf_param_type_double, &_param->SDC2_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "SDC2_Cooling_DDS_frequency", conf_param_type_double, &_param->SDC2_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "SDC2_VCO_setpoint", conf_param_type_double, &_param->SDC2_VCO_setpoint );
  conf_param_instantiate( _confparams, "SDC2_VCO_repumper", conf_param_type_double, &_param->SDC2_VCO_repumper );

  // SDC2_to_Blue
  conf_param_instantiate( _confparams, "Blue_trans_length", conf_param_type_double, &_param->Blue_trans_length );

  // Blue
  conf_param_instantiate( _confparams, "Blue_length", conf_param_type_double, &_param->Blue_length );
  conf_param_instantiate( _confparams, "Blue_AOM_amp", conf_param_type_double, &_param->Blue_AOM_amp );
  conf_param_instantiate( _confparams, "Blue_Repumper_amp", conf_param_type_double, &_param->Blue_Repumper_amp );
  conf_param_instantiate( _confparams, "Blue_Repumper_DDS_frequency", conf_param_type_double, &_param->Blue_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "Blue_Cooling_DDS_frequency", conf_param_type_double, &_param->Blue_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "Blue_VCO_setpoint", conf_param_type_double, &_param->Blue_VCO_setpoint );
  conf_param_instantiate( _confparams, "Blue_VCO_repumper", conf_param_type_double, &_param->Blue_VCO_repumper );

  // Blue_to_Depumping
  conf_param_instantiate( _confparams, "Dep_trans_length", conf_param_type_double, &_param->Dep_trans_length );

  // Depumping
  conf_param_instantiate( _confparams, "Dep_length", conf_param_type_double, &_param->Dep_length );
  conf_param_instantiate( _confparams, "Dep_AOM_amp", conf_param_type_double, &_param->Dep_AOM_amp );
  conf_param_instantiate( _confparams, "Dep_Repumper_amp", conf_param_type_double, &_param->Dep_Repumper_amp );
  conf_param_instantiate( _confparams, "Dep_Repumper_DDS_frequency", conf_param_type_double, &_param->Dep_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "Dep_Cooling_DDS_frequency", conf_param_type_double, &_param->Dep_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "Dep_VCO_setpoint", conf_param_type_double, &_param->Dep_VCO_setpoint );
  conf_param_instantiate( _confparams, "Dep_VCO_repumper", conf_param_type_double, &_param->Dep_VCO_repumper );

  // Expansion
  conf_param_instantiate( _confparams, "expansion_length", conf_param_type_double, &_param->expansion_length );
  conf_param_instantiate( _confparams, "expansion_AOM_amp", conf_param_type_double, &_param->expansion_AOM_amp );
  conf_param_instantiate( _confparams, "expansion_Repumper_amp", conf_param_type_double, &_param->expansion_Repumper_amp );
  conf_param_instantiate( _confparams, "expansion_Repumper_DDS_frequency", conf_param_type_double, &_param->expansion_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "expansion_Cooling_DDS_frequency", conf_param_type_double, &_param->expansion_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "expansion_VCO_setpoint", conf_param_type_double, &_param->expansion_VCO_setpoint );
  conf_param_instantiate( _confparams, "expansion_VCO_repumper", conf_param_type_double, &_param->expansion_VCO_repumper );

  // Pi2_1
  conf_param_instantiate( _confparams, "pi2_1_length", conf_param_type_double, &_param->pi2_1_length );

  // Ramsey
  conf_param_instantiate( _confparams, "T_R", conf_param_type_double, &_param->T_R );
  conf_param_instantiate( _confparams, "Ramsey_delay_sw", conf_param_type_double, &_param->Ramsey_delay_sw );
  conf_param_instantiate( _confparams, "Ramsey_delay_cavity_frequency", conf_param_type_double, &_param->Ramsey_delay_cavity_frequency );
  conf_param_instantiate( _confparams, "Ramsey_Cavity_DDS_frequency", conf_param_type_double, &_param->Ramsey_Cavity_DDS_frequency );
  
  // Pi2_2
  conf_param_instantiate( _confparams, "pi2_2_length", conf_param_type_double, &_param->pi2_2_length );

  // Detection_wait
  conf_param_instantiate( _confparams, "det_wait_length", conf_param_type_double, &_param->det_wait_length );
  conf_param_instantiate( _confparams, "det_wait_AOM_amp", conf_param_type_double, &_param->det_wait_AOM_amp );
  conf_param_instantiate( _confparams, "det_wait_Repumper_amp", conf_param_type_double, &_param->det_wait_Repumper_amp );
  conf_param_instantiate( _confparams, "det_wait_Repumper_DDS_frequency", conf_param_type_double, &_param->det_wait_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "det_wait_Cooling_DDS_frequency", conf_param_type_double, &_param->det_wait_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "det_wait_VCO_setpoint", conf_param_type_double, &_param->det_wait_VCO_setpoint );
  conf_param_instantiate( _confparams, "det_wait_VCO_repumper", conf_param_type_double, &_param->det_wait_VCO_repumper );
  
  // Detection
  conf_param_instantiate( _confparams, "det_length", conf_param_type_double, &_param->det_length );
  
  // Push_Depump
  conf_param_instantiate( _confparams, "push_depump_length", conf_param_type_double, &_param->push_depump_length );
  conf_param_instantiate( _confparams, "push_depump_AOM_amp", conf_param_type_double, &_param->push_depump_AOM_amp );
  conf_param_instantiate( _confparams, "push_depump_Repumper_amp", conf_param_type_double, &_param->push_depump_Repumper_amp );
  conf_param_instantiate( _confparams, "push_depump_Repumper_DDS_frequency", conf_param_type_double, &_param->push_depump_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "push_depump_Cooling_DDS_frequency", conf_param_type_double, &_param->push_depump_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "push_depump_VCO_setpoint", conf_param_type_double, &_param->push_depump_VCO_setpoint );
  conf_param_instantiate( _confparams, "push_depump_VCO_repumper", conf_param_type_double, &_param->push_depump_VCO_repumper );

  // Detection_thermal_zero
  conf_param_instantiate( _confparams, "det_th_zero_length", conf_param_type_double, &_param->det_th_zero_length );
  conf_param_instantiate( _confparams, "det_th_zero_AOM_amp", conf_param_type_double, &_param->det_th_zero_AOM_amp );
  conf_param_instantiate( _confparams, "det_th_zero_Repumper_amp", conf_param_type_double, &_param->det_th_zero_Repumper_amp );
  conf_param_instantiate( _confparams, "det_th_zero_Repumper_DDS_frequency", conf_param_type_double, &_param->det_th_zero_Repumper_DDS_frequency );
  conf_param_instantiate( _confparams, "det_th_zero_Cooling_DDS_frequency", conf_param_type_double, &_param->det_th_zero_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "det_th_zero_VCO_setpoint", conf_param_type_double, &_param->det_th_zero_VCO_setpoint );
  //conf_param_instantiate( _confparams, "det_th_zero_VCO_repumper", conf_param_type_double, &_param->det_th_zero_VCO_repumper );

  // Detection_thermal_wait
  conf_param_instantiate( _confparams, "det_th_wait_length", conf_param_type_double, &_param->det_th_wait_length );
  conf_param_instantiate( _confparams, "det_th_wait_AOM_amp", conf_param_type_double, &_param->det_th_wait_AOM_amp );
  conf_param_instantiate( _confparams, "det_th_wait_Repumper_amp", conf_param_type_double, &_param->det_th_wait_Repumper_amp );
  conf_param_instantiate( _confparams, "det_th_wait_Cooling_DDS_frequency", conf_param_type_double, &_param->det_th_wait_Cooling_DDS_frequency );
  conf_param_instantiate( _confparams, "det_th_wait_VCO_setpoint", conf_param_type_double, &_param->det_th_wait_VCO_setpoint );

  // Detection_thermal
  conf_param_instantiate( _confparams, "det_th_length", conf_param_type_double, &_param->det_th_length );
  //conf_param_instantiate( _confparams, "det_th_AOM_amp", conf_param_type_double, &_param->det_th_AOM_amp );
  //conf_param_instantiate( _confparams, "det_th_Repumper_amp", conf_param_type_double, &_param->det_th_Repumper_amp );
  //conf_param_instantiate( _confparams, "det_th_Cooling_DDS_frequency", conf_param_type_double, &_param->det_th_Cooling_DDS_frequency );

  // Dead_time
  conf_param_instantiate( _confparams, "dead_time", conf_param_type_double, &_param->dead_time );
  conf_param_instantiate( _confparams, "Dead_time_AOM_amp", conf_param_type_double, &_param->Dead_time_AOM_amp );
  conf_param_instantiate( _confparams, "Dead_time_Repumper_amp", conf_param_type_double, &_param->Dead_time_Repumper_amp );

  
  // TTL parameters
  // Cooling
  conf_param_instantiate( _confparams, "Cooling_TTL_trigscope1", conf_param_type_uint, &_param->Cooling_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Cooling_TTL_trigscope2", conf_param_type_uint, &_param->Cooling_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Cooling_TTL_AOM_sw", conf_param_type_uint, &_param->Cooling_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Cooling_TTL_Cooling_shutter", conf_param_type_uint, &_param->Cooling_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Cooling_TTL_Vertical_shutter", conf_param_type_uint, &_param->Cooling_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Cooling_TTL_Repumper_sw", conf_param_type_uint, &_param->Cooling_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Cooling_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Cooling_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Cooling_TTL_Trig_datalogger", conf_param_type_uint, &_param->Cooling_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Cooling_TTL_Cavity_sw", conf_param_type_uint, &_param->Cooling_TTL_Cavity_sw );
  
  // Cooling_to_SDC1_transition
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_trigscope1", conf_param_type_uint, &_param->SDC1_trans_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_trigscope2", conf_param_type_uint, &_param->SDC1_trans_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_AOM_sw", conf_param_type_uint, &_param->SDC1_trans_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Cooling_shutter", conf_param_type_uint, &_param->SDC1_trans_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Vertical_shutter", conf_param_type_uint, &_param->SDC1_trans_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Repumper_sw", conf_param_type_uint, &_param->SDC1_trans_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Dispenser_TTL", conf_param_type_uint, &_param->SDC1_trans_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Trig_datalogger", conf_param_type_uint, &_param->SDC1_trans_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "SDC1_trans_TTL_Cavity_sw", conf_param_type_uint, &_param->SDC1_trans_TTL_Cavity_sw );
  
  // SDC1
  conf_param_instantiate( _confparams, "SDC1_TTL_trigscope1", conf_param_type_uint, &_param->SDC1_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "SDC1_TTL_trigscope2", conf_param_type_uint, &_param->SDC1_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "SDC1_TTL_AOM_sw", conf_param_type_uint, &_param->SDC1_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "SDC1_TTL_Cooling_shutter", conf_param_type_uint, &_param->SDC1_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "SDC1_TTL_Vertical_shutter", conf_param_type_uint, &_param->SDC1_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "SDC1_TTL_Repumper_sw", conf_param_type_uint, &_param->SDC1_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "SDC1_TTL_Dispenser_TTL", conf_param_type_uint, &_param->SDC1_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "SDC1_TTL_Trig_datalogger", conf_param_type_uint, &_param->SDC1_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "SDC1_TTL_Cavity_sw", conf_param_type_uint, &_param->SDC1_TTL_Cavity_sw );
  
  // SDC1_to_SDC2_transition
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_trigscope1", conf_param_type_uint, &_param->SDC2_trans_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_trigscope2", conf_param_type_uint, &_param->SDC2_trans_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_AOM_sw", conf_param_type_uint, &_param->SDC2_trans_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Cooling_shutter", conf_param_type_uint, &_param->SDC2_trans_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Vertical_shutter", conf_param_type_uint, &_param->SDC2_trans_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Repumper_sw", conf_param_type_uint, &_param->SDC2_trans_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Dispenser_TTL", conf_param_type_uint, &_param->SDC2_trans_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Trig_datalogger", conf_param_type_uint, &_param->SDC2_trans_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "SDC2_trans_TTL_Cavity_sw", conf_param_type_uint, &_param->SDC2_trans_TTL_Cavity_sw );
  
  // SDC2
  conf_param_instantiate( _confparams, "SDC2_TTL_trigscope1", conf_param_type_uint, &_param->SDC2_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "SDC2_TTL_trigscope2", conf_param_type_uint, &_param->SDC2_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "SDC2_TTL_AOM_sw", conf_param_type_uint, &_param->SDC2_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "SDC2_TTL_Cooling_shutter", conf_param_type_uint, &_param->SDC2_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "SDC2_TTL_Vertical_shutter", conf_param_type_uint, &_param->SDC2_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "SDC2_TTL_Repumper_sw", conf_param_type_uint, &_param->SDC2_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "SDC2_TTL_Dispenser_TTL", conf_param_type_uint, &_param->SDC2_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "SDC2_TTL_Trig_datalogger", conf_param_type_uint, &_param->SDC2_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "SDC2_TTL_Cavity_sw", conf_param_type_uint, &_param->SDC2_TTL_Cavity_sw );
  
  // SDC2_to_Blue
  conf_param_instantiate( _confparams, "Blue_trans_TTL_trigscope1", conf_param_type_uint, &_param->Blue_trans_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_trigscope2", conf_param_type_uint, &_param->Blue_trans_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_AOM_sw", conf_param_type_uint, &_param->Blue_trans_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Cooling_shutter", conf_param_type_uint, &_param->Blue_trans_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Vertical_shutter", conf_param_type_uint, &_param->Blue_trans_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Repumper_sw", conf_param_type_uint, &_param->Blue_trans_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Blue_trans_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Trig_datalogger", conf_param_type_uint, &_param->Blue_trans_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Blue_trans_TTL_Cavity_sw", conf_param_type_uint, &_param->Blue_trans_TTL_Cavity_sw );
  
  // Blue
  conf_param_instantiate( _confparams, "Blue_TTL_trigscope1", conf_param_type_uint, &_param->Blue_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Blue_TTL_trigscope2", conf_param_type_uint, &_param->Blue_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Blue_TTL_AOM_sw", conf_param_type_uint, &_param->Blue_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Blue_TTL_Cooling_shutter", conf_param_type_uint, &_param->Blue_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Blue_TTL_Vertical_shutter", conf_param_type_uint, &_param->Blue_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Blue_TTL_Repumper_sw", conf_param_type_uint, &_param->Blue_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Blue_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Blue_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Blue_TTL_Trig_datalogger", conf_param_type_uint, &_param->Blue_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Blue_TTL_Cavity_sw", conf_param_type_uint, &_param->Blue_TTL_Cavity_sw );
  
  // Blue_to_Depumping
  conf_param_instantiate( _confparams, "Dep_trans_TTL_trigscope1", conf_param_type_uint, &_param->Dep_trans_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_trigscope2", conf_param_type_uint, &_param->Dep_trans_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_AOM_sw", conf_param_type_uint, &_param->Dep_trans_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Cooling_shutter", conf_param_type_uint, &_param->Dep_trans_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Vertical_shutter", conf_param_type_uint, &_param->Dep_trans_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Repumper_sw", conf_param_type_uint, &_param->Dep_trans_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Dep_trans_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Trig_datalogger", conf_param_type_uint, &_param->Dep_trans_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Dep_trans_TTL_Cavity_sw", conf_param_type_uint, &_param->Dep_trans_TTL_Cavity_sw );
  
   // Depumping
  conf_param_instantiate( _confparams, "Dep_TTL_trigscope1", conf_param_type_uint, &_param->Dep_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Dep_TTL_trigscope2", conf_param_type_uint, &_param->Dep_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Dep_TTL_AOM_sw", conf_param_type_uint, &_param->Dep_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Dep_TTL_Cooling_shutter", conf_param_type_uint, &_param->Dep_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Dep_TTL_Vertical_shutter", conf_param_type_uint, &_param->Dep_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Dep_TTL_Repumper_sw", conf_param_type_uint, &_param->Dep_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Dep_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Dep_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Dep_TTL_Trig_datalogger", conf_param_type_uint, &_param->Dep_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Dep_TTL_Cavity_sw", conf_param_type_uint, &_param->Dep_TTL_Cavity_sw );
  
  // Expansion
  conf_param_instantiate( _confparams, "expansion_TTL_trigscope1", conf_param_type_uint, &_param->expansion_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "expansion_TTL_trigscope2", conf_param_type_uint, &_param->expansion_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "expansion_TTL_AOM_sw", conf_param_type_uint, &_param->expansion_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "expansion_TTL_Cooling_shutter", conf_param_type_uint, &_param->expansion_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "expansion_TTL_Vertical_shutter", conf_param_type_uint, &_param->expansion_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "expansion_TTL_Repumper_sw", conf_param_type_uint, &_param->expansion_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "expansion_TTL_Dispenser_TTL", conf_param_type_uint, &_param->expansion_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "expansion_TTL_Trig_datalogger", conf_param_type_uint, &_param->expansion_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "expansion_TTL_Cavity_sw", conf_param_type_uint, &_param->expansion_TTL_Cavity_sw );
  
  // Pi2_1
  conf_param_instantiate( _confparams, "pi2_1_TTL_trigscope1", conf_param_type_uint, &_param->pi2_1_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "pi2_1_TTL_trigscope2", conf_param_type_uint, &_param->pi2_1_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "pi2_1_TTL_AOM_sw", conf_param_type_uint, &_param->pi2_1_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Cooling_shutter", conf_param_type_uint, &_param->pi2_1_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Vertical_shutter", conf_param_type_uint, &_param->pi2_1_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Repumper_sw", conf_param_type_uint, &_param->pi2_1_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Dispenser_TTL", conf_param_type_uint, &_param->pi2_1_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Trig_datalogger", conf_param_type_uint, &_param->pi2_1_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "pi2_1_TTL_Cavity_sw", conf_param_type_uint, &_param->pi2_1_TTL_Cavity_sw );
  
  // Ramsey
  conf_param_instantiate( _confparams, "Ramsey_TTL_trigscope1", conf_param_type_uint, &_param->Ramsey_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Ramsey_TTL_trigscope2", conf_param_type_uint, &_param->Ramsey_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Ramsey_TTL_AOM_sw", conf_param_type_uint, &_param->Ramsey_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Cooling_shutter", conf_param_type_uint, &_param->Ramsey_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Vertical_shutter", conf_param_type_uint, &_param->Ramsey_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Repumper_sw", conf_param_type_uint, &_param->Ramsey_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Ramsey_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Trig_datalogger", conf_param_type_uint, &_param->Ramsey_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Ramsey_TTL_Cavity_sw", conf_param_type_uint, &_param->Ramsey_TTL_Cavity_sw );
  
  // Pi2_2
  conf_param_instantiate( _confparams, "pi2_2_TTL_trigscope1", conf_param_type_uint, &_param->pi2_2_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "pi2_2_TTL_trigscope2", conf_param_type_uint, &_param->pi2_2_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "pi2_2_TTL_AOM_sw", conf_param_type_uint, &_param->pi2_2_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Cooling_shutter", conf_param_type_uint, &_param->pi2_2_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Vertical_shutter", conf_param_type_uint, &_param->pi2_2_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Repumper_sw", conf_param_type_uint, &_param->pi2_2_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Dispenser_TTL", conf_param_type_uint, &_param->pi2_2_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Trig_datalogger", conf_param_type_uint, &_param->pi2_2_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "pi2_2_TTL_Cavity_sw", conf_param_type_uint, &_param->pi2_2_TTL_Cavity_sw );
  
  // Detection_wait
  conf_param_instantiate( _confparams, "det_wait_TTL_trigscope1", conf_param_type_uint, &_param->det_wait_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "det_wait_TTL_trigscope2", conf_param_type_uint, &_param->det_wait_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "det_wait_TTL_AOM_sw", conf_param_type_uint, &_param->det_wait_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "det_wait_TTL_Cooling_shutter", conf_param_type_uint, &_param->det_wait_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "det_wait_TTL_Vertical_shutter", conf_param_type_uint, &_param->det_wait_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "det_wait_TTL_Repumper_sw", conf_param_type_uint, &_param->det_wait_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "det_wait_TTL_Dispenser_TTL", conf_param_type_uint, &_param->det_wait_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "det_wait_TTL_Trig_datalogger", conf_param_type_uint, &_param->det_wait_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "det_wait_TTL_Cavity_sw", conf_param_type_uint, &_param->det_wait_TTL_Cavity_sw );
  
  // Detection
  conf_param_instantiate( _confparams, "det_TTL_trigscope1", conf_param_type_uint, &_param->det_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "det_TTL_trigscope2", conf_param_type_uint, &_param->det_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "det_TTL_AOM_sw", conf_param_type_uint, &_param->det_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "det_TTL_Cooling_shutter", conf_param_type_uint, &_param->det_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "det_TTL_Vertical_shutter", conf_param_type_uint, &_param->det_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "det_TTL_Repumper_sw", conf_param_type_uint, &_param->det_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "det_TTL_Dispenser_TTL", conf_param_type_uint, &_param->det_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "det_TTL_Trig_datalogger", conf_param_type_uint, &_param->det_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "det_TTL_Cavity_sw", conf_param_type_uint, &_param->det_TTL_Cavity_sw );
  
  // Push_Depump
  conf_param_instantiate( _confparams, "push_depump_TTL_trigscope1", conf_param_type_uint, &_param->push_depump_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "push_depump_TTL_trigscope2", conf_param_type_uint, &_param->push_depump_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "push_depump_TTL_AOM_sw", conf_param_type_uint, &_param->push_depump_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "push_depump_TTL_Cooling_shutter", conf_param_type_uint, &_param->push_depump_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "push_depump_TTL_Vertical_shutter", conf_param_type_uint, &_param->push_depump_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "push_depump_TTL_Repumper_sw", conf_param_type_uint, &_param->push_depump_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "push_depump_TTL_Dispenser_TTL", conf_param_type_uint, &_param->push_depump_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "push_depump_TTL_Trig_datalogger", conf_param_type_uint, &_param->push_depump_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "push_depump_TTL_Cavity_sw", conf_param_type_uint, &_param->push_depump_TTL_Cavity_sw );
  
  // Detection_thermal_zero
  conf_param_instantiate( _confparams, "det_th_zero_TTL_trigscope1", conf_param_type_uint, &_param->det_th_zero_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_trigscope2", conf_param_type_uint, &_param->det_th_zero_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_AOM_sw", conf_param_type_uint, &_param->det_th_zero_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Cooling_shutter", conf_param_type_uint, &_param->det_th_zero_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Vertical_shutter", conf_param_type_uint, &_param->det_th_zero_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Repumper_sw", conf_param_type_uint, &_param->det_th_zero_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Dispenser_TTL", conf_param_type_uint, &_param->det_th_zero_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Trig_datalogger", conf_param_type_uint, &_param->det_th_zero_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "det_th_zero_TTL_Cavity_sw", conf_param_type_uint, &_param->det_th_zero_TTL_Cavity_sw );
  
  // Detection_thermal_wait
  conf_param_instantiate( _confparams, "det_th_wait_TTL_trigscope1", conf_param_type_uint, &_param->det_th_wait_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_trigscope2", conf_param_type_uint, &_param->det_th_wait_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_AOM_sw", conf_param_type_uint, &_param->det_th_wait_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Cooling_shutter", conf_param_type_uint, &_param->det_th_wait_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Vertical_shutter", conf_param_type_uint, &_param->det_th_wait_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Repumper_sw", conf_param_type_uint, &_param->det_th_wait_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Dispenser_TTL", conf_param_type_uint, &_param->det_th_wait_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Trig_datalogger", conf_param_type_uint, &_param->det_th_wait_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "det_th_wait_TTL_Cavity_sw", conf_param_type_uint, &_param->det_th_wait_TTL_Cavity_sw );
  
  // Detection_thermal
  conf_param_instantiate( _confparams, "det_th_TTL_trigscope1", conf_param_type_uint, &_param->det_th_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "det_th_TTL_trigscope2", conf_param_type_uint, &_param->det_th_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "det_th_TTL_AOM_sw", conf_param_type_uint, &_param->det_th_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "det_th_TTL_Cooling_shutter", conf_param_type_uint, &_param->det_th_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "det_th_TTL_Vertical_shutter", conf_param_type_uint, &_param->det_th_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "det_th_TTL_Repumper_sw", conf_param_type_uint, &_param->det_th_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "det_th_TTL_Dispenser_TTL", conf_param_type_uint, &_param->det_th_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "det_th_TTL_Trig_datalogger", conf_param_type_uint, &_param->det_th_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "det_th_TTL_Cavity_sw", conf_param_type_uint, &_param->det_th_TTL_Cavity_sw );
  
  // Dead_time
  conf_param_instantiate( _confparams, "Dead_time_TTL_trigscope1", conf_param_type_uint, &_param->Dead_time_TTL_trigscope1 );
  conf_param_instantiate( _confparams, "Dead_time_TTL_trigscope2", conf_param_type_uint, &_param->Dead_time_TTL_trigscope2 );
  conf_param_instantiate( _confparams, "Dead_time_TTL_AOM_sw", conf_param_type_uint, &_param->Dead_time_TTL_AOM_sw );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Cooling_shutter", conf_param_type_uint, &_param->Dead_time_TTL_Cooling_shutter );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Vertical_shutter", conf_param_type_uint, &_param->Dead_time_TTL_Vertical_shutter );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Repumper_sw", conf_param_type_uint, &_param->Dead_time_TTL_Repumper_sw );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Dispenser_TTL", conf_param_type_uint, &_param->Dead_time_TTL_Dispenser_TTL );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Trig_datalogger", conf_param_type_uint, &_param->Dead_time_TTL_Trig_datalogger );
  conf_param_instantiate( _confparams, "Dead_time_TTL_Cavity_sw", conf_param_type_uint, &_param->Dead_time_TTL_Cavity_sw );
  
  if ( p_confparams != NULL )
    *p_confparams = _confparams;

  return _param;
}


struct mclock_param *mclock_param_duplicate( struct mclock_param *p_param )
{
  struct mclock_param *_param;

  _param = xzmalloc( sizeof(struct mclock_param) );

  memcpy( _param, p_param, sizeof(struct mclock_param) );

  return _param;
}


