
#ifndef IOVEC_H
#define IOVEC_H


#ifndef WIN32
# include <sys/uio.h>
#else

#include <stddef.h>

struct iovec
{
  void  *iov_base;    /* Starting address */
  size_t iov_len;     /* Number of bytes to transfer */
};
#endif

#endif // IOVEC_H
