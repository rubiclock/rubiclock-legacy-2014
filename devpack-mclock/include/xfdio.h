
#ifndef XFDIO_H
#define XFDIO_H

#include <sys/types.h>


int xopen_ro( const char *pathname );

ssize_t xread(int fd, void *buf, size_t count);

int xcreat(const char *pathname );

int xcreat_notrunc( const char *pathname );

void xftruncate( int fd, long long length );

long long xlseek( int fd, long long offset, int whence );

ssize_t xwrite(int fd, const void *buf, size_t count);

ssize_t writenwarn( int fd, const void *buf, size_t count );

ssize_t xread(int fd, void *buf, size_t count);

void xread_exact(int fd, void *buf, size_t count);

void xclose(int fd);

#endif // XFDIO_H
