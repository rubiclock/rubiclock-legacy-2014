
#ifndef XSTDIO_H
#define XSTDIO_H

#include <stdio.h>

FILE *xfopen(const char *path, const char *mode);

#endif // XSTDIO_H
