
#ifndef NORMALIZE_H
#define NORMALIZE_H


#include <math.h>


//
// Repris de http://stackoverflow.com/questions/1628386/normalise-orientation-between-0-and-360
//

/*

This is one that normalizes to any range. Useful for normalizing between [-180,180], [0,180] or [0,360].

( it's in C++ though )

//Normalizes any number to an arbitrary range 
//by assuming the range wraps around when going below min or above max 
double normalise( const double value, const double start, const double end ) 
{
  const double width       = end - start   ;   // 
  const double offsetValue = value - start ;   // value relative to 0

  return ( offsetValue - ( floor( offsetValue / width ) * width ) ) + start ;
  // + start to reset back to start of original range
}

For ints

//Normalizes any number to an arbitrary range 
//by assuming the range wraps around when going below min or above max 
int normalise( const int value, const int start, const int end ) 
{
  const int width       = end - start   ;   // 
  const int offsetValue = value - start ;   // value relative to 0

  return ( offsetValue - ( ( offsetValue / width ) * width ) ) + start ;
  // + start to reset back to start of original range
}

So basically the same but without the floor. The version I personally use is a generic one that works for all numeric types and it also uses a redefined floor that does nothing in case of integral types.

*/



static inline double normalize( double value, double start, double end )
{
  const double width       = end - start   ;   // 
  const double offsetValue = value - start ;   // value relative to 0

  return ( offsetValue - ( floor( offsetValue / width ) * width ) ) + start ;
  // + start to reset back to start of original range
}


static inline double normalize_radians( double rad )
{
  return normalize( rad, 0, 2*M_PI );
}


static inline double normalize_radians_center( double rad )
{
   return normalize( rad, -M_PI, M_PI );
}


static inline double normalize_degrees( double deg )
{
  return normalize( deg, 0, 2*180 );
}


static inline double normalize_degrees_center( double deg )
{
   return normalize( deg, -180, 180 );
}



#endif // NORMALIZE_H
