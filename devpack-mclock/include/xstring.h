
#ifndef XSTRING_H
#define XSTRING_H

#include <stdbool.h>

#include <stddef.h>

#include "iovec.h"


size_t iov_get_len( const struct iovec *iov, int iovcnt );

void iov_copy_to_linear( void *dest, const struct iovec *iov, int iovcnt );


char *xatrim( char *str );

char *xastrsep( char **stringp, const char *delim );





void xparse_name_val_pair( char *p_str, char **p_name, char **p_val );

bool is_line_blank_or_comment( char *p_line );

void xfile_parse_name_val_pair( char *p_filename,
                                void (*p_cb)( void *, char *, char * ),
                                void *p_priv );


char *getenv_default( const char *name, char *def );

char *xgetenv(const char *name);

#endif // XSTRING_H
