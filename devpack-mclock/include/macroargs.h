
#ifndef MACROARGS_H
#define MACROARGS_H


/* Indirect stringification.  Doing two levels allows the parameter to be a
 * macro itself.  For example, compile with -DFOO=bar, __stringify(FOO)
 * converts to "bar".
*/

#define __stringify_1(x)        #x
#define __stringify(x)          __stringify_1(x)

// Indirect concatenation  Doing two levels allows the parameter to be a
// macro itself.

#define __concat_1(a,b) a##_##b
#define __concat(a,b) __concat_1(a,b)

#define __concat3_1(a,b,c) a##_##b##_##c
#define __concat3(a,b,c) __concat3_1(a,b,c)



/* Are two types/vars the same type (ignoring qualifiers)? */
#ifndef __same_type
# define __same_type(a, b) __builtin_types_compatible_p(typeof(a), typeof(b))
#endif



#endif // MACROARGS_H
