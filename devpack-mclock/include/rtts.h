
#ifndef RTTS_H
#define RTTS_H

//
// RTTS : Real Time Triggered Sequencer
//

#define RTTS_GROUP_MAX_SIZE 1024

struct rtts_group_header
{
  // nombre d'evenements du groupe
  unsigned long nevents;
};


enum rtts_event
{
  rtts_event_unk,
  rtts_event_timestamp,
  rtts_event_spi_xfer,
  rtts_event_gpio,
  rtts_event_gpio_pulse
};



struct rtts_event_header
{
  enum rtts_event event;
#if 0
  // pour le debug
  unsigned long seqnum;
#endif
};


#define RTTS_TIMESTAMP_MAX_BUF_SIZE 64

struct rtts_timestamp
{
  // time stamp de l'evenement
  unsigned long ts;

  // id de l'evenement a timestamper
  // START_CYCLE, ...
  unsigned long id;
  // DATA associe a l'evenement
  unsigned long data;

  unsigned long buflen;

  char buf[RTTS_TIMESTAMP_MAX_BUF_SIZE];
};



//
// Well known id
//

// pour ces deux la, on a le numero de cycle dans data
// et cycle_time comme buffer associe
#define RTTS_TIMESTAMP_ID_START_CYCLE 0x1
#define RTTS_TIMESTAMP_ID_LAST_CYCLE 0x2

union rtts_timestamp_buf
{
  struct rtts_cycle_time
  {
    // realtime en seconds Unix Epoch (heure UTC)
    double time;
    // temps sequenceur : commence a zero
    double tseq;
  } cycletime;

  char buf[RTTS_TIMESTAMP_MAX_BUF_SIZE];
};





struct rtts_spi_xfer
{
  unsigned long cs;
  unsigned long cdiv;
  unsigned long spilen;
  unsigned char spibuf[];
};

struct rtts_gpio
{
  unsigned long gpio;
  int val;
};

struct rtts_gpio_pulse
{
  unsigned long gpio;
  int pulseval;
  int delay;
};

#endif // RTTS_H

