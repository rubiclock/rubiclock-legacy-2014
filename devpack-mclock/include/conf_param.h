
#ifndef CONF_PARAM_H
#define CONF_PARAM_H

#include <stddef.h>

struct conf_params;

struct conf_params *conf_params_instantiate( char *p_name );

void conf_params_set_baseptr( struct conf_params *p_confparams,
                              void *p_ptr );

enum conf_param_type
{
  conf_param_type_unk,
  conf_param_type_bool,
  conf_param_type_int,
  conf_param_type_uint,
  conf_param_type_double,
  conf_param_type_string
};


struct conf_param;

struct conf_param *conf_param_instantiate(struct conf_params *p_confparams,
                                          char *p_name,
                                          enum conf_param_type p_type,
                                          void *p_ptr );

struct conf_param *conf_param_get_by_name(struct conf_params *p_confparams,
                                          char *p_name );

struct conf_params *conf_params_duplicate( struct conf_params *p_confparams );

char *conf_param_get_name( struct conf_param *p_param );

enum conf_param_type conf_param_get_type( struct conf_param *p_param );

void *conf_param_get_ptr( struct conf_param *p_param );

void conf_params_print_names( struct conf_params *p_confparams );






void conf_param_set_by_string( struct conf_param *p_param, char *p_val );

void conf_param_override( struct conf_params *p_confparams,
                          char *p_name,
                          char *p_val );

void conf_params_load_file( struct conf_params *p_confparams, char *p_filename );


void conf_params_dump( struct conf_params *p_confparams );

void conf_params_change_baseptr( struct conf_params *p_confparams, void *p_ptr );

void conf_param_fprintf_val( FILE *p_file, struct conf_param *p_param );


void conf_params_iterate( struct conf_params *p_confparams,
                          void(*p_cb)(void *, struct conf_param *),
                          void *p_priv );

void conf_param_serialize_argz_add( struct conf_param *p_param,
                                    char **p_argz,
                                    size_t *p_argzlen );

void conf_params_serialize_argz(struct conf_params *p_confparams,
                                char **p_argz,
                                size_t *p_argzlen );

void conf_param_deserialize_argz( struct conf_params *p_confparams,
                                  char *p_argz,
                                  size_t p_argzlen );
#endif // CONF_PARAM_H
