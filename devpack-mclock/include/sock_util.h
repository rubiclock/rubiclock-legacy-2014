
#ifndef SOCK_UTIL_H
#define SOCK_UTIL_H

//
// return: 0 data available
//        -1 : erreur
//        -2 : timeout
//
int sock_poll_for_read( int p_fd, int p_timeoutms );


#endif // SOCK_UTIL_H
