

#ifndef TIMESPEC_H
#define TIMESPEC_H

#include <time.h>

#include <sys/time.h>



// Number of nanoseconds in one second
#define NS_IN_SEC 1000000000
// Number of nanoseconds in one millisecond
#define NS_IN_MS  1000000
// Number of nanoseconds in one microsecond
#define NS_IN_USEC  1000

// Number or milliseconds in a second
#define MS_IN_SEC 1000

// Number or microseconds in a second
#define US_IN_SEC 1000000

// Number of microseconds in one millisecond
#define US_IN_MS  1000

/* Operations on timespecs */
#define	timespecclear(tvp)	((tvp)->tv_sec = (tvp)->tv_nsec = 0)
#define	timespecisset(tvp)	((tvp)->tv_sec || (tvp)->tv_nsec)
#define	timespeccmp(tvp, uvp, cmp)					\
 	(((tvp)->tv_sec == (uvp)->tv_sec) ?				\
 	    ((tvp)->tv_nsec cmp (uvp)->tv_nsec) :			\
 	    ((tvp)->tv_sec cmp (uvp)->tv_sec))
#define timespecadd(vvp, uvp)						\
 	do {								\
 		(vvp)->tv_sec += (uvp)->tv_sec;				\
 		(vvp)->tv_nsec += (uvp)->tv_nsec;			\
 		if ((vvp)->tv_nsec >= 1000000000) {			\
 			(vvp)->tv_sec++;				\
 			(vvp)->tv_nsec -= 1000000000;			\
 		}							\
 	} while (0)
#define timespecsub(vvp, uvp)						\
 	do {								\
 		(vvp)->tv_sec -= (uvp)->tv_sec;				\
 		(vvp)->tv_nsec -= (uvp)->tv_nsec;			\
 		if ((vvp)->tv_nsec < 0) {				\
 			(vvp)->tv_sec--;				\
 			(vvp)->tv_nsec += 1000000000;			\
 		}							\
 	} while (0)



#define timespecprint( tp ) \
  printf("%s = %ld.%ld\n", #tp, tp.tv_sec, tp.tv_nsec );


static inline void timeval_to_timepsec( struct timeval *p_tv,
                                        struct timespec *p_tp )
{
  p_tp->tv_sec =  p_tv->tv_sec;
  p_tp->tv_nsec =  p_tv->tv_usec * 1000L;
}

static inline void seconds_to_timespec( double p_seconds,
                                        struct timespec *p_tp )
{
  long _intseconds = (long)p_seconds;

  p_tp->tv_sec = (time_t)_intseconds;

  p_tp->tv_nsec = (long)( ( p_seconds - (double)_intseconds ) * NS_IN_SEC );
}

static inline double timespec_to_seconds( struct timespec *p_tp )
{
  double _seconds;

  _seconds = (double)p_tp->tv_sec + ( (double)p_tp->tv_nsec ) / NS_IN_SEC;

  return _seconds;
}

static inline unsigned long u32_timespecsub_usec( struct timespec *p_tp1,
                                                  struct timespec *p_tp2 )
{
  unsigned long _usec;

  _usec = (unsigned long)( ( p_tp1->tv_sec - p_tp2->tv_sec ) * NS_IN_SEC
                        + ( p_tp1->tv_nsec - p_tp2->tv_nsec ) );
  return _usec / 1000U;
}


static inline int int32_timespecsub_usec( struct timespec *p_tp1,
                                          struct timespec *p_tp2 )
{
  int _usec;

  _usec = (int)( ( p_tp1->tv_sec - p_tp2->tv_sec ) * NS_IN_SEC
                        + ( p_tp1->tv_nsec - p_tp2->tv_nsec ) );
  return _usec / 1000L;
}


static inline int int32_timespecsub_msec( struct timespec *p_tp1,
                                          struct timespec *p_tp2 )
{
  int _msec;

  _msec = (int)( ( p_tp1->tv_sec - p_tp2->tv_sec ) * NS_IN_SEC
                        + ( p_tp1->tv_nsec - p_tp2->tv_nsec ) );
  return _msec / 1000000L;
}


static inline void timespecadd_msec(struct timespec *p_tp,
                                    unsigned long p_msec )
{

  struct timespec _tp = { .tv_sec = 0, .tv_nsec = 0 };

  while( p_msec > MS_IN_SEC )
  {
    p_msec -= MS_IN_SEC;
    _tp.tv_sec++;
  }

  _tp.tv_nsec = p_msec * NS_IN_MS;

  timespecadd( p_tp, &_tp );
}

static inline void timespecadd_usec(struct timespec *p_tp,
                                    unsigned long p_usec )
{
  struct timespec _tp = { .tv_sec = 0, .tv_nsec = 0 };

  while( p_usec > US_IN_SEC )
  {
    p_usec -= US_IN_SEC;
    _tp.tv_sec++;
  }

  _tp.tv_nsec = p_usec * NS_IN_USEC;

  timespecadd( p_tp, &_tp );
}


#endif // TIMESPEC_H
