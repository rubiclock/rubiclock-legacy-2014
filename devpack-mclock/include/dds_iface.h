
#ifndef DDS_IFACE_H
#define DDS_IFACE_H

#include <stddef.h>
//
// Interface DDS
// Par interface, on entend interface physique, cad dire signal du DDS
// gestion du SPI et des discrets donc
//

struct dds_iface;


void dds_iface_spibuf_add_byte( struct dds_iface *p_dds, unsigned long p_byte );

void dds_iface_spibuf_add_uint64( struct dds_iface *p_dds, unsigned long long p_val );
unsigned long long dds_iface_spibuf_to_cpu_uint64( void *p_buf );

void dds_iface_spibuf_add_uint48( struct dds_iface *p_dds, unsigned long long p_val );
unsigned long long dds_iface_spibuf_to_cpu_uint48( void *p_buf );

void dds_iface_spibuf_add_uint40( struct dds_iface *p_dds, unsigned long long p_val );
unsigned long long dds_iface_spibuf_to_cpu_uint40( void *p_buf );

void dds_iface_spibuf_add_uint32( struct dds_iface *p_dds, unsigned long p_val );
unsigned long dds_iface_spibuf_to_cpu_uint32( void *p_buf );

void dds_iface_spibuf_add_uint24( struct dds_iface *p_dds, unsigned long p_val );
unsigned long dds_iface_spibuf_to_cpu_uint24( void *p_buf );

void dds_iface_spibuf_add_uint16( struct dds_iface *p_dds, unsigned long p_val );
unsigned long dds_iface_spibuf_to_cpu_uint16( void *p_buf );


void dds_iface_spibuf_set_marker( struct dds_iface *p_dds );

void dds_iface_spibuf_reverse( struct dds_iface *p_dds );

void dds_iface_spibuf_write( struct dds_iface *p_dds );

void dds_iface_spibuf_write_sync( struct dds_iface *p_dds );

void dds_iface_spibuf_clear( struct dds_iface *p_dds );

void dds_iface_spibuf_get(struct dds_iface *p_dds,
                          void **p_buf,
                          size_t *p_count );

void dds_iface_set_time( struct dds_iface *p_dds, unsigned long long p_time );

void dds_iface_get_time( struct dds_iface *p_dds, unsigned long long *p_time );

void dds_iface_spi_write( struct dds_iface *p_dds,
                          void *p_buf,
                          size_t p_count );

void dds_iface_spi_read(struct dds_iface *p_dds,
                        unsigned long p_ibyte,
                        void *p_buf,
                        size_t p_count );

void dds_iface_iword16_spi_read(struct dds_iface *p_dds,
                                unsigned long p_iword,
                                void *p_buf,
                                size_t p_count );

void dds_iface_io_update( struct dds_iface *p_dds );

void dds_iface_reset( struct dds_iface *p_dds );


#endif // DDS_IFACE_H
