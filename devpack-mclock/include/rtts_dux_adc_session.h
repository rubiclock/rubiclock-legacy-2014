
#ifndef RTTS_DUX_ADC_SESSION_H
#define RTTS_DUX_ADC_SESSION_H

#include <stdbool.h>

struct rtts_dux_adc_session;


struct rtts_dux_adc_session *rtts_dux_adc_session_instantiate(
                                                      char *p_filerecord,
                                                      char *p_server,
                                                      double p_adcfreq,
                                                      double p_shotperiod );

void rtts_dux_adc_session_add_channel(struct rtts_dux_adc_session *p_session,
                                      unsigned long p_channelid,
                                      bool p_isdifferential );

void rtts_dux_adc_session_run(struct rtts_dux_adc_session *p_session,
                              bool p_detach );

#endif // RTTS_DUX_ADC_SESSION_H
