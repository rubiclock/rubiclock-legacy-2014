
#ifndef AD9959_H
#define AD9959_H

#include <stdbool.h>

#include "dds_iface.h"

struct ad9959_dds;

struct ad9959_dds *ad9959_dds_instantiate(struct dds_iface *p_ddsiface,
                                          double p_fs );


//
// Calcul
//

void ad9959_compute_frequency_tuning_word(struct ad9959_dds *p_dds,
                                          double p_fout,
                                          unsigned long *p_ftw,
                                          double *p_ferr  );

int ad9959_compute_phase_offset_word( struct ad9959_dds *p_dds,
                                      double p_phaseoffsetrad,
                                      unsigned long *p_pow,
                                      double *p_phaseerrrad  );

double ad9959_ftw_to_hz( struct ad9959_dds *p_dds, unsigned long p_ftw );


double ad9959_srr_to_sec( struct ad9959_dds *p_dds, unsigned long p_srr );

unsigned long ad9959_sec_to_srr(struct ad9959_dds *p_dds,
                                double p_stepduration );


//
// Implementation
//

struct dds_iface *ad9959_get_dds_iface( struct ad9959_dds *p_dds );


void ad9959_dds_init_internal_pll(struct ad9959_dds *p_dds,
                                  unsigned long p_multiplier,
                                  bool p_vcogainhigh );

void ad9959_reset( struct ad9959_dds *p_dds );

void ad9959_io_update( struct ad9959_dds *p_dds );

void ad9959_select_channel( struct ad9959_dds *p_dds, unsigned long p_channel );




void ad9959_set_single_tone_reg(struct ad9959_dds *p_dds,
                                int p_channel,
                                unsigned long p_ftw,
                                unsigned long p_pow );

void ad9959_set_single_tone_ftw_pow(struct ad9959_dds *p_dds,
                                    int p_channel,
                                    bool p_setftw,
                                    unsigned long p_ftw,
                                    bool p_setpow,
                                    unsigned long p_pow );

void ad9959_set_single_tone_sequence( struct ad9959_dds *p_dds,
                                      int p_channel,
                                      double p_freq,
                                      double p_phase );

void ad9959_set_two_level_modulation_sequence(struct ad9959_dds *p_dds,
                                              int p_channel,
                                              double p_freq1,
                                              double p_freq2 );


void ad9959_set_two_level_amplitude_modulation_sequence(struct ad9959_dds *p_dds,
                                                        int p_channel,
                                                        double p_freq,
                                                        unsigned long p_amp1,
                                                        unsigned long p_amp2 );


void ad9959_set_linear_sweep_ppin_level(struct ad9959_dds *p_dds,
                                        int p_channel,
                                        bool p_ppinhigh,
                                        double p_fstart,
                                        double p_fend,
                                        unsigned long p_deltaword,
                                        unsigned long p_sweepramprate );

void ad9959_set_linear_sweep( struct ad9959_dds *p_dds,
                              int p_channel,
                              bool p_ppinhigh,
                              double p_fstart,
                              double p_fend,
                              double p_alpha );

void ad9959_set_linear_sweep_ramp_rate_given( struct ad9959_dds *p_dds,
                                              int p_channel,
                                              bool p_ppinhigh,
                                              double p_fstart,
                                              double p_fend,
                                              double p_stepduration,
                                              double p_duration );

#if 0

void ad9959_set_linear_sweep_sequence(struct ad9959_dds *p_dds,
                                      int p_channel,

                                      double p_fstart0,
                                      double p_alpharising,

                                      double p_fend0,
                                      double p_alphafalling,

                                      bool p_nodwellmode );

#endif

#endif // AD9959_H
