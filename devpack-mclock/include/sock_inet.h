
#ifndef SOCK_INET_H
#define SOCK_INET_H

#include "iovec.h"

#ifndef WIN32
# include <netinet/in.h>
#else
# include <winsock2.h>
# include <ws2tcpip.h>
#endif


struct sockaddr_in *xsockaddr_in_init( char *p_hostname, unsigned short p_port );

struct sockaddr_in *xsockaddr_in_initbystring( char *p_str );

char *xsockaddr_in_stringify( struct sockaddr_in *sin );

int xsocket_inet_dgram(void);

int xsocket_inet_stream(void);

void xsock_connect_to_serv_bystring( int p_fd, char *p_servstr );

int sock_brk_send( int s, const void *buf, size_t len, int flags );

void xsock_send( int s, const void *buf, size_t len, int flags );

int sock_brk_sendv( int s, struct iovec *iov, int iovcnt, int flags );

void xsock_sendv( int s, struct iovec *iov, int iovcnt, int flags );

int xsock_brk_recv( int s, void *buf, size_t len, int flags );

int sock_brk_brecv( int s, void *buf, size_t len, int flags );

void xsock_brecv( int s, void *buf, size_t len, int flags );

void xsocket_setsockopt_reuseaddr( int p_fd );

void xsocket_listen( int p_fd, int p_backlog );

int xsocket_accept_sockaddr_in( int p_fd, struct sockaddr_in *p_in );

int xsocket_accept_sockaddr_in_sa_restart( int p_fd, struct sockaddr_in *p_in );

void xsocket_set_non_blocking( int p_fd );




void xsock_inet_dgram_sendto( int s,
                              const void *buf,
                              size_t len,
                              int flags,
                              const struct sockaddr_in *to );

void xsock_inet_dgram_sendv(int s,
                            struct iovec *iov,
                            int iovcnt,
                            int flags,
                            const struct sockaddr_in *to );

ssize_t sock_inet_recvfrom( int s,
                            void *buf,
                            size_t len,
                            int flags,
                            struct sockaddr_in *from );

ssize_t xsock_inet_recvfrom(int s,
                            void *buf,
                            size_t len,
                            int flags,
                            struct sockaddr_in *from );


void xsock_setup_inaddr_any_binded( int p_fd, unsigned short p_port );

int xsock_udp_setup_binded( unsigned short p_port );

#endif // SOCK_INET_H
