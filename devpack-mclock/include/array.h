
#ifndef ARRAY_H
#define ARRAY_H

#define SIZEOF_ARRAY(x) ( sizeof(x) / sizeof(x[0]) )

// le terme "ARRAY_SIZE" en provenance du noyau linux est reellement mieux
#define ARRAY_SIZE SIZEOF_ARRAY

#endif // ARRAY_H

