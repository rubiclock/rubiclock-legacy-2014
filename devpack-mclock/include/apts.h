
#ifndef APTS_H
#define APTS_H


struct apts;

struct apts *apts_instantiate(void);



void apts_register_daqseq(
          struct apts *p_apts,
          void *p_daqseqpriv,
          double (*p_get_seqfreq)( void * ),
          unsigned long (*p_get_dline_by_name)( void *, char * ),
          unsigned long (*p_get_ao_channel_by_name)( void *, char * ),
          unsigned long (*p_get_ai_channel_by_name)( void *, char * ),
          unsigned long (*p_get_dds_by_name)( void *, char * ),
          unsigned long (*p_get_dds_channel_by_name)( void *, char *, char * ),
          unsigned long (*p_get_serial_line_by_name)( void *, char * ),
          void (*p_set_ao_sampling_period_check)( void *, double ),
          void (*p_get_ai_check)( void *, double, double ),
          void (*p_register_loadseq_callback)( void *, int(*)(void *), void * ),
          int (*p_run_sequences)( void *, struct apts * ),
          void (*p_run_sequences_reset)( void * ) );




void apts_set_timeref(struct apts *p_apts,
                      char *p_timeref,
                      char *p_timeorigref,
                      double p_timeoff );


double apts_timeref_get_time( struct apts *p_apts,
                              char *p_timeref,
                              double p_timeoff );



void apts_set_periodic_sequence(struct apts *p_apts,
                                char *p_name,
                                char *p_timeref,
                                double p_timeoff,
                                double p_period,
                                int p_nrperiods );

void apts_set_one_shot_sequence(struct apts *p_apts,
                                char *p_name,
                                char *p_timeref,
                                double p_timeoff,
                                double p_deadline );

void apts_set_end_sequence( struct apts *p_apts, double p_deadline );



void apts_set_event(struct apts *p_apts,
                    char *p_timeref,
                    double p_timeoff,
                    char *p_event,
                    ... );

void apts_set_discrete( struct apts *p_apts,
                        char *p_timeref,
                        double p_timeoff,
                        char *p_dline,
                        unsigned long p_bitval );


void apts_set_single_pulse( struct apts *p_apts,
                            char *p_timeref,
                            double p_timeoff,
                            char *p_dline,
                            unsigned long p_pulsepolarity,
                            double p_pulsewidth );


void apts_set_ao( struct apts *p_apts,
                  char *p_timeref,
                  double p_timeoff,
                  unsigned long p_nrchannels,
                  char **p_channels,
                  double *p_voltages );

void apts_set_ao_waveform(struct apts *p_apts,
                          char *p_timeref,
                          double p_timeoff,
                          double p_period,
                          unsigned long p_nrsamples,
                          unsigned long p_nrchannels,
                          char **p_channels,
                          double **p_voltages );

void apts_set_ao_ramp(struct apts *p_apts,
                      char *p_timeref,
                      double p_timeoff,
                      double p_duration,
                      unsigned long p_nrsamples,
                      unsigned long p_nrchannels,
                      char **p_channels,
                      double *p_v0,
                      double *p_v1 );

void apts_get_ai( struct apts *p_apts,
                  char *p_timeref,
                  double p_timeoff,
                  unsigned long p_nrsamples,
                  double p_interchannelgap,
                  double p_samplingperiod,
                  void (*p_cb)(unsigned long, double *, void *),
                  void *p_cbpriv,
                  unsigned long p_nrchannels,
                  char **p_channels );

void apts_set_dds_chirp(struct apts *p_apts,
                        char *p_timeref,
                        double p_timeoff,
                        char *p_ddsname,
                        char *p_ddschannel,
                        double p_fstart,
                        double p_phase,
                        double p_stepduration,
                        double p_alpha );

void apts_adjust_dds_phase( struct apts *p_apts,
                            char *p_timeref,
                            double p_timeoff,
                            char *p_ddsname,
                            char *p_ddschannel,
                            double p_phase );

void apts_serial_write( struct apts *p_apts,
                        char *p_timeref,
                        double p_timeoff,
                        char *p_linename,
                        unsigned long p_nrbytes,
                        char *p_buffer );


void apts_daqseq_register_loadseq_callback( struct apts *p_apts,
                                            int (*p_loadseq)(void *),
                                            void *p_loadseqpriv );

int apts_daqseq_run_sequences( struct apts *p_apts );

void apts_daqseq_run_sequences_reset( struct apts *p_apts );

void apts_reset( struct apts *p_apts );


#endif // APTS_H
