
#ifndef APTS_NIDAQ_H
#define APTS_NIDAQ_H

#include <stdbool.h>

#include "apts.h"


//
// Definition des lignes discretes
// line : 0 -> P0.0
//        1 -> P0.1
//        ...
//        31 -> P0.31
//
struct apts_nidaq_dline
{
  char *name;
  unsigned long line;
};


struct apts_nidaq_aochannel
{
  char *name;
  char *nidaqchannel;
  double vrangemin;
  double vrangemax;
};


struct apts_nidaq_aichannel
{
  char *name;
  char *nidaqchannel;
  double vrangemin;
  double vrangemax;
};



//
// Pour le moment on se contente d'un seul bus SPI gere par le sequenceur
// Ce bus SPI est caracterise par :
// - une horloge SPI CLK -> genree par un counter du DAQ, ce counter etant "gate"
// par un discret pour generer les fronts d'horloge
// - un discret MOSI
// et c'est tout, la ligne CS est geree au niveau du SPI DEVICE


//
// Definition d'un SPI DEVICE
//
struct apts_nidaq_spidev
{
  // nom du device SPI
  char *name;

  // Chip Selecy line
  unsigned long csline;

  unsigned long csstartdelay;
  unsigned long interbytegap;
  unsigned long csenddelay;
};


struct apts_nidaq_ddshw
{
  // nom de la DDS
  char *name;

  // type HW de la DDS
  char *hwtype;

  // nom du SPI DEVICE qui correspond a la DDS
  char *spidevname;

  unsigned long ioupdateline;
  unsigned long resetline;

  // frequence de fonctionnement de la DDS
  double fs;
};


struct apts_nidaq_ddschannel
{
  // nom du channel
  char *name;
  // nom de la DDS qui heberge la voie
  char *ddsname;
  // numero logique du channel
  unsigned long channel;
};


struct apts_nidaq_serialline
{
  // nom de la serial line
  char *name;

  // discret utilise pour la serial line
  unsigned long dline;

  // parametrage de la liaison serie
  unsigned long ratedivisor;
  unsigned long bitsperword;
  unsigned long maxwords;
  unsigned long idlebits;
  bool invpolarity;
};


struct apts_nidaq_conf
{
  char *extclocksrcterm;
  double seqrefclkfreq;
  char *clockdiscretepulseterm;
  char *clockspipulseterm;
  char *clockspigateterm;
  unsigned long mosispiniline;
  double spiclkfreq;

  bool aitaskpollread;
  int aitaskpollreadinterval;
  unsigned long aireadsamplesbufsize;
  unsigned long aisamplesperpassmult;
  unsigned long aireqsamplesbufsize;

  unsigned long aisamplingclockniline;
  char *aisamplingclocksrcterm;

  unsigned long aotaskbufsize;
  unsigned long aosamplingclockniline;
  char *aosamplingclocksrcterm;

  unsigned long dwgsamplesperpassmult;
  unsigned long dwgsamplesperpass;
  unsigned long dwgusbxferreqsize;
  unsigned long dwgswsignaldline;
  char *dichangedetectionline;


  unsigned long spisclkdriveniline;
  unsigned long spibufmaxsize;

  unsigned long sequenceinsertionearlydelay;

  unsigned long dwgEveryNSamplesTimeout;
  bool registersequenceinterruptionhandler;

};



struct apts_nidaqseq;



struct apts_nidaqseq *apts_nidaqseq_instantiate(
                              void (*p_conf_override)(struct apts_nidaq_conf *),
                              unsigned long p_nrdline,
                              struct apts_nidaq_dline *p_dline,
                              unsigned long p_nraochannel,
                              struct apts_nidaq_aochannel *p_aochannel,
                              unsigned long p_nraichannel,
                              struct apts_nidaq_aichannel *p_aichannel,
                              unsigned long p_nrspidev,
                              struct apts_nidaq_spidev *p_spidev,
                              unsigned long p_nrddshw,
                              struct apts_nidaq_ddshw *p_ddshw,
                              unsigned long p_nrddsch,
                              struct apts_nidaq_ddschannel *p_ddsch,
                              unsigned long p_nrsline,
                              struct apts_nidaq_serialline *p_sline );


double apts_nidaq_get_seqfreq( void *p_daqseqpriv );

unsigned long apts_nidaq_get_dline_by_name( void *p_daqseqpriv,
                                            char *p_name );

unsigned long apts_nidaq_get_ao_channel_by_name(void *p_daqseqpriv,
                                                char *p_name );

unsigned long apts_nidaq_get_ai_channel_by_name(void *p_daqseqpriv,
                                                char *p_name );


unsigned long apts_nidaq_get_dds_by_name( void *p_daqseqpriv, char *p_name );

unsigned long apts_nidaq_get_dds_channel_by_name( void *p_daqseqpriv,
                                                  char *p_ddsname,
                                                  char *p_name );

unsigned long apts_nidaq_get_serial_line_by_name( void *p_daqseqpriv,
                                                  char *p_name );

void apts_nidaq_set_ao_sampling_period_check( void *p_daqseqpriv,
                                              double p_samplingperiod );

void apts_nidaq_get_ai_check( void *p_daqseqpriv,
                              double p_interchannelgap,
                              double p_samplingperiod );



void apts_nidaqseq_register_loadseq_callback( void *p_daqseqpriv,
                                              int (*p_loadseq)(void *),
                                              void *p_loadseqpriv );



#define APTS_NIDAQSEQ_DWG_EVENT_TIMEOUT 0x1
#define APTS_NIDAQSEQ_DWG_SIGNAL_ERROR 0x2
#define APTS_NIDAQSEQ_DWG_FIFO_UNDERFLOW 0x4
#define APTS_NIDAQSEQ_AI_FLUSH_TIMEOUT 0x8
#define APTS_NIDAQSEQ_SEQUENCE_INTERRUPTED 0x10

int apts_nidaqseq_run_sequences( void *p_daqseqpriv, struct apts *p_apts );

void apts_nidaqseq_run_sequences_reset( void *p_daqseqpriv );

void apts_nidaqseq_run_sequences_dump_retval( int p_retval );

void apts_nidaqseq_dump_perf_timing( struct apts_nidaqseq *p_seq );


#endif // APTS_NIDAQ_H
