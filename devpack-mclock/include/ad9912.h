
#ifndef AD9912_H
#define AD9912_H

#include "dds_iface.h"




struct ad9912_dds;

struct ad9912_dds *ad9912_dds_instantiate(struct dds_iface *p_ddsiface,
                                          double p_fs,
                                          bool p_use_sw_update );

void ad9912_write(struct ad9912_dds *p_dds,
                  unsigned long p_addr,
                  void *p_buf,
                  size_t p_count );


void ad9912_read( struct ad9912_dds *p_dds,
                  unsigned long p_addr,
                  void *p_buf,
                  size_t p_count );


void ad9912_set_serial_4wire( struct ad9912_dds *p_dds );


void ad9912_reset( struct ad9912_dds *p_dds );

void ad9912_soft_reset( struct ad9912_dds *p_dds );

void ad9912_io_update( struct ad9912_dds *p_dds );



void ad9912_compute_frequency_tuning_word(struct ad9912_dds *p_dds,
                                          double p_fout,
                                          unsigned long long *p_ftw,
                                          double *p_ferr );

int ad9912_compute_phase_offset_word( struct ad9912_dds *p_dds,
                                      double p_phaseoffsetrad,
                                      unsigned long *p_pow,
                                      double *p_phaseerrrad  );


void ad9912_set_frequency( struct ad9912_dds *p_dds, double p_freq );

void ad9912_set_phase( struct ad9912_dds *p_dds, double p_phase );

void ad9912_set_freq_phase( struct ad9912_dds *p_dds,
                            double p_freq,
                            double p_phase );

void ad9912_set_dac_fs_current( struct ad9912_dds *p_dds,
                                unsigned long p_scale );

#endif // AD9912_H
