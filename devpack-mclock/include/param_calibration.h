
#ifndef PARAM_CALIBRATION_H
#define PARAM_CALIBRATION_H

typedef struct {double x; double y;} CALIBRATION_CURVE;

struct param_calibrations;

struct param_calibrations *param_calibrations_instantiate(void);

struct param_calibration;


struct param_calibration *param_calibration_instantiate(
                                            struct param_calibrations *p_params,
                                            char *p_name );



void param_calibration_set_analog_to_phys(struct param_calibration *p_param,
                                          double (*p_func)(double) );


void param_calibration_set_phys_to_analog(struct param_calibration *p_param,
                                          double (*p_func)(double) );


void param_calibration_set_interp(struct param_calibration *p_param,
                                  char *p_interpname,
                                  unsigned long p_n,
                                  double *p_analog,
                                  double *p_phys );


double param_calibration_analog_to_phys_eval( struct param_calibration *p_param,
                                              double p_analog );

double param_calibration_phys_to_analog_eval( struct param_calibration *p_param,
                                              double p_phys );



double param_calibrations_analog_to_phys_eval(
                                            struct param_calibrations *p_params,
                                            char *p_name,
                                            double p_analog );

double param_calibrations_phys_to_analog_eval(
                                            struct param_calibrations *p_params,
                                            char *p_name,
                                            double p_phys );

double interp_lin( const CALIBRATION_CURVE* curve, int n_points, double y);
											
#endif // PARAM_CALIBRATION_H
