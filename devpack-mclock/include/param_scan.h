
#ifndef PARAM_SCAN_H
#define PARAM_SCAN_H

#include <stdbool.h>

#include <stddef.h>

#include "conf_param.h"


struct param_scan;

unsigned long param_scan_get_range_nstep( double p_start,
                                          double p_end,
                                          double p_step,
                                          bool p_fatal );

struct param_scan *param_scan_instantiate(char *p_tag,
                                          double *p_ptr,
                                          double p_start,
                                          double p_end,
                                          double p_step,
                                          unsigned long p_nsameshot,
                                          unsigned long p_npass );

int param_scan_iterate( struct param_scan *p_scan,
                        double *p_val,
                        unsigned long *p_nshot,
                        unsigned long *p_pass );

void param_scan_free( struct param_scan *p_scan );

char *param_scan_get_tag( struct param_scan *p_scan );


void param_scan_serialize(char *p_tag,
                          char *p_param,
                          double p_start,
                          double p_end,
                          double p_step,
                          unsigned long p_nsameshot,
                          unsigned long p_npass,
                          char **p_argz,
                          size_t *p_argzlen );


struct param_scan *param_scan_deserialize(struct conf_params *p_confparams,
                                          char *p_argz,
                                          size_t p_argzlen );



#endif // PARAM_SCAN_H
