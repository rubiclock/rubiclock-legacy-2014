
#ifndef GNU_STDIO_H
#define GNU_STDIO_H


#if !defined(_STDIO_H) || ( defined(_STDIO_H) && !defined(__USE_XOPEN2K8) )

#include <sys/types.h>

ssize_t getline(char **lineptr, size_t *n, FILE *stream);

ssize_t getdelim(char **lineptr, size_t *n, int delim, FILE *stream);

#endif

#endif // GNU_STDIO_H
