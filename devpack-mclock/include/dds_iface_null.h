
#ifndef DDS_IFACE_NULL_H
#define DDS_IFACE_NULL_H

#include "dds_iface.h"

struct dds_iface *dds_iface_null_instantiate(void);

#endif // DDS_IFACE_NULL_H
