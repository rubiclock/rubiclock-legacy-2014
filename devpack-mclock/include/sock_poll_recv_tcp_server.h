
#ifndef SOCK_POLL_RECV_TCP_SERVER_H
#define SOCK_POLL_RECV_TCP_SERVER_H

#include <stddef.h>

void sock_poll_recv_tcp_server(
                      int *p_ports,
                      unsigned long p_nrports,
                      int p_backlog,
                      size_t p_maxrecvlen,
                      void *(*p_client_priv_instantiate)(void),
                      void (*p_client_recv)(void *priv, void *buf, size_t len),
                      void (*p_client_priv_free)(void *) );

#endif // SOCK_POLL_RECV_TCP_SERVER_H
