
#ifndef PTRARITH_H
#define PTRARITH_H


#include <stddef.h>


#define PTR_ADD( ptr, val ) ( (void *) ( (unsigned long)(ptr) + (unsigned long)(val) ) )

#define PTR_ADD_DEREF( ptr, val, type ) ( *(type *)PTR_ADD( ptr, val ) )

#define PTR_ADD_TYPE( ptr, val ) ( (__typeof__(ptr)) ( (unsigned long)(ptr) + (unsigned long)(val) ) )

#define PTR_DIFF( ptr, val ) ( (ptrdiff_t)( ( (unsigned long)(ptr) - (unsigned long)(val) ) ) )



//
// provient directement de <linux/kernel.h>
//

#define __ALIGN_KERNEL(x, a)		__ALIGN_KERNEL_MASK(x, (typeof(x))(a) - 1)
#define __ALIGN_KERNEL_MASK(x, mask)	(((x) + (mask)) & ~(mask))


#define ALIGN(x, a)		__ALIGN_KERNEL((x), (a))
#define __ALIGN_MASK(x, mask)	__ALIGN_KERNEL_MASK((x), (mask))
#define PTR_ALIGN(p, a)		((typeof(p))ALIGN((unsigned long)(p), (a)))
#define IS_ALIGNED(x, a)		(((x) & ((typeof(x))(a) - 1)) == 0)

#endif // PTRARITH_H
