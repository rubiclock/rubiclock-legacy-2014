
#ifndef RTTS_SESSION_H
#define RTTS_SESSION_H


struct rtts_session;


struct rtts_session *rtts_session_instantiate(void);


void rtts_session_connect( struct rtts_session *p_rtts, char *p_server );


void rtts_session_add_event_timestamp(struct rtts_session *p_rtts,
                                      unsigned long p_id,
                                      unsigned long p_data,
                                      unsigned long p_buflen,
                                      void *p_buf );

void rtts_session_add_event_spi_xfer( struct rtts_session *p_rtts,
                                      unsigned long p_cs,
                                      unsigned long p_fclk,
                                      unsigned long p_spilen,
                                      unsigned char p_spibuf[] );


void rtts_session_add_event_gpio( struct rtts_session *p_rtts,
                                  unsigned long p_gpio,
                                  int p_val );

void rtts_session_add_event_gpio_pulse( struct rtts_session *p_rtts,
                                        unsigned long p_gpio,
                                        int p_pulseval,
                                        int p_delay );


void rtts_session_add_group( struct rtts_session *p_rtts );

void rtts_session_add_close_msg( struct rtts_session *p_rtts );

void rtts_session_add_enable_msg( struct rtts_session *p_rtts );

void rtts_session_add_disable_msg( struct rtts_session *p_rtts );

void rtts_session_flush( struct rtts_session *p_rtts );

#endif // RTTS_SESSION_H
