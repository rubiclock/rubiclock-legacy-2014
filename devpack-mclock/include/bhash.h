
#ifndef BHASH_H
#define BHASH_H

#include <stdint.h>

//
// Fonctions de hash basees sur celle ecrite par Daniel J. Bernstein
//

static inline unsigned int bhash_uint32( uint32_t p_val )
{
  unsigned int _hash = 5381;
  union
  {
    uint32_t integer;
    struct
    {
      uint8_t a;
      uint8_t b;
      uint8_t c;
      uint8_t d;
    } bytes;
  } _val;

  _val.integer = p_val;

  _hash = ((_hash << 5) + _hash) + _val.bytes.a;
  _hash = ((_hash << 5) + _hash) + _val.bytes.b;
  _hash = ((_hash << 5) + _hash) + _val.bytes.c;
  _hash = ((_hash << 5) + _hash) + _val.bytes.d;

  return _hash;
}

#endif // BHASH_H
