
#ifndef XSTRTONUM_H
#define XSTRTONUM_H

#include <stdbool.h>

long int xstrtol( const char *nptr );

unsigned long int xstrtoul( const char *nptr );

unsigned long long xstrtoull( const char *nptr );

double xstrtod( const char *nptr );

bool xstrtobool( const char *nptr );

#endif // XSTRTONUM_H
