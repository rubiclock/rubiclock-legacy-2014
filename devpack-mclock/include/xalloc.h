
#ifndef XALLOC_H
#define XALLOC_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void *xmalloc( size_t p_size );

void *xzmalloc( size_t p_size );

void *xcalloc(size_t nmemb, size_t size);

void *xrealloc(void *ptr, size_t size);

char *xstrdup(const char *s);

int xvasprintf( char **p_strp, const char *p_fmt, va_list p_ap );

int xasprintf(char **p_strp, const char *p_fmt, ...);

void xfree( void *ptr );

void xargz_add(char **argz, size_t *argz_len, const char *str);

#endif // XALLOC_H
