
#ifndef GPLOT_H
#define GPLOT_H

#include <stdarg.h>
#include <stdio.h>


FILE *gplot_open_session( char *p_display );


//
// gestion des windows (plusieurs windows pour une seule session)
//

struct gplot_window;

struct gplot_window *vgplot_window_instantiate( FILE *p_gplot,
                                                int p_id,
                                                char *p_title,
                                                va_list p_ap );

struct gplot_window *gplot_window_instantiate(FILE *p_gplot,
                                              int p_id,
                                              char *p_title,
                                              ... );

void gplot_window_free( struct gplot_window *p_gwin );

void gplot_window_set( struct gplot_window *p_gwin );

void gplot_window_close( struct gplot_window *p_gwin );



//
//
//

FILE *gplot_open_window( char *p_display, char *p_title, ... );

void gplot_close_current_window( FILE *p_gplot );


void gplot_cmd( FILE *p_gplot, char const *p_cmd, ... );



void gplot_close( FILE *p_gplot );

enum gplot_binary_format
{
  gplot_binary_format_unk,
  gplot_binary_format_none,
  gplot_binary_format_int8,
  gplot_binary_format_uint8,
  gplot_binary_format_int16,
  gplot_binary_format_uint16,
  gplot_binary_format_int32,
  gplot_binary_format_uint32,
  gplot_binary_format_int64,
  gplot_binary_format_uint64,
  gplot_binary_format_float32,
  gplot_binary_format_float64
};


struct gplot_plot;

struct gplot_plot *gplot_plot_instantiate(enum gplot_binary_format p_xformat,
                                          enum gplot_binary_format p_yformat,
                                          char *p_title,
                                          ... );

void gplot_plot_set_style( struct gplot_plot *_plot, char *p_style );

void gplot_plot_set_data( struct gplot_plot *p_plot,
                          void *p_x,
                          void *p_y,
                          unsigned long p_n );

void gplot_plot_draw( struct gplot_plot *p_plot, FILE *p_gplot );




struct gplot_plots;

struct gplot_plots *gplot_plots_instantiate(void);

void gplot_plots_add_plot(struct gplot_plots *p_plots,
                          struct gplot_plot *p_plot );

void gplot_plots_draw( struct gplot_plots *p_plots, FILE *p_gplot );


#endif // GPLOT_H
