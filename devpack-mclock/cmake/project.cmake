

#
# Definition de CMK_BASE
#
set( CMK_BASE $ENV{CMK_BASE} )

if ( CMK_BASE  )

  get_filename_component( CMK_BASE ${CMK_BASE} ABSOLUTE )

else()

  set( CMK_BASE $ENV{HOME}/cmk )

endif()




#
# Positionnement des variables de cible utiles quand on compile en natif
#

if( NOT CMAKE_CROSSCOMPILING )
  # Si on ne cross compile pas, on met a jour ces valeurs
  set( CMAKE_SYSTEM_PROCESSOR x86 )
  set( CMAKE_SYSTEM_BIN_ARCH i386 )
  set( CMAKE_SYSTEM_BFD_ARCH elf32-i386 )
endif( NOT CMAKE_CROSSCOMPILING )


#
# Positionnement de la destination des binaires
# selon qu'on produit in source ou out of source
#

if( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )
  # Cas d'un in source build
  message( STATUS "Building in source directory" )
  set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SYSTEM_PROCESSOR} )
  set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SYSTEM_PROCESSOR} )
  set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SYSTEM_PROCESSOR} )

else( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )
  # Cas d'un out of source build
  message( STATUS "Building ouf of source directory" )
  set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
  set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib )
  set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin )

endif( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )





